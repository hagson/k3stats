<?php
// 	Copyright 2018,2019 Hagson

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

	$myPath = "/path/to/logs.db"; // EDIT THIS ACCORDINGLY


	function printArray($theArray) {
		foreach(array_keys($theArray) as $paramName) {
			echo $paramName . " : " . $theArray[$paramName] . "<br>";
		}
	}

	$instances = ["Naxxramas", "Temple of Ahn'Qiraj", "Blackwing Lair", "Molten Core", "Onyxia's Lair"];
	$bosses = [];
	$bosses["Naxxramas"] = ["Patchwerk", "Grobbulus", "Gluth", "Thaddius", "Anub'Rekhan", "Grand Widow Faerlina", "Maexxna", "Noth the Plaguebringer", "Heigan the Unclean", "Loatheb", "Instructor Razuvious", "Gothik the Harvester", "Four Horsemen", "Sapphiron", "Kel'Thuzad"];
	$bosses["Temple of Ahn'Qiraj"] = ["The Prophet Skeram", "Bug Trio", "Battleguard Sartura", "Fankriss the Unyielding", "Viscidus", "Princess Huhuran", "Twin Emperors", "Ouro", "C'Thun"];
	$bosses["Blackwing Lair"] = ["Razorgore the Untamed", "Vaelastrasz the Corrupt", "Broodlord Lashlayer", "Firemaw", "Ebonroc", "Flamegor", "Chromaggus", "Nefarian"];
	$bosses["Molten Core"] = ["Lucifron", "Magmadar", "Gehennas", "Garr", "Shazzrah", "Baron Geddon", "Sulfuron Harbinger", "Golemagg the Incinerator", "Majordomo Executus", "Ragnaros"];
	$bosses["Onyxia's Lair"] = ["Onyxia"];
	$instancelong = [];
	$instancelong["naxx"] = "Naxxramas";
	$instancelong["aq"] = "Temple of Ahn'Qiraj";
	$instancelong["bwl"] = "Blackwing Lair";
	$instancelong["mc"] = "Molten Core";
	$instancelong["ony"] = "Onyxia's Lair";
	$instanceshort = [];
	$instanceshort["Naxxramas"] = "naxx";
	$instanceshort["Temple of Ahn'Qiraj"] = "aq";
	$instanceshort["Blackwing Lair"] = "bwl";
	$instanceshort["Molten Core"] = "mc";
	$instanceshort["Onyxia's Lair"] = "ony";

	$sqlbossorder = "	CASE bossName
						WHEN \"Patchwerk\" THEN 1
						WHEN \"Grobbulus\" THEN 2
						WHEN \"Gluth\" THEN 3
						WHEN \"Thaddius\" THEN 4
						WHEN \"Anub'Rekhan\" THEN 5
						WHEN \"Grand Widow Faerlina\" THEN 6
						WHEN \"Maexxna\" THEN 7
						WHEN \"Noth the Plaguebringer\" THEN 8
						WHEN \"Heigan the Unclean\" THEN 9
						WHEN \"Loatheb\" THEN 10
						WHEN \"Instructor Razuvious\" THEN 11
						WHEN \"Gothik the Harvester\" THEN 12
						WHEN \"Four Horsemen\" THEN 13
						WHEN \"Sapphiron\" THEN 14
						WHEN \"Kel'Thuzad\" THEN 15
						WHEN \"The Prophet Skeram\" THEN 16
						WHEN \"Bug Trio\" THEN 17
						WHEN \"Battleguard Sartura\" THEN 18
						WHEN \"Fankriss the Unyielding\" THEN 19
						WHEN \"Viscidus\" THEN 20
						WHEN \"Princess Huhuran\" THEN 21
						WHEN \"Twin Emperors\" THEN 22
						WHEN \"Ouro\" THEN 23
						WHEN \"C'Thun\" THEN 24
						WHEN \"Razorgore the Untamed\" THEN 25
						WHEN \"Vaelastrasz the Corrupt\" THEN 26
						WHEN \"Broodlord Lashlayer\" THEN 27
						WHEN \"Firemaw\" THEN 28
						WHEN \"Ebonroc\" THEN 29
						WHEN \"Flamegor\" THEN 30
						WHEN \"Chromaggus\" THEN 31
						WHEN \"Nefarian\" THEN 32
						WHEN \"Lucifron\" THEN 33
						WHEN \"Magmadar\" THEN 34
						WHEN \"Gehennas\" THEN 35
						WHEN \"Garr\" THEN 36
						WHEN \"Baron Geddon\" THEN 37
						WHEN \"Shazzrah\" THEN 38
						WHEN \"Sulfuron Harbinger\" THEN 39
						WHEN \"Golemagg the Incinerator\" THEN 40
						WHEN \"Majordomo Executus\" THEN 41
						WHEN \"Ragnaros\" THEN 42
						WHEN \"Onyxia\" THEN 43
						END";
	$sqlinstanceorder = "	CASE instance
							WHEN \"Naxxramas\" THEN 1
							WHEN \"Temple of Ahn'Qiraj\" THEN 2
							WHEN \"Blackwing Lair\" THEN 3
							WHEN \"Molten Core\" THEN 4
							WHEN \"Onyxia's Lair\" THEN 5
							END";

	function getDB() {
		global $myPath;
		return new SQLite3($myPath, SQLITE3_OPEN_READONLY);
	}

	function getRace($raceID) {
		switch ($raceID) {
			case 1:
				return "Human";
			case 2:
				return "Orc";
			case 3:
				return "Dwarf";
			case 4:
				return "Night Elf";
			case 5:
				return "Undead";
			case 6:
				return "Tauren";
			case 7:
				return "Gnome";
			case 8:
				return "Troll";
		}
		return $raceID;
	}

	function getClass($classID) {
		switch ($classID) {
			case 1:
				return "Warrior";
			case 2:
				return "Paladin";
			case 3:
				return "Hunter";
			case 4:
				return "Rogue";
			case 5:
				return "Priest";
			case 7:
				return "Shaman";
			case 8:
				return "Mage";
			case 9:
				return "Warlock";
			case 11:
				return "Druid";
		}
		return $classID;
	}

	function getClassID($class) {
		switch (strtolower($class)) {
			case "warrior":
				return 1;
			case "paladin":
				return 2;
			case "hunter":
				return 3;
			case "rogue":
				return 4;
			case "priest":
				return 5;
			case "shaman":
				return 7;
			case "mage":
				return 8;
			case "warlock":
				return 9;
			case "druid":
				return 11;
		}
		return "%";
	}

	function padToTwoDigits($n) {
		if ($n < 10) {
			return "0" . $n;
		} else {
			return $n;
		}
	}

	function formatTimeRel($time, $includehour) {
		if ($includehour) {
			$str = padToTwoDigits(floor($time/3600)) . ":";
			$time %= 3600;
			$str .= padToTwoDigits(floor($time/60));
			$time %= 60;
		} else {
			$str = padToTwoDigits(floor($time/60));
			$fmod = fmod($time, 1);
			$time %= 60;
			if ($fmod == 0) { // No decimals, add .0
				$time .= "<span style=\"font-size: 70%;\">.0</span>";
			} elseif ($fmod >= 0.95) { // Round to .0
				$time += 1;
				$time .= "<span style=\"font-size: 70%;\">.0</span>";
			} else { // Round to one decimal
				$time .= "<span style=\"font-size: 70%;\">." . round($fmod*10) . "</span>";
			}
		}
		return $str . ":" . padToTwoDigits($time);
	}

	function formatTimeAbs($time) {
		$dt = new DateTime("@$time");
		$dt->setTimeZone(new DateTimeZone("Europe/Stockholm")); // Server time
		return $dt->format("Y-m-d H:i:s");
	}

	function echoInitial($title, $includeSorting, $includeTrimming) {
		echo "<!DOCTYPE html>\n";
		echo "<html>\n";
		echo "<head>\n";
		echo "\t<meta charset=\"utf-8\" />\n";
		if ($title !== "") {
			echo "\t<title>" . $title . " - K3Stats</title>\n";
		} else {
			echo "\t<title>K3Stats</title>\n";
		}
		echo "\t<link href=\"css.css\" rel=\"stylesheet\"/>\n";
		if ($includeSorting) {
			echo "\t<script src=\"sorttable.js\"></script>\n";
		}
		if ($includeTrimming) {
			echo "\t<script src=\"cleanForms.js\"></script>\n";
		}
		echo "</head>\n";
		echo "<body>\n";
		echo "\t<header>\n";
		echo "\t\t<a href=\"index.php\" class=\"title\">K3Stats</a>\n";
		echo "\t\t<a href=\"findraids.php\" class=\"headerlink\">Raids</a>\n";
		echo "\t\t<a href=\"records.php\" class=\"headerlink\">Records</a>\n";
		echo "\t\t<a href=\"findguilds.php\" class=\"headerlink\">Guilds</a>\n";
		echo "\t\t<a href=\"findplayers.php\" class=\"headerlink\">Players</a>\n";
		echo "\t\t<a href=\"about.html\" class=\"headerlink\">About</a>\n";
		echo "\t</header>\n";
	}

	function niceify($string) {
		return rawurldecode(str_replace("+", " ", $string));
	}

	function deniceify($string) {
		return (str_replace(" ", "+", $string));
	}

	function statName($string) {
		switch ($string) {
			case ("encounterID"):
				return "Encounter ID";
			case ("playerName"):
				return "Name";
			case ("playerClass"):
				return "Class";
			case ("playerRace"):
				return "Race";
			case ("playerItemLevel"):
				return "Item Level";
			case ("usefullTime"):
				return "Usefull Time";
			case ("dmgDone"):
				return "Damage Done";
			case ("healingDone"):
				return "Healing Done";
			case ("dmgTaken"):
				return "Damage Taken";
			case ("healingTaken"):
				return "Healing Taken";
			case ("overhealingDone"):
				return "Overhealing Done";
			case ("dmgAbsorbed"):
				return "Damage Absorbed";
			case ("absorbDone"):
				return "Absorption Done";
			case ("dps"):
				return "DPS";
			case ("interrupts"):
				return "Interrupts";
			case ("dispels"):
				return "Dispels";
			case ("hps"):
				return "HPS";
			case ("deaths"):
				return "Deaths";
			case ("resurrections"):
				return "Resurrections";
			case ("bossName"):
				return "Boss";
			case ("instance"):
				return "Instance";
			case ("numKills"):
				return "Number of Kills";
			case ("guildName"):
				return "Guild";
			case ("raidID"):
				return "Raid ID";
			case ("resetNo"):
				return "Reset Number";
			case ("numBosses"):
				return "Number of Bosses";
			case ("killedAt"):
				return "Killed At";
			case ("fightLength"):
				// return "Fight Length";
				return "Duration";
			case ("noParticipants"):
				return "Number of Participants";
			case ("averageItemLevel"):
				return "Average Item Level";
			case ("totalDmgDone"):
				return "Total Damage Done";
			case ("totalHealingDone"):
				return "Total Healing Done";
			case ("totalDmgTaken"):
				return "Total Damage Taken";
			case ("totalHealingTaken"):
				return "Total Healing Taken";
			case ("totalOverhealingDone"):
				return "Total Overhealing Done";
			case ("totalDmgAbsorbed"):
				return "Total Damage Absorbed";
			case ("totalAbsorbDone"):
				return "Total Absorption Done";
			case ("raidDPS"):
				return "Raid DPS";
			case ("totalInterrupts"):
				return "Total Interrupts";
			case ("totalDispels"):
				return "Total Dispels";
			case ("raidHPS"):
				return "Raid HPS";
			case ("averageRaidDPS"):
				return "Average Raid DPS";
			case ("averageRaidHPS"):
				return "Average Raid HPS";
		}
		return $string;
	}

	function raidDisplay($result) {
		$table = "\t<table class=\"sortable\">\n\t\t<tr><th>Raid</th><th>Guild</th><th>Instance</th><th>Earliest kill</th><th>Latest kill</th></tr>\n";
		while ($row = $result->fetchArray()) {
			$table .= "\t\t<tr><td><a href=\"raids.php?id=" . $row["raidID"] . "\">" . $row["raidID"]  . "</a></td><td class=\"faction" . $row["faction"] . "\"><a href=\"guilds.php?name=" . rawurlencode($row["guildName"]) . "\">" . niceify($row["guildName"]) . "</a></td><td>" . $row["instance"] . "</td><td>" . formatTimeAbs($row["startTime"]) . "</td><td>" . formatTimeAbs($row["endTime"]) . "</td></tr>\n";
		}
		echo $table . "\t</table>\n";
	}

	function encounterDisplay($result) {
		$table = "\t<table>\n\t\t<tr><th>Boss name</th><th>Players</th><th>Deaths</th><th>Raid DPS</th><th>Fight Duration</th><th>Killed At</th></tr>\n";
		while ($row = $result->fetchArray()) {
			$table .= "\t\t<tr><td><a href=\"encounters.php?id=" . $row["encounterID"] . "\">" . $row["bossName"] . "</a></td><td>" . $row["noParticipants"] . "</td><td>" . $row["deaths"] . "</td><td>" . floor($row["raidDPS"]) . "</td><td>" . formatTimeRel($row["fightLength"], false) . "</td><td>" . formatTimeAbs($row["killedAt"]) . "</td></tr>\n";
		}
		$table .= "\t</table>\n";
		echo $table;
	}

	function statDisplay($result) {
		$table = "\t<table class=\"sortable\">\n\t\t<tr><th>#</th><th>Player</th><th>Dmg Done</th><th>DPS</th><th>Dmg Taken</th><th>Healing Done</th><th>HPS</th><th>Healing Taken</th><th>Absorb Done</th><th>Dmg Absorbed</th><th>Overhealing Done</th><th>Dispels</th><th>Interrupts</th><th>Deaths</th></tr>\n";
		$i = 1;
		while ($row = $result->fetchArray()) {
			$table .= "\t\t<tr>";
			$table .= "<td>" . $i . "</td>";
			$table .= "<td class=\"class" . $row["playerClass"] . "\"><a href=\"" . "players.php?name=" . $row["playerName"] . "\">" . $row["playerName"] . "</a></td>";
			$table .= "<td>" . $row["dmgDone"] . "</td>";
			$table .= "<td>" . floor($row["dps"]) . "</td>";
			$table .= "<td>" . $row["dmgTaken"] . "</td>";
			$table .= "<td>" . $row["healingDone"] . "</td>";
			$table .= "<td>" . floor($row["hps"]) . "</td>";
			$table .= "<td>" . $row["healingTaken"] . "</td>";
			$table .= "<td>" . $row["absorbDone"] . "</td>";
			$table .= "<td>" . $row["dmgAbsorbed"] . "</td>";
			$table .= "<td>" . $row["overhealingDone"] . "</td>";
			$table .= "<td>" . $row["dispels"] . "</td>";
			$table .= "<td>" . $row["interrupts"] . "</td>";
			$table .= "<td>" . $row["deaths"] . "</td>";
			$table .= "</tr>\n";
			$i++;
		}
		echo ($table . "\t</table>\n");
	}

	function playerRaidRecordsDisplay($result, $tableExtra, $stat, $header) {
		$table = "";
		$i = 1;
		while ($row = $result->fetchArray()) {
			$table .= "<tr><td>" . $i . "</td><td class=\"class" . $row["playerClass"] . "\"><a href=\"players.php?name=" . $row["playerName"] . "\">" . $row["playerName"] . "</a></td><td class=\"faction" . $row["faction"] . "\"><a href=\"guilds.php?name=" . rawurlencode($row["guildName"]) . "\">" . niceify($row["guildName"]) . "</a></td><td><a href=\"raids.php?id=" . $row["raidID"] . "\">" . floor($row[$stat]) . "</a></td></tr>";
			$i++;
		}
		if ($table != "") {
			return "<table " . $tableExtra . " ><tr><th>#</th><th>Player</th><th>Guild</th><th>" . $header . "</th></tr>" . $table . "</table>";
		} else {
			return "";
		}
	}

	function b5o6_playerRaidRecordsDisplay($result, $tableExtra, $stat, $header) {
		$table = "";
		$i = 1;
		while ($row = $result->fetchArray()) {
			$table .= "<tr><td>" . $i . "</td><td class=\"class" . $row["playerClass"] . "\"><a href=\"players.php?name=" . $row["playerName"] . "\">" . $row["playerName"] . "</a></td><td class=\"faction" . $row["faction"] . "\"><a href=\"guilds.php?name=" . rawurlencode($row["guildName"]) . "\">" . niceify($row["guildName"]) . "</a></td><td>" . floor($row[$stat]) . "</td></tr>";
			$i++;
		}
		if ($table != "") {
			return "<table " . $tableExtra . " ><tr><th>#</th><th>Player</th><th>Guild</th><th>" . $header . "</th></tr>" . $table . "</table>";
		} else {
			return "";
		}
	}

	function playerBossRecordsDisplay($result, $tableExtra, $stat, $header) {
		$table = "";
		$i = 1;
		while ($row = $result->fetchArray()) {
			$table .= "<tr><td>" . $i . "</td><td class=\"class" . $row["playerClass"] . "\"><a href=\"players.php?name=" . $row["playerName"] . "\">" . $row["playerName"] . "</a></td><td class=\"faction" . $row["faction"] . "\"><a href=\"guilds.php?name=" . rawurlencode($row["guildName"]) . "\">" . niceify($row["guildName"]) . "</a></td><td><a href=\"encounters.php?id=" . $row["encounterID"] . "\">" . floor($row[$stat]) . "</a></td></tr>";
			$i++;
		}
		if ($table != "") {
			return "<table " . $tableExtra . " ><tr><th>#</th><th>Player</th><th>Guild</th><th>" . $header . "</th></tr>" . $table . "</table>";
		} else {
			return "";
		}
	}

	function b5o6_playerBossRecordsDisplay($result, $tableExtra, $stat, $header) {
		$table = "";
		$i = 1;
		while ($row = $result->fetchArray()) {
			$table .= "<tr><td>" . $i . "</td><td class=\"class" . $row["playerClass"] . "\"><a href=\"players.php?name=" . $row["playerName"] . "\">" . $row["playerName"] . "</a></td><td class=\"faction" . $row["faction"] . "\"><a href=\"guilds.php?name=" . rawurlencode($row["guildName"]) . "\">" . niceify($row["guildName"]) . "</a></td><td>" . floor($row[$stat]) . "</td></tr>";
			$i++;
		}
		if ($table != "") {
			return "<table " . $tableExtra . "><tr><th>#</th><th>Player</th><th>Guild</th><th>" . $header . "</th></tr>" . $table . "</table>";
		} else {
			return "";
		}
	}

	function killtimeRecordsDisplay($result, $boss) {
		$table = "";
		$i = 1;
		while ($row = $result->fetchArray()) {
			$table .= "<tr><td>" . $i . "</td><td class=\"faction" . $row["faction"] . "\"><a href=\"guilds.php?name=" . rawurlencode($row["guildName"]) . "\">" . niceify($row["guildName"]) . "</a></td><td><a href=\"encounters.php?id=" . $row["encounterID"] . "\">" . formatTimeAbs($row["killedAt"]) . "</td><td>" . formatTimeRel($row["fightLength"], false) . "</td></tr>";
			$i++;
		}
		if ($table != "") {
			return "<h2>" . $boss . "</h2><table><tr><th>#</th><th>Guild</th><th>Killed At</th><th>Fight Length</th></tr>" . $table . "</table>";
		} else {
			return "";
		}
	}

	function b5o6_killtimeRecordsDisplay($result) {
		$table = "";
		$i = 1;
		while ($row = $result->fetchArray()) {
			$table .= "<tr><td>" . $i . "</td><td class=\"faction" . $row["faction"] . "\"><a href=\"guilds.php?name=" . deniceify($row["guildName"]) . "\">" . niceify($row["guildName"]) . "</a></td><td>" . formatTimeRel($row["fightLength"], false) . "</td></tr>";
			$i++;
		}
		if ($table != "") {
			return "<h2 title=\"Average of the best 5 of the last 6 entries\" style=\"cursor: help\">Average Performance</h2><table><tr><th>#</th><th>Guild</th><th>Kill Time</th></tr>" . $table . "</table>";
		} else {
			return "";
		}
	}

	function instanceRecordsDisplay($result, $instance) {
		$table = "";
		$i = 1;
		while ($row = $result->fetchArray()) {
			$table .= "<tr><td>" . $i . "</td><td class=\"faction" . $row["faction"] . "\"><a href=\"guilds.php?name=" . rawurlencode($row["guildName"]) . "\">" . niceify($row["guildName"]) . "</a></td><td>";
			$cleartime = formatTimeRel($row["clearTime"], true);
			$table .= formatTimeAbs($row["startTime"]) . "</td><td>" . formatTimeAbs($row["endTime"]) . "</td><td><a href=\"raids.php?id=" . $row["raidID"] . "\">" . $cleartime . "</a></td></tr>";
			$i++;
		}
		if ($table != "") {
			return array(
				("<h2>" . $instance . "</h2><table><tr><th>#</th><th>Guild</th><th>Start time</th><th>End Time</th><th>Cleartime</th></tr>" . $table . "</table>\n"),
				$i);
		} else {
			return array("", 0);
		}
	}

	function b5o6_instanceRecordsDisplay($result, $rows) {
		$table = "";
		$i = 1;
		while ($row = $result->fetchArray()) {
			$table .= "<tr><td>" . $i . "</td><td class=\"faction" . $row["faction"] . "\"><a href=\"guilds.php?name=" . deniceify($row["guildName"]) . "\">" . niceify($row["guildName"]) . "</a></td><td>" . formatTimeRel($row["clearTime"], true) . "</td></tr>";
			$i++;
		}
		if ($table != "") {
			return "<h2 title=\"Average of the best 5 of the last 6 entries\" style=\"cursor: help\">Average Performance</h2><table style=\"margin-bottom: " . ((($rows-$i)*28)+20) . "px\"><tr><th>#</th><th>Guild</th><th>Clear Time</th></tr>" . $table . "</table>\n";
		} else {
			return "";
		}
	}
?>
