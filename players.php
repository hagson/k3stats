<!-- Copyright 2018,2019 Hagson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

<?php
	if ($_GET["name"] == "") {
		header("Location: findplayers.php");
		die();
	}
	require("util.php");

	$db = getDB();
	$statement = $db->prepare("SELECT playerName, guildName, playerClass, playerRace FROM encounters_player JOIN encounters_guild USING (encounterID) WHERE playerName = :name ORDER BY encounterID DESC LIMIT 1");
	$name = /*sqlite_escape_string*/(htmlspecialchars($_GET["name"]));
	$statement->bindValue(":name", $name);
	$result = $statement->execute();
	$row = $result->fetchArray();
	if ($row != false and sizeof($row) > 0) {
		echoInitial($row["playerName"], false, false);
		echo ("<h1>" . $row["playerName"] . " of <a href=\"guilds.php?name=" . urlencode($row["guildName"]) . "\">" . niceify($row["guildName"]) . "</a></h1>\n<h4>" . getRace($row["playerRace"]) . " " . getClass($row["playerClass"]) . "</h4>\n");
	} else {
		echoInitial("Unknown player " . $name, false, false);
		echo ("<h1>Player " . $name . " not found</h1>\n");
		die();
	}
	$legalstats = ["playerItemLevel", "usefullTime", "dmgDone", "healingDone", "dmgTaken", "healingTaken", "overhealingDone", "dmgAbsorbed", "absorbDone", "dps", "interrupts", "dispels", "hps", "deaths"];
	if (!isset($_GET["stat"])) {
		if($row["playerClass"] == 2 || $row["playerClass"] == 5 || $row["playerClass"] == 7 || $row["playerClass"] == 11) {
			$stat = "hps";
		} else {
			$stat = "dps";
		}
	} else if (in_array(htmlspecialchars($_GET["stat"]), $legalstats)) {
		$stat = /*sqlite_escape_string*/(htmlspecialchars($_GET["stat"]));
	} else {
		echo("<h2>Can't rank by " . htmlspecialchars($_GET["stat"]) . "</h2>");
		die();
	}
	$stattext = statName($stat);
	echo ("<form method=\"GET\">
	<input type=\"hidden\" name=\"name\" value=\"" . $row["playerName"] . "\">
	<select name=\"stat\" class=\"raidfilter\">
		<option value=\"" . $stat . "\" selected hidden>" . $stattext . "</option>
		<option value=\"dps\">DPS</option>
		<option value=\"hps\">HPS</option>
		<option value=\"dmgDone\">Damage Done</option>
		<option value=\"healingDone\">Healing Done</option>
		<option value=\"dmgTaken\">Damage Taken</option>
		<option value=\"healingTaken\">Healing Taken</option>
		<option value=\"overhealingDone\">Overhealing Done</option>
		<option value=\"absorbDone\">Absorption Done</option>
		<option value=\"dmgAbsorbed\">Damage Absorbed</option>
		<option value=\"interrupts\">Interrupts</option>
		<option value=\"dispels\">Dispels</option>
		<option value=\"deaths\">Deaths</option>
		<option value=\"resurrections\">Times Resurrected</option>
		<!--<option value=\"playerItemLevel\">Item Level</option>
		<option value=\"usefullTime\">Usefull Time</option>-->
	</select>
	<input type=\"submit\" value=\"Change stat\">
	</form>");

	// Unfortunately you can't bind what you select over so we have to use normal string manipulation. $stat is one of whitelisted values though.
	$playerinstances = [];
	$b5o6_playerinstances = [];
	$raid_playerinstances = [];
	$b5o6_raid_playerinstances = [];
	$totstmt = $db->prepare("	SELECT instance, bossName, encounterID, max(" . $stat . ") AS stat
								FROM (encounters_player JOIN encounters_guild USING (encounterid))
								WHERE playerName = :name
								GROUP BY bossName
								ORDER BY " . $sqlbossorder);
	$totstmt->bindValue(":name", $name);
	$rankstmt = $db->prepare("	SELECT instance, bossName, total_" . $stat . " AS total, guild_" . $stat . " AS guild, class_" . $stat . " AS class, faction_" . $stat . " AS faction
								FROM ranks_encounters_player
								WHERE playerName = :name
								GROUP BY bossName
								ORDER BY " . $sqlbossorder);
	$rankstmt->bindValue(":name", $name);
	$totres = $totstmt->execute();
	$rankres = $rankstmt->execute();
	$raid_totstmt = $db->prepare("	SELECT instance, raidID, max(" . $stat . ") AS stat
									FROM (raids_player JOIN raids_guild USING (raidID))
									WHERE playerName = :name
									GROUP BY instance");
	$raid_totstmt->bindValue(":name", $name);
	$raid_rankstmt = $db->prepare("	SELECT instance, total_" . $stat . " AS total, guild_" . $stat . " AS guild, class_" . $stat . " AS class, faction_" . $stat . " AS faction
									FROM ranks_raids_player
									WHERE playerName = :name
									GROUP BY instance");
	$raid_rankstmt->bindValue(":name", $name);
	$raid_totres = $raid_totstmt->execute();
	$raid_rankres = $raid_rankstmt->execute();
	$b5o6_totstmt = $db->prepare("	SELECT numKills, instance, bossName, " . $stat . " as stat
									FROM b5o6_encounters_player
									WHERE playerName = :name
									GROUP BY bossName
									ORDER BY " . $sqlbossorder);
	$b5o6_totstmt->bindValue(":name", $name);
	$b5o6_rankstmt = $db->prepare("	SELECT instance, bossName, total_" . $stat . " AS total, guild_" . $stat . " AS guild, class_" . $stat . " AS class, faction_" . $stat . " AS faction
									FROM ranks_b5o6_encounters_player
									WHERE playerName = :name
									GROUP BY bossName
									ORDER BY " . $sqlbossorder);
	$b5o6_rankstmt->bindValue(":name", $name);
	$b5o6_totres = $b5o6_totstmt->execute();
	$b5o6_rankres = $b5o6_rankstmt->execute();
	$b5o6_raid_totstmt = $db->prepare("	SELECT numClears, instance, " . $stat . " as stat
										FROM b5o6_raids_player
										WHERE playerName = :name
										GROUP BY instance");
	$b5o6_raid_totstmt->bindValue(":name", $name);
	$b5o6_raid_rankstmt = $db->prepare("SELECT instance, total_" . $stat . " AS total, guild_" . $stat . " AS guild, class_" . $stat . " AS class, faction_" . $stat . " AS faction
										FROM ranks_b5o6_raids_player
										WHERE playerName = :name
										GROUP BY instance");
	$b5o6_raid_rankstmt->bindValue(":name", $name);
	$b5o6_raid_totres = $b5o6_raid_totstmt->execute();
	$b5o6_raid_rankres = $b5o6_raid_rankstmt->execute();

	while ($totrow = $totres->fetchArray()) {
		$playerinstances[$totrow["instance"]][$totrow["bossName"]]["id"] = $totrow["encounterID"];
		$playerinstances[$totrow["instance"]][$totrow["bossName"]]["stat"] = $totrow["stat"];
	}
	while ($rankrow = $rankres->fetchArray()) {
		$playerinstances[$rankrow["instance"]][$rankrow["bossName"]]["rank"] = $rankrow["total"];
		$playerinstances[$rankrow["instance"]][$rankrow["bossName"]]["crank"] = $rankrow["class"];
		$playerinstances[$rankrow["instance"]][$rankrow["bossName"]]["grank"] = $rankrow["guild"];
	}
	while ($totrow = $b5o6_totres->fetchArray()) {
		$b5o6_playerinstances[$totrow["instance"]][$totrow["bossName"]]["numKills"] = $totrow["numKills"];
		$b5o6_playerinstances[$totrow["instance"]][$totrow["bossName"]]["stat"] = $totrow["stat"];
	}
	while ($rankrow = $b5o6_rankres->fetchArray()) {
		$b5o6_playerinstances[$rankrow["instance"]][$rankrow["bossName"]]["rank"] = $rankrow["total"];
		$b5o6_playerinstances[$rankrow["instance"]][$rankrow["bossName"]]["crank"] = $rankrow["class"];
		$b5o6_playerinstances[$rankrow["instance"]][$rankrow["bossName"]]["grank"] = $rankrow["guild"];
	}
	while ($totrow = $raid_totres->fetchArray()) {
		$raid_playerinstances[$totrow["instance"]]["id"] = $totrow["raidID"];
		$raid_playerinstances[$totrow["instance"]]["stat"] = $totrow["stat"];
	}
	while ($rankrow = $raid_rankres->fetchArray()) {
		$raid_playerinstances[$rankrow["instance"]]["rank"] = $rankrow["total"];
		$raid_playerinstances[$rankrow["instance"]]["crank"] = $rankrow["class"];
		$raid_playerinstances[$rankrow["instance"]]["grank"] = $rankrow["guild"];
	}
	while ($totrow = $b5o6_raid_totres->fetchArray()) {
		$b5o6_raid_playerinstances[$totrow["instance"]]["numClears"] = $totrow["numClears"];
		$b5o6_raid_playerinstances[$totrow["instance"]]["stat"] = $totrow["stat"];
	}
	while ($rankrow = $b5o6_raid_rankres->fetchArray()) {
		$b5o6_raid_playerinstances[$rankrow["instance"]]["rank"] = $rankrow["total"];
		$b5o6_raid_playerinstances[$rankrow["instance"]]["crank"] = $rankrow["class"];
		$b5o6_raid_playerinstances[$rankrow["instance"]]["grank"] = $rankrow["guild"];
	}

	foreach ($playerinstances as $instance => $val) {
		echo ("<h1 style=\"clear: both\">" . $instance . "</h1>\n<div style=\"width: auto; margin: auto; display: inline-block\">\n");
		echo "<div style=\"float: left; margin-bottom: 30px; padding-right: 40px;\">\n<h2>All time records</h2>\n";
		$table = "";
		foreach ($val as $boss => $stats) {
			$table .= "<tr>";
			$table .= "<td>" . $boss . "</a></td>";
			$table .= "<td><a href=\"encounters.php?id=" . $stats["id"] . "\">" . floor($stats["stat"]) . "</a></td>";
			$table .= "<td><a href=\"records.php?boss=" . $boss . "&stat=" . $stat . "&guild=" . $row["guildName"] . "\">" . $stats["grank"] . "</td>";
			$table .= "<td><a href=\"records.php?boss=" . $boss . "&stat=" . $stat . "&class=" . getClass($row["playerClass"]) . "\">" . $stats["crank"] . "</td>";
			$table .= "<td><a href=\"records.php?boss=" . $boss . "&stat=" . $stat . "\">" . $stats["rank"] . "</td>";
			$table .= "</tr>";
		}
		if ($instance != "Onyxia's Lair") {
			$table .= "<tr style=\"background-color: rgb(40, 40, 40)\"><td title=\"Average of all fights in a raid, weighted by length\">Full raid</td>";
			$table .= "<td><a href=\"raids.php?id=" . $raid_playerinstances[$instance]["id"] . "\">" . floor($raid_playerinstances[$instance]["stat"]) . "</a></td>";
			$table .= "<td><a href=\"records.php?instance=" . $instanceshort[$instance] . "&stat=" . $stat . "&guild=" . $row["guildName"] . "\">" . $raid_playerinstances[$instance]["grank"] . "</td>";
			$table .= "<td><a href=\"records.php?instance=" . $instanceshort[$instance] . "&stat=" . $stat . "&class=" . getClass($row["playerClass"]) . "\">" . $raid_playerinstances[$instance]["crank"] . "</td>";
			$table .= "<td><a href=\"records.php?instance=" . $instanceshort[$instance] . "&stat=" . $stat . "\">" . $raid_playerinstances[$instance]["rank"] . "</td>";
			$table .= "</tr>";
		}

		echo "<table class=\"sortable\"><tr><th>Boss</th><th>" . $stattext . "</th><th>Guild #</th><th>Class #</th><th>Total #</th></tr>\n" . $table . "</table>\n</div>\n";

		$table = "";
		echo "<div style=\"float: right; margin-bottom: 30px;\">\n<h2 title=\"Average of the best 5 of the last 6 entries\">Average performance</h2>\n";
		if (isset($b5o6_playerinstances[$instance])) { # At least one b5o6 records in this instance
			foreach($b5o6_playerinstances[$instance] as $boss => $stats) {
				$table .= "<tr>";
				$table .= "<td>" . $boss . "</td>";
				if ($stats["numKills"] >= 6) {
					$table .= "<td>" . floor($stats["stat"]) . "</td>";
					$table .= "<td>" . $stats["grank"] . "</td>";
					$table .= "<td>" . $stats["crank"] . "</td>";
					$table .= "<td>" . $stats["rank"] . "</td>";
				} else {
					$table .= "<td>n/a</td>";
					$table .= "<td>n/a</td>";
					$table .= "<td>n/a</td>";
					$table .= "<td>n/a</td>";
				}
				$table .= "</tr>";
			}
			if ($instance != "Onyxia's Lair") {
				$table .= "<tr style=\"background-color: rgb(40, 40, 40)\"><td title=\"Average of all fights in a raid, weighted by length\">Full raid</td>";
				if ($b5o6_raid_playerinstances[$instance]["numClears"] >= 6) {
					$table .= "<td>" . floor($b5o6_raid_playerinstances[$instance]["stat"]) . "</td>";
					$table .= "<td>" . $b5o6_raid_playerinstances[$instance]["grank"] . "</td>";
					$table .= "<td>" . $b5o6_raid_playerinstances[$instance]["crank"] . "</td>";
					$table .= "<td>" . $b5o6_raid_playerinstances[$instance]["rank"] . "</td>";
				} else {
					$table .= "<td>n/a</td>";
					$table .= "<td>n/a</td>";
					$table .= "<td>n/a</td>";
					$table .= "<td>n/a</td>";
				}
				$table .= "</tr>";
			}
			
			echo "<table class=\"sortable\"><tr><th>Boss</th><th>" . $stattext . "</th><th>Guild #</th><th>Class #</th><th>Total #</th></tr>\n" . $table . "</table>\n";
		}
		echo ("</div></div>\n");
	}

	// Latest 20 raids
	echo "</div></div><h2>Latest raids</h2>\n";
	$raids = $db->prepare("	SELECT raidID, guildName, faction, instance, startTime, endTime
							FROM raids_guild JOIN raids_player using (raidID)
							WHERE playerName = :name
							ORDER BY endTime DESC LIMIT 20");
	$raids->bindValue(":name", $name);
	$raids = $raids->execute();
	raidDisplay($raids);
?>
</body>
</html>
