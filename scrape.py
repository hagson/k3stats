#!/usr/bin/python3

# Copyright 2018,2019 Hagson

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import signal
import os
import sys
import sqlite3
from datetime import datetime #, timedelta
import requests
import argparse
import pickle
# import pytz

INFO = 0
WARN = 1
FAIL = 2
logLevel = INFO # All messages (Information, Warning and Failure messages)
# logLevel = WARN # Only Warning and Failure messages
# logLevel = FAIL # Only Failure messages
# logLevel = 3 # No logging at all. Note that failure level events usually result in the program terminating, so it's useful to have at least these enabled
logMessage = ["INFO", "WARN", "FAIL"]

myPath = "./" # Can be overruled with command line arguments

stopFetching = False
doneFetching = False
bossesInInstance = {}
bossesInInstance["Onyxia's Lair"] = 1
bossesInInstance["Molten Core"] = 10
# bossesInInstance["Molten Core"] = 9 # No Majordomo
bossesInInstance["Blackwing Lair"] = 8
bossesInInstance["Temple of Ahn'Qiraj"] = 9
bossesInInstance["Naxxramas"] = 15
legalBosses = [	"Onyxia",
				"Lucifron", "Magmadar", "Gehennas", "Garr", "Baron Geddon", "Shazzrah", "Sulfuron Harbinger", "Golemagg the Incinerator", "Majordomo Executus", "Ragnaros",
				"Razorgore the Untamed", "Vaelastrasz the Corrupt", "Broodlord Lashlayer", "Firemaw", "Ebonroc", "Flamegor", "Chromaggus", "Nefarian",
				"The Prophet Skeram", "Princess Yauj", "Lord Kri", "Vem", "Battleguard Sartura", "Fankriss the Unyielding", "Viscidus", "Princess Huhuran", "Twin Emperors", "Ouro", "Eye of C'Thun", "C'Thun",
				"Patchwerk", "Grobbulus", "Gluth", "Thaddius", "Anub'Rekhan", "Grand Widow Faerlina", "Maexxna", "Noth the Plaguebringer", "Heigan the Unclean", "Loatheb", "Instructor Razuvious", "Gothik the Harvester", "Thane Korth'azz", "Highlord Mograine", "Lady Blaumeux", "Sir Zeliek", "Sapphiron", "Kel'Thuzad" ]
skippable = [ "Bug Trio", "Viscidus", "Ouro" ]
specialBosses = [ "Princess Yauj", "Lord Kri", "Vem", "Eye of C'Thun", "C'Thun" ]
bugTrioBosses = [ "Princess Yauj", "Lord Kri", "Vem" ]
fourHorsemen = [ "Thane Korth'azz", "Highlord Mograine", "Lady Blaumeux", "Sir Zeliek" ]
forbidden = [	# Bosskills that shouldn't be included. Ex. GM's killing bosses, raid reset bugs, etc.
				683818, 683829, # Players Feikar and Coyos somehow managed to kill Onyxia twice in one ID
				693071, # Theemage somehow reset MC ID and joined a second raid
				694587, 694589, 694594, 694595, 694599, 694601, 694602, 694604, 694609 # GM's killed a number of bosses just before BWL release
			]

def getInfo(twinstarID):
	if stopFetching:
		print("Aborting due to SIGINT")
		return "abort"
	if twinstarID in forbidden:
		writeToLog(INFO, "Ignoring forbidden ID", twinstarID)
		updateLatest(twinstarID)
		return
	url = "https://vanilla-twinhead.twinstar.cz/?boss-kill=%d" % twinstarID
	page = requests.get(url)
	if page.status_code != 200 or page.url != url:
		writeToLog(WARN, "Failed to load boss kill page: " + url, twinstarID)
		return
	content = page.text
	conn = sqlite3.connect(myPath + "logs.db")
	c = conn.cursor()

	# Get info for "encounters_player" table
	startOfData = content.find("bosskillData = [{") + 17
	endOfData = content.find("]", startOfData) - 1
	data = content[startOfData:endOfData]
	stats = data.split("},{")
	averageItemLevel = 0.0
	totalDmgDone = 0.0
	totalHealingDone = 0.0
	totalDmgTaken = 0.0
	totalHealingTaken = 0.0
	totalOverhealingDone = 0.0
	totalDmgAbsorbed = 0.0
	totalAbsorbDone = 0.0
	raidDPS = 0.0
	totalInterrupts = 0.0
	totalDispels = 0.0
	raidHPS = 0.0

	# print(content)
	marker = content.find("?guild=")
	marker2 = content.find("&realm=", marker)
	guildName = content[marker+7:marker2]
	realmName = content[marker2+7:content.find(">", marker2+1)-1]
	marker = content.find("href=\"?npc=")
	marker = content.find("\">", marker)
	bossName = content[marker+2:content.find("<", marker+1)]
	if bossName == "Emperor Vek'lor":
		bossName = "Twin Emperors"

	if guildName == "":
		writeToLog(INFO, "Not inserting info for " + bossName + ", due to Mixed Group", twinstarID)
		updateLatest(twinstarID)
		return

	if realmName != "Kronos III":
		writeToLog(INFO, "Not inserting info for " + bossName + " by " + guildName + ", due to wrong realm", twinstarID)
		updateLatest(twinstarID)
		return

	if bossName not in legalBosses:
		writeToLog(INFO, "Not inserting info for " + bossName + " by " + guildName + ", due to Illegal Boss", twinstarID)
		updateLatest(twinstarID)
		return

	marker = content.find("Raid</td>")
	marker = content.find("<td>", marker)
	instance = content[marker+4:content.find("<", marker+1)]
	if instance == "Ahn'Qiraj Temple":
		instance = "Temple of Ahn'Qiraj"

	marker = content.find("Killed at")
	marker = content.find("<td>", marker)
	killedAt = content[marker+4:content.find("<",marker+1)]
	# killedAtEpoch = (datetime.strptime(killedAt, "%Y/%m/%d %H:%M:%S") - datetime(1970,1,1)).total_seconds()
	killedAtEpoch = int(datetime.strptime(killedAt, "%Y/%m/%d %H:%M:%S").timestamp())
	resetNo = calcResetNo(instance, killedAtEpoch, twinstarID)

	marker = content.find("Fight Length")
	marker = content.find("<td>", marker)
	fightLength = content[marker+4:content.find("<", marker+2)]
	if "min" in fightLength:
		minpos = fightLength.find("min")
		fightLength = 60 * int(fightLength[:minpos]) + float(fightLength[minpos+4:-3])
	else:
		fightLength = float(fightLength[:-3])

	marker = content.find("Deaths During Fight")
	marker = content.find("<td>", marker)
	deaths = int(content[marker+4:content.find("<", marker+2)])

	marker = content.find("Resurrects During Fight")
	marker = content.find("<td>", marker)
	resurrections = int(content[marker+4:content.find("<", marker+2)])

	raidID = None
	faction = None
	playerStats = []
	for i in range(len(stats)): # There are some useless stats we don't include, such as realm and guild
		individualStats = stats[i].split("\"")
		playerStats.append({})
		playerStats[i]["playerName"] = individualStats[11]
		playerStats[i]["playerClass"] = individualStats[15]
		playerStats[i]["playerRace"] = individualStats[19]
		playerStats[i]["playerItemLevel"] = individualStats[31]
		playerStats[i]["usefullTime"] = individualStats[35]
		playerStats[i]["dmgDone"] = individualStats[39]
		playerStats[i]["healingDone"] = individualStats[43]
		playerStats[i]["dmgTaken"] = individualStats[47]
		playerStats[i]["healingTaken"] = individualStats[51]
		playerStats[i]["overhealingDone"] = individualStats[55]
		playerStats[i]["dmgAbsorbed"] = individualStats[59]
		playerStats[i]["absorbDone"] = individualStats[63]
		playerStats[i]["dps"] = float(individualStats[39])/fightLength # Use manually calculated DPS since twinhead is not always accurate
		playerStats[i]["interrupts"] = individualStats[75]
		playerStats[i]["dispels"] = individualStats[79]
		playerStats[i]["hps"] = float(individualStats[43])/fightLength # Use manually calculated HPS since twinhead is not always accurate
		playerStats[i]["deaths"] = 0
		playerStats[i]["resurrections"] = 0

		if faction is None:
			if (int(playerStats[i]["playerRace"]) in [1, 3, 4, 7]): # Alliance
				faction = 0
			else:
				faction = 1

		averageItemLevel += float(playerStats[i]["playerItemLevel"])
		totalDmgDone += float(playerStats[i]["dmgDone"])
		totalHealingDone += float(playerStats[i]["healingDone"])
		totalDmgTaken += float(playerStats[i]["dmgTaken"])
		totalHealingTaken += float(playerStats[i]["healingTaken"])
		totalOverhealingDone += float(playerStats[i]["overhealingDone"])
		totalDmgAbsorbed += float(playerStats[i]["dmgAbsorbed"])
		totalAbsorbDone += float(playerStats[i]["dmgAbsorbed"])
		raidDPS += float(playerStats[i]["dps"])
		totalInterrupts += float(playerStats[i]["interrupts"])
		totalDispels += float(playerStats[i]["dispels"])
		raidHPS += float(playerStats[i]["hps"])

		if raidID is None:
			raidID = getRaidID(c, instance, resetNo, playerStats[i]["playerName"])
		elif getRaidID(c, instance, resetNo, playerStats[i]["playerName"]) != None and getRaidID(c, instance, resetNo, playerStats[i]["playerName"]) != raidID: # Shouldn't happen
			writeToLog(FAIL, "Two different raidIDs for " + playerStats[i]["playerName"] + " in " + instance + " at resetNo " + str(resetNo) + " - " + raidID + " and " + getRaidID(c, instance, resetNo, playerStats[i]["playerName"]), twinstarID)
			exit(1)
			return

	averageItemLevel = averageItemLevel / len(stats)

	# To find out who died and got ressed you need to consult the javascript chart. This is not an excellent way to do so
	marker = content.find("var chart_data = [[[")
	marker2 = content.find(";", marker)
	chart = content[marker+20:marker2]
	tables = chart.split("[[")
	if len(tables) > 4:
		namestart = tables[4].find("\"")
		nameend = tables[4].find("\"", namestart+1)
		while namestart != -1: # deaths
			names = tables[4][namestart+1:nameend]
			names = names.split(", ")
			for i in range(len(names)):
				for j in range(len(stats)):
					if playerStats[j]["playerName"] == names[i]:
						playerStats[j]["deaths"] += 1
			namestart = tables[4].find("\"", nameend+1)
			nameend = tables[4].find("\"", namestart+1)

	if len(tables) > 5:
		namestart = tables[5].find("\"")
		nameend = tables[5].find("\"", namestart+1)
		while namestart != -1: # resses
			names = tables[5][namestart+1:nameend]
			names = names.split(", ")
			for i in range(len(names)):
				for j in range(len(stats)):
					if playerStats[j]["playerName"] == names[i]:
						playerStats[j]["resurrections"] += 1
			namestart = tables[5].find("\"", nameend+1)
			nameend = tables[5].find("\"", namestart+1)

	if instance == "Onyxia's Lair":
		fullclear = 1
		finished = 1
	else:
		fullclear = 0
		finished = 0

	if bossName == "Razorgore the Untamed":
		theStats = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	else:
		theStats = deaths, resurrections, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, raidDPS, totalInterrupts, totalDispels, raidHPS

	# Ragnaros stats reset after submerge, so don't include fights that have a submerge
	if bossName == "Ragnaros" and totalDmgDone < 1099230*0.95: # If a ragnaros fight has substantially less dmgDone than ragnaros has hp there was probably a submerge
		writeToLog(INFO, "Ignoring " + bossName + " by " + guildName + " at " + killedAt + " due to probable phase 2", twinstarID)
		updateLatest(twinstarID)
		return
	elif bossName in fourHorsemen:
		if totalDmgDone < (592918 + 532960 + 499650 + 499650)*0.999: # Not the last horseman killed
			writeToLog(INFO, "Ignoring " + bossName + " by " + guildName + " at " + killedAt + " due to probably not being the last horseman", twinstarID)
			updateLatest(twinstarID)
			return
		else:
			bossName = "Four Horsemen"

	if bossName not in specialBosses:
		if raidID is not None:
			c.execute('''	SELECT finished FROM raids_guild
							WHERE raidID=?''',
							(raidID,))
			alreadyFinished = c.fetchall()[0][0]
			c.execute('''	SELECT COUNT(*) FROM encounters_guild
							WHERE raidID=?''',
							(raidID,))
			res = int(c.fetchall()[0][0])
			if res == bossesInInstance[instance]-1: # This kill makes it a full clear
				fullclear = 1
				if not alreadyFinished:
					finished = 1
			modifyRaidsGuild( c, raidID, finished, killedAtEpoch, *theStats )
		else:
			insertIntoRaidsGuild( c, guildName, faction, instance, finished, resetNo, killedAtEpoch-int(fightLength), killedAtEpoch, *theStats )
			raidID = fetchRaidID( c, guildName, faction, instance, resetNo, *theStats )

		insertIntoEncountersGuild( c, twinstarID, raidID, guildName, faction, bossName, instance, killedAtEpoch, fightLength, deaths, resurrections, len(stats), resetNo, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, raidDPS, totalInterrupts, totalDispels, raidHPS )

		for i in range(len(playerStats)):
			insertIntoEncountersPlayer( c, twinstarID, playerStats[i]["playerName"], playerStats[i]["playerClass"], playerStats[i]["playerRace"], playerStats[i]["playerItemLevel"], playerStats[i]["usefullTime"], playerStats[i]["dmgDone"], playerStats[i]["healingDone"], playerStats[i]["dmgTaken"], playerStats[i]["healingTaken"], playerStats[i]["overhealingDone"], playerStats[i]["dmgAbsorbed"], playerStats[i]["absorbDone"], playerStats[i]["dps"], playerStats[i]["interrupts"], playerStats[i]["dispels"], playerStats[i]["hps"], playerStats[i]["deaths"], playerStats[i]["resurrections"] )

			if bossName == "Razorgore the Untamed": # Don't include Razorgore stats in average
				theStats = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			else:
				theStats = (fightLength, playerStats[i]["playerItemLevel"], playerStats[i]["dmgDone"], playerStats[i]["healingDone"], playerStats[i]["dmgTaken"], playerStats[i]["healingTaken"], playerStats[i]["overhealingDone"], playerStats[i]["dmgAbsorbed"], playerStats[i]["absorbDone"], playerStats[i]["dps"], playerStats[i]["interrupts"], playerStats[i]["dispels"], playerStats[i]["hps"], playerStats[i]["deaths"], playerStats[i]["resurrections"])

			if raidsPlayerExists(c, raidID, playerStats[i]["playerName"]):
				modifyRaidsPlayer( c, raidID, playerStats[i]["playerName"], *theStats )
			else:
				insertIntoRaidsPlayer( c, raidID, playerStats[i]["playerName"], playerStats[i]["playerClass"], playerStats[i]["playerRace"], *theStats )

	else: # Special boss
		if raidID is None:
			writeToLog(FAIL, "Special boss " + bossName + " may not be initial boss", twinstarID)
			exit(1)

		if bossName == "Eye of C'Thun":
			c.execute('''	SELECT encounterID FROM special_guild
							WHERE raidID=? and bossName=?''',
							(raidID, "Eye of C'Thun"))
			res = c.fetchall()
			if len(res) != 0: # There was an attempt that finished phase one, but not phase two. Remove old phase one data
				c.execute('''	DELETE FROM special_guild
								WHERE raidID=? and bossName=?''',
								(raidID, bossName))
				c.execute('''	DELETE FROM special_player
								WHERE encounterID=?''',
								(res[0][0],))

			c.execute('''	INSERT INTO special_guild
							VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
							(twinstarID, raidID, guildName, faction, bossName, instance, killedAtEpoch, fightLength, deaths, resurrections, len(stats), resetNo, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, raidDPS, totalInterrupts, totalDispels, raidHPS))
			for i in range(len(playerStats)):
				c.execute('''	INSERT INTO special_player
								VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )''',
								(twinstarID, playerStats[i]["playerName"], playerStats[i]["playerClass"], playerStats[i]["playerRace"], playerStats[i]["playerItemLevel"], playerStats[i]["usefullTime"], playerStats[i]["dmgDone"], playerStats[i]["healingDone"], playerStats[i]["dmgTaken"], playerStats[i]["healingTaken"], playerStats[i]["overhealingDone"], playerStats[i]["dmgAbsorbed"], playerStats[i]["absorbDone"], playerStats[i]["dps"], playerStats[i]["interrupts"], playerStats[i]["dispels"], playerStats[i]["hps"], playerStats[i]["deaths"], playerStats[i]["resurrections"]))

			conn.commit()
			conn.close()
			writeToLog(INFO, "Prepared info for " + bossName + " by " + guildName + " at " + killedAt + ", raid " + raidID, twinstarID)
			updateLatest(twinstarID)
			return # Do not update b5o6 since we didn't add a "real" boss

		elif bossName == "C'Thun":
			c.execute('''	SELECT bossName FROM encounters_guild
							WHERE raidID=?''',
							(raidID,)) # get bossnames instead of count since we might need the names
			res = c.fetchall()
			if len(res) == bossesInInstance[instance]-1: # This kill makes it a full clear
				finished = 1
				fullclear = 1
			else:
				j = len(skippable)
				for i in range(len(skippable)):
					if (skippable[i],) in res:
						j -= 1
				if len(res) == bossesInInstance[instance]-1-j:
					finished = 1
			c.execute('''	SELECT fightLength, deaths, resurrections, noParticipants, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, raidDPS, totalInterrupts, totalDispels, raidHPS FROM special_guild
							WHERE raidID=? and bossName=?''',
							(raidID, "Eye of C'Thun"))
			res = c.fetchall()
			if len(res) == 0:
				writeToLog(FAIL, "There is no Eye of C'Thun info for this C'Thun kill", twinstarID)
				exit(1)
			eyeStats = res[0]
			realFightLength = fightLength+eyeStats[0]
			realDeaths = deaths+eyeStats[1]
			realResurrections = resurrections+eyeStats[2]
			realNoParticipants = max(len(stats), eyeStats[3])
			realAverageItemLevel = (averageItemLevel+eyeStats[4])/2
			realTotalDmgDone = totalDmgDone+eyeStats[5]
			realTotalHealingDone = totalHealingDone+eyeStats[6]
			realTotalDmgTaken = totalDmgTaken+eyeStats[7]
			realTotalHealingtaken = totalHealingTaken+eyeStats[8]
			realTotalOverhealingDone = totalOverhealingDone+eyeStats[9]
			realTotalDmgAbsorbed = totalDmgAbsorbed+eyeStats[10]
			realTotalAbsorbDone = totalAbsorbDone+eyeStats[11]
			realRaidDPS = (raidDPS*fightLength+eyeStats[12]*eyeStats[0])/(fightLength+eyeStats[0])
			realTotalInterrupts = totalInterrupts+eyeStats[13]
			realTotalDispels = totalDispels+eyeStats[14]
			realRaidHPS = (raidHPS*fightLength+eyeStats[15]*eyeStats[0])/(fightLength+eyeStats[0])

			insertIntoEncountersGuild( c, twinstarID, raidID, guildName, faction, bossName, instance, killedAtEpoch, realFightLength, realDeaths, realResurrections, realNoParticipants, resetNo, realAverageItemLevel, realTotalDmgDone, realTotalHealingDone, realTotalDmgTaken, realTotalHealingtaken, realTotalOverhealingDone, realTotalDmgAbsorbed, realTotalAbsorbDone, realRaidDPS, realTotalInterrupts, realTotalDispels, realRaidHPS )
			modifyRaidsGuild( c, raidID, finished, killedAtEpoch, realDeaths, realResurrections, realAverageItemLevel, realTotalDmgDone, realTotalHealingDone, realTotalDmgTaken, realTotalHealingtaken, realTotalOverhealingDone, realTotalDmgAbsorbed, realTotalAbsorbDone, realRaidDPS, realTotalInterrupts, realTotalDispels, realRaidHPS )

			c.execute('''	SELECT playerName, playerClass, playerRace, playerItemLevel, usefullTime, dmgDone, healingDone, dmgTaken, healingTaken, overhealingDone, dmgAbsorbed, absorbDone, interrupts, dispels, special_player.deaths, special_player.resurrections
							FROM special_player
							JOIN special_guild USING (encounterID)
							WHERE raidID=?''',
							(raidID,))
			res = c.fetchall()

			playerArray = {}
			for i in range(len(res)): # Phase one stats
				playerArray[res[i][0]] = [
					res[i][1], # class
					res[i][2], # race
					res[i][3], # itemLevel
					res[i][4], # usefullTime
					res[i][5], # dmgDone
					res[i][6], # healingDone
					res[i][7], # dmgTaken
					res[i][8], # healingTaken
					res[i][9], # overhealingDone
					res[i][10], # dmgAbsorbed
					res[i][11], # absorbDone
					res[i][5]/realFightLength, # dps, as if they won't attend p2
					res[i][12], # interrupts
					res[i][13], # dispels
					res[i][6]/realFightLength, # hps, as if they won't attend p2
					res[i][14], # deaths
					res[i][15] # resurrections
				]

			for i in range(len(playerStats)): # Phase two stats
				if playerStats[i]["playerName"] not in playerArray: # Someone who attended phase two but did not attend phase one
					playerArray[playerStats[i]["playerName"]] = [
						playerStats[i]["playerClass"],
						playerStats[i]["playerRace"],
						playerStats[i]["playerItemLevel"],
						playerStats[i]["usefullTime"],
						playerStats[i]["dmgDone"],
						playerStats[i]["healingDone"],
						playerStats[i]["dmgTaken"],
						playerStats[i]["healingTaken"],
						playerStats[i]["overhealingDone"],
						playerStats[i]["dmgAbsorbed"],
						playerStats[i]["absorbDone"],
						int(playerStats[i]["dmgDone"])/realFightLength,
						playerStats[i]["interrupts"],
						playerStats[i]["dispels"],
						int(playerStats[i]["healingDone"])/realFightLength,
						playerStats[i]["deaths"],
						playerStats[i]["resurrections"]
					]
				else:
					playerArray[playerStats[i]["playerName"]] = [
						playerArray[playerStats[i]["playerName"]][0], # class
						playerArray[playerStats[i]["playerName"]][1], # race
						(playerArray[playerStats[i]["playerName"]][2]+float(playerStats[i]["playerItemLevel"]))/2,
						playerArray[playerStats[i]["playerName"]][3]+int(playerStats[i]["usefullTime"]),
						playerArray[playerStats[i]["playerName"]][4]+int(playerStats[i]["dmgDone"]),
						playerArray[playerStats[i]["playerName"]][5]+int(playerStats[i]["healingDone"]),
						playerArray[playerStats[i]["playerName"]][6]+int(playerStats[i]["dmgTaken"]),
						playerArray[playerStats[i]["playerName"]][7]+int(playerStats[i]["healingTaken"]),
						playerArray[playerStats[i]["playerName"]][8]+int(playerStats[i]["overhealingDone"]),
						playerArray[playerStats[i]["playerName"]][9]+int(playerStats[i]["dmgAbsorbed"]),
						playerArray[playerStats[i]["playerName"]][10]+int(playerStats[i]["absorbDone"]),
						playerArray[playerStats[i]["playerName"]][11]+(int(playerStats[i]["dmgDone"])/realFightLength), # remember that old stat is already divided by time
						playerArray[playerStats[i]["playerName"]][12]+int(playerStats[i]["interrupts"]),
						playerArray[playerStats[i]["playerName"]][13]+int(playerStats[i]["dispels"]),
						playerArray[playerStats[i]["playerName"]][14]+(int(playerStats[i]["healingDone"])/realFightLength), # remember that old stat is already divided by time
						playerArray[playerStats[i]["playerName"]][15]+int(playerStats[i]["deaths"]),
						playerArray[playerStats[i]["playerName"]][16]+int(playerStats[i]["resurrections"])
					]

			for name in playerArray: # Merged stats
				insertIntoEncountersPlayer( c, twinstarID, name, *(playerArray[name]) )

				if raidsPlayerExists(c, raidID, name):
					modifyRaidsPlayer( c, raidID, name, realFightLength, playerArray[name][2], *(playerArray[name][4:]) )
				else: # This is their first boss of the raid
					insertIntoRaidsPlayer( c, raidID, name, playerArray[name][0], playerArray[name][1], realFightLength, *(playerArray[name][3:]))

		elif bossName in bugTrioBosses:
			pos = content.find("var chart_data = [[[") + 20
			pos2 = 0
			i = 0
			chartrepr = []
			chartrepr.append("")
			while True:
				pos2 = content.find(",", pos)
				pos = content.find("]", pos2)
				rval = content[pos2+1:pos]
				if content.find("]]",pos2) == pos: # New row
					i+=1
					if i >= 4:
						break
					chartrepr.append("")
				else:
					chartrepr[i] += " " + rval # Don't include the last element of the row, since we cut in the middle of this segment
				pos = content.find("[", pos+1)

			c.execute('''	SELECT bossName, pickledChart
							FROM bugTrio
							WHERE raidID=?''',
							(raidID,))
			res = c.fetchall()

			if len(res) == 0: # This is the first bug trio boss killed in this raid
				c.execute('''	INSERT INTO bugTrio
								VALUES (?, ?, ?)''',
								(raidID, bossName, pickle.dumps(chartrepr)))
				writeToLog(INFO, "Prepared info for " + bossName + " by " + guildName + " at " + killedAt + ", raid " + raidID, twinstarID)
				updateLatest(twinstarID)
				conn.commit()
				conn.close()
				return

			elif len(res) == 1:
				otherName = res[0][0]
				otherChart = pickle.loads(res[0][1])
				isCont = True
				if otherName != bossName:
					for i in range(len(chartrepr)):
						if not chartrepr[i].find(otherChart[i]) == 0:
							isCont = False
							break
				else:
					isCont = False

				if not isCont: # The stored data refers to a separate attempt (which wiped), so remove it
					c.execute('''	DELETE FROM bugTrio
									WHERE raidID=?''',
									(raidID,))

				c.execute('''	INSERT INTO bugTrio
								VALUES (?, ?, ?)''',
								(raidID, bossName, pickle.dumps(chartrepr)))
				writeToLog(INFO, "Prepared info for " + bossName + " by " + guildName + " at " + killedAt + ", raid " + raidID, twinstarID)
				updateLatest(twinstarID)
				conn.commit()
				conn.close()
				return

			elif len(res) == 2:
				nameA = res[0][0]
				nameB = res[1][0]
				chartA = pickle.loads(res[0][1])
				chartB = pickle.loads(res[1][1])
				isCont = True
				if nameA != bossName and nameB != bossName:
					for i in range(len(chartrepr)):
						if not chartrepr[i].find(chartA[i]) == 0 or not chartrepr[i].find(chartB[i]) == 0:
							isCont = False
							break
				else:
					isCont = False

				c.execute('''	DELETE FROM bugTrio
								WHERE raidID=?''',
								(raidID,)) # Delete stored data since it's now useless

				if not isCont: # The old data refered to another attempt, meaning this kill is a new attempt and we need to store this data
					c.execute('''	INSERT INTO bugTrio
									VALUES (?, ?, ?)''',
									(raidID, bossName, pickle.dumps(chartrepr)))
					writeToLog(INFO, "Prepared info for " + bossName + " by " + guildName + " at " + killedAt + ", raid " + raidID, twinstarID)
					updateLatest(twinstarID)
					conn.commit()
					conn.close()
					return

				else: # We now have three bosskills with the charts being subsets of eachother. It is very likely these refer to the same attempt, so we will assume they do
					bossName = "Bug Trio"
					c.execute('''	SELECT COUNT(*) FROM encounters_guild
									WHERE raidID=?''',
									(raidID,))
					res = int(c.fetchall()[0][0])
					if res == bossesInInstance[instance]-1: # This kill makes it a full clear
						fullclear = 1

					modifyRaidsGuild( c, raidID, finished, killedAtEpoch, *theStats ) # Conveniently, the stats are already merged in the final kill
					insertIntoEncountersGuild( c, twinstarID, raidID, guildName, faction, bossName, instance, killedAtEpoch, fightLength, deaths, resurrections, len(stats), resetNo, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, raidDPS, totalInterrupts, totalDispels, raidHPS )

					for i in range(len(playerStats)):
						insertIntoEncountersPlayer( c, twinstarID, playerStats[i]["playerName"], playerStats[i]["playerClass"], playerStats[i]["playerRace"], playerStats[i]["playerItemLevel"], playerStats[i]["usefullTime"], playerStats[i]["dmgDone"], playerStats[i]["healingDone"], playerStats[i]["dmgTaken"], playerStats[i]["healingTaken"], playerStats[i]["overhealingDone"], playerStats[i]["dmgAbsorbed"], playerStats[i]["absorbDone"], playerStats[i]["dps"], playerStats[i]["interrupts"], playerStats[i]["dispels"], playerStats[i]["hps"], playerStats[i]["deaths"], playerStats[i]["resurrections"] )

						theStats = (fightLength, playerStats[i]["playerItemLevel"], playerStats[i]["dmgDone"], playerStats[i]["healingDone"], playerStats[i]["dmgTaken"], playerStats[i]["healingTaken"], playerStats[i]["overhealingDone"], playerStats[i]["dmgAbsorbed"], playerStats[i]["absorbDone"], playerStats[i]["dps"], playerStats[i]["interrupts"], playerStats[i]["dispels"], playerStats[i]["hps"], playerStats[i]["deaths"], playerStats[i]["resurrections"])

						if raidsPlayerExists(c, raidID, playerStats[i]["playerName"]):
							modifyRaidsPlayer( c, raidID, playerStats[i]["playerName"], *theStats )
						else:
							insertIntoRaidsPlayer( c, raidID, playerStats[i]["playerName"], playerStats[i]["playerClass"], playerStats[i]["playerRace"], *theStats )

			else: # Too many bug kills, there has been an error in determining which kills belonged to the same attempt, and the database is now corrupt
				writeToLog(FAIL, "There were more than two previous bug kills for raid " + raidID, twinstarID)
				exit(1)

		else:
			writeToLog(FAIL, "No case for special boss " + bossName, twinstarID)
			exit(1)


	conn.commit()
	conn.close()
	conn = sqlite3.connect(myPath + "logs.db")
	c = conn.cursor()

	for i in range(len(stats)):
		updateB5o6EncountersPlayer(c, playerStats[i]["playerName"], bossName, instance)

	updateB5o6EncountersGuild(c, guildName, bossName, faction, instance)

	if fullclear:
		if bossName == "C'Thun": # Use the combined p1 and p2 array instead of the normal one
			for name in playerArray:
				updateB5o6RaidsPlayer(c, name, instance)
		else:
			for i in range(len(stats)):
				updateB5o6RaidsPlayer(c, playerStats[i]["playerName"], instance)
		updateB5o6RaidsGuildStats(c, guildName, instance, faction)

	if finished:
		updateB5o6RaidsGuildCleartime(c, guildName, instance, faction)
		
	conn.commit()
	conn.close()

	writeToLog(INFO, "Inserted info for " + bossName + " by " + guildName + " at " + killedAt + ", raid " + raidID, twinstarID)
	updateLatest(twinstarID)

	return (
		bossName,
		instance if fullclear else None,
		instance if finished else None
		)

def updateLatest(twinstarID):
	with open(myPath + "latest.txt", "w") as file:
		file.write(str(twinstarID) + "\n")

def writeToLog(messageLevel, toWrite, twinstarID):
	if logLevel <= messageLevel:
		levelText = logMessage[messageLevel]
		with open(myPath + "log.txt", "a") as log:
			msg = levelText + " - " + str(twinstarID) + ": " + toWrite
			log.write(msg + "\n")
			print(msg)

def insertIntoEncountersPlayer( c, twinstarID, playerName, playerClass, playerRace, playerItemLevel, usefullTime, dmgDone, healingDone, dmgTaken, healingTaken, overhealingDone, dmgAbsorbed, absorbDone, dps, interrupts, dispels, hps, deaths, resurrections ):
	theStats = ( twinstarID, playerName, playerClass, playerRace, playerItemLevel, usefullTime, dmgDone, healingDone, dmgTaken, healingTaken, overhealingDone, dmgAbsorbed, absorbDone, dps, interrupts, dispels, hps, deaths, resurrections )
	c.execute('''	INSERT INTO encounters_player
					VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )''',
					theStats)

def insertIntoRaidsPlayer( c, raidID, playerName, playerClass, playerRace, fightLength, playerItemLevel, dmgDone, healingDone, dmgTaken, healingTaken, overhealingDone, dmgAbsorbed, absorbDone, dps, interrupts, dispels, hps, deaths, resurrections ):
	theStats = raidID, playerName, playerClass, playerRace, fightLength, playerItemLevel, dmgDone, healingDone, dmgTaken, healingTaken, overhealingDone, dmgAbsorbed, absorbDone, dps, interrupts, dispels, hps, deaths, resurrections, 1
	c.execute('''	INSERT INTO raids_player
					VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )''',
				theStats)

def modifyRaidsPlayer( c, raidID, playerName, fightLength, playerItemLevel, dmgDone, healingDone, dmgTaken, healingTaken, overhealingDone, dmgAbsorbed, absorbDone, dps, interrupts, dispels, hps, deaths, resurrections ):
	raidsPlayerIdentifier = ( raidID, playerName )
	c.execute('''	SELECT totalFightLength, playerItemLevel, dmgDone, healingDone, dmgTaken, healingTaken, overhealingDone, dmgAbsorbed, absorbDone, dps, interrupts, dispels, hps, deaths, resurrections, numBosses FROM raids_player
					WHERE raidID=? AND playerName=?''',
					raidsPlayerIdentifier)
	oldStats = c.fetchall()
	numBossesInRaid = int(oldStats[0][15])
	c.execute('''	SELECT instance FROM raids_guild
					WHERE raidID=?''',
					(raidID,))
	newNumBosses = numBossesInRaid + 1
	if c.fetchone()[0] == "Blackwing Lair": # Don't include Razorgore stats in average
		numBossesInRaid -= 1
	newDPS = (float(oldStats[0][9]*oldStats[0][0])+(float(dps)*fightLength))/(oldStats[0][0]+fightLength)
	newHPS = (float(oldStats[0][12]*oldStats[0][0])+(float(hps)*fightLength))/(oldStats[0][0]+fightLength)
	raidsPlayerUpdater = ( int(oldStats[0][0])+int(fightLength), (float(oldStats[0][1])*numBossesInRaid+float(playerItemLevel))/(numBossesInRaid+1), int(oldStats[0][2])+int(dmgDone), int(oldStats[0][3])+int(healingDone), int(oldStats[0][4])+int(dmgTaken), int(oldStats[0][5])+int(healingTaken), int(oldStats[0][6])+int(overhealingDone), int(oldStats[0][7])+int(dmgAbsorbed), int(oldStats[0][8])+int(absorbDone), newDPS, int(oldStats[0][10])+int(interrupts), int(oldStats[0][11])+int(dispels), newHPS, int(oldStats[0][13])+int(deaths), int(oldStats[0][14])+int(resurrections), newNumBosses, raidID, playerName )
	c.execute('''	UPDATE raids_player
					SET totalFightLength=?, playerItemLevel=?, dmgDone=?, healingDone=?, dmgTaken=?, healingTaken=?, overhealingDone=?, dmgAbsorbed=?, absorbDone=?, dps=?, interrupts=?, dispels=?, hps=?, deaths=?, resurrections=?, numBosses=?
					WHERE raidID=? AND playerName=?''',
					raidsPlayerUpdater)

def insertIntoEncountersGuild( c, twinstarID, raidID, guildName, faction, bossName, instance, killedAt, fightLength, deaths, resurrections, noParticipants, resetNo, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, raidDPS, totalInterrupts, totalDispels, raidHPS ):
	theStats = ( twinstarID, raidID, guildName, faction, bossName, instance, killedAt, fightLength, deaths, resurrections, noParticipants, resetNo, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, raidDPS, totalInterrupts, totalDispels, raidHPS )
	c.execute('''	INSERT INTO encounters_guild
					VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )''',
					theStats)

def insertIntoRaidsGuild( c, guildName, faction, instance, finished, resetNo, startTime, endTime, totalDeaths, totalResurrections, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, raidDPS, totalInterrupts, totalDispels, raidHPS ):
	theStats = ( guildName, faction, instance, finished, resetNo, startTime, endTime, totalDeaths, totalResurrections, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, raidDPS, totalInterrupts, totalDispels, raidHPS, 1 )
	c.execute('''	INSERT INTO raids_guild
					VALUES ( NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )''',
					theStats)

def modifyRaidsGuild( c, raidID, finished, endTime, totalDeaths, totalResurrections, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, averageRaidDPS, totalInterrupts, totalDispels, averageRaidHPS ):
	raidsGuildIdentifier = (raidID,)
	c.execute('''	SELECT totalDeaths, totalResurrections, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, averageRaidDPS, totalInterrupts, totalDispels, averageRaidHPS, numBosses, finished, endTime, instance FROM raids_guild
					WHERE raidID=?''',
					raidsGuildIdentifier)
	oldStats = c.fetchall()
	numBossesInRaid = oldStats[0][14]
	newNumBosses = numBossesInRaid + 1
	if oldStats[0][17] == "Blackwing Lair": # Razorgore stats not included
		numBossesInRaid -= 1
	if oldStats[0][15] == 1: # Raid is already finished
		endTime = oldStats[0][16] # Don't update the end time
		finished = 1
	raidsGuildUpdater = ( endTime, oldStats[0][0]+totalDeaths, oldStats[0][1]+totalResurrections, (oldStats[0][2]*numBossesInRaid+averageItemLevel)/(numBossesInRaid+1), oldStats[0][3]+totalDmgDone, oldStats[0][4]+totalHealingDone, oldStats[0][5]+totalDmgTaken, oldStats[0][6]+totalHealingTaken, oldStats[0][7]+totalOverhealingDone, oldStats[0][8]+totalDmgAbsorbed, oldStats[0][9]+totalAbsorbDone, (oldStats[0][10]*numBossesInRaid+averageRaidDPS)/(numBossesInRaid+1), oldStats[0][11]+totalInterrupts, oldStats[0][12]+totalDispels, (oldStats[0][13]*numBossesInRaid+averageRaidHPS)/(numBossesInRaid+1), newNumBosses, finished, raidID )
	c.execute('''	UPDATE raids_guild
					SET endTime=?, totalDeaths=?, totalResurrections=?, averageItemLevel=?, totalDmgDone=?, totalHealingDone=?, totalDmgTaken=?, totalHealingTaken=?, totalOverhealingDone=?, totalDmgAbsorbed=?, totalAbsorbDone=?, averageRaidDPS=?, totalInterrupts=?, totalDispels=?, averageRaidHPS=?, numBosses=?, finished=?
					WHERE raidID=?''',
					raidsGuildUpdater)

def updateB5o6EncountersPlayer(c, player, boss, instance):
	identifiers = (player, boss)
	c.execute('''	SELECT
					playerClass, playerRace, guildName, faction,
					(sum(playerItemLevel)-min(playerItemLevel))/5 AS b5o6_playerItemLevel,
					(sum(usefullTime)-min(usefullTime))/5 AS b5o6_usefullTime,
					(sum(dmgDone)-min(dmgDone))/5 AS b5o6_dmgDone,
					(sum(healingDone)-min(healingDone))/5 AS b5o6_healingDone,
					(sum(dmgTaken)-min(dmgTaken))/5 AS b5o6_dmgTaken,
					(sum(healingTaken)-min(healingTaken))/5 AS b5o6_healingTaken,
					(sum(overhealingDone)-min(overhealingDone))/5 AS b5o6_overhealingDone,
					(sum(dmgAbsorbed)-min(dmgAbsorbed))/5 AS b5o6_dmgAbsorbed,
					(sum(absorbDone)-min(absorbDone))/5 AS b5o6_absorbDone,
					(sum(dps)-min(dps))/5 AS b5o6_dps,
					(sum(interrupts)-min(interrupts))/5 AS b5o6_interrupts,
					(sum(dispels)-min(dispels))/5 AS b5o6_dispels,
					(sum(hps)-min(hps))/5 AS b5o6_hps,
					(sum(deaths)-min(deaths))/5 AS b5o6_deaths,
					(sum(resurrections)-min(resurrections))/5 AS b5o6_resurrections
					FROM (
						SELECT
						playerClass, playerRace, guildName, faction, playerItemLevel, usefullTime, dmgDone, healingDone, dmgTaken, healingTaken, overhealingDone, dmgAbsorbed, absorbDone, dps, interrupts, dispels, hps, encounters_player.deaths AS deaths, encounters_player.resurrections AS resurrections
						FROM encounters_player JOIN encounters_guild USING (encounterid)
						WHERE playerName=? AND bossName=?
						ORDER BY encounterID DESC
						LIMIT 6
					)''',
					identifiers)
	b5o6 = c.fetchall()
	newStats = b5o6[0]
	c.execute('''	SELECT numKills FROM b5o6_encounters_player
					WHERE playerName=? AND bossName=?
					LIMIT 1''',
					identifiers)
	numKills = c.fetchall()
	if len(numKills) == 0: # First b5o6
		c.execute('''	INSERT INTO b5o6_encounters_player
						VALUES (?, ?, ?, 1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
						''',
						identifiers+(instance,)+newStats)
	else:
		c.execute('''	UPDATE b5o6_encounters_player
						SET numKills=?, playerClass=?, playerRace=?, guildName=?, faction=?, playerItemLevel=?, usefullTime=?, dmgDone=?, healingDone=?, dmgTaken=?, healingTaken=?, overhealingDone=?, dmgAbsorbed=?, absorbDone=?, dps=?, interrupts=?, dispels=?, hps=?, deaths=?, resurrections=?
						WHERE playerName=? AND bossName=?''',
						(numKills[0][0]+1,)+newStats+identifiers)

def updateB5o6EncountersGuild(c, guildName, bossName, faction, instance):
	identifiers = (guildName, bossName)
	c.execute('''	SELECT
					(sum(fightlength)-max(fightlength))/5 AS b5o6_fightLength,
					(sum(deaths)-min(deaths))/5 AS b5o6_deaths,
					(sum(resurrections)-min(resurrections))/5 AS b5o6_resurrections,
					(sum(noParticipants)-min(noParticipants))/5 AS b5o6_noParticipants,
					(sum(averageItemLevel)-min(averageItemLevel))/5 AS b5o6_averageItemLevel,
					(sum(totalDmgDone)-min(totalDmgDone))/5 AS b5o6_totalDmgDone,
					(sum(totalHealingDone)-min(totalHealingDone))/5 AS b5o6_totalHealingDone,
					(sum(totalDmgTaken)-min(totalDmgTaken))/5 AS b5o6_totalDmgTaken,
					(sum(totalHealingTaken)-min(totalHealingTaken))/5 AS b5o6_totalHealingTaken,
					(sum(totalOverhealingDone)-min(totalOverhealingDone))/5 AS b5o6_totalOverhealingDone,
					(sum(totalDmgAbsorbed)-min(totalDmgAbsorbed))/5 AS b5o6_totalDmgAbsorbed,
					(sum(totalAbsorbDone)-min(totalAbsorbDone))/5 AS b5o6_totalAbsorbDone,
					(sum(raidDPS)-min(raidDPS))/5 AS b5o6_raidDPS,
					(sum(totalInterrupts)-min(totalInterrupts))/5 AS b5o6_totalInterrupts,
					(sum(totalDispels)-min(totalDispels))/5 AS b5o6_totalDispels,
					(sum(raidHPS)-min(raidHPS))/5 AS b5o6_raidHPS
					FROM (
						SELECT fightLength, deaths, resurrections, noParticipants, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, raidDPS, totalInterrupts, totalDispels, raidHPS
						FROM encounters_guild
						WHERE guildName=? AND bossName=?
						ORDER BY encounterID DESC
						LIMIT 6
					)
					''',
					identifiers)
	b5o6 = c.fetchall()
	newStats = b5o6[0]
	c.execute('''	SELECT numKills FROM b5o6_encounters_guild
					WHERE guildName=? AND bossName=?
					LIMIT 1''',
					identifiers)
	numKills = c.fetchall()
	if len(numKills) == 0: # First b5o6
		c.execute('''	INSERT INTO b5o6_encounters_guild
						VALUES (?, ?, ?, ?, 1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
						''',
						identifiers+(faction,instance)+newStats)
	else:
		c.execute('''	UPDATE b5o6_encounters_guild
						SET numKills=?, fightLength=?, deaths=?, resurrections=?, noParticipants=?, averageItemLevel=?, totalDmgDone=?, totalHealingDone=?, totalDmgTaken=?, totalHealingTaken=?, totalOverhealingDone=?, totalDmgAbsorbed=?, totalAbsorbDone=?, raidDPS=?, totalInterrupts=?, totalDispels=?, raidHPS=?
						WHERE guildName=? AND bossName=?''',
						(numKills[0][0]+1,)+newStats+identifiers)

def updateB5o6RaidsPlayer(c, player, instance):
	b5o6_identifiers = (player, instance)
	c.execute('''	SELECT
					playerClass, playerRace, guildName, faction,
					(sum(playerItemLevel)-min(playerItemLevel))/5 AS b5o6_playerItemLevel,
					(sum(dmgDone)-min(dmgDone))/5 AS b5o6_dmgDone,
					(sum(healingDone)-min(healingDone))/5 AS b5o6_healingDone,
					(sum(dmgTaken)-min(dmgTaken))/5 AS b5o6_dmgTaken,
					(sum(healingTaken)-min(healingTaken))/5 AS b5o6_healingTaken,
					(sum(overhealingDone)-min(overhealingDone))/5 AS b5o6_overhealingDone,
					(sum(dmgAbsorbed)-min(dmgAbsorbed))/5 AS b5o6_dmgAbsorbed,
					(sum(absorbDone)-min(absorbDone))/5 AS b5o6_absorbDone,
					(sum(dps)-min(dps))/5 AS b5o6_dps,
					(sum(interrupts)-min(interrupts))/5 AS b5o6_interrupts,
					(sum(dispels)-min(dispels))/5 AS b5o6_dispels,
					(sum(hps)-min(hps))/5 AS b5o6_hps,
					(sum(deaths)-min(deaths))/5 AS b5o6_deaths,
					(sum(resurrections)-min(resurrections))/5 AS b5o6_resurrections
					FROM (
						SELECT playerClass, playerRace, guildName, faction, playerItemLevel, dmgDone, healingDone, dmgTaken, healingTaken, overhealingDone, dmgAbsorbed, absorbDone, dps, interrupts, dispels, hps, deaths, resurrections
						FROM raids_player JOIN raids_guild USING (raidID)
						WHERE playerName=? AND instance=? AND raids_player.numBosses=?
						ORDER BY raidID DESC
						LIMIT 6
					)
					''',
					b5o6_identifiers+(bossesInInstance[instance],))
	b5o6 = c.fetchall()
	newStats = b5o6[0]
	if None in newStats: # This player only participated in part of the raid and therefore has no stats
		return
	c.execute('''	SELECT numClears FROM b5o6_raids_player
					WHERE playerName=? AND instance=?
					LIMIT 1''',
					b5o6_identifiers)
	numClears = c.fetchall()
	if len(numClears) == 0: # First b5o6
		c.execute('''	INSERT INTO b5o6_raids_player
						VALUES (?, ?, 1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
						''',
						b5o6_identifiers+newStats)
	else:
		c.execute('''	UPDATE b5o6_raids_player
						SET numClears=?, playerClass=?, playerRace=?, guildName=?, faction=?, playerItemLevel=?, dmgDone=?, healingDone=?, dmgTaken=?, healingTaken=?, overhealingDone=?, dmgAbsorbed=?, absorbDone=?, dps=?, interrupts=?, dispels=?, hps=?, deaths=?, resurrections=?
						WHERE playerName=? AND instance=?''',
						(numClears[0][0]+1,)+newStats+b5o6_identifiers)

def updateB5o6RaidsGuildStats(c, guildName, instance, faction):
	b5o6_identifiers = (guildName, instance)
	c.execute('''	SELECT
					(sum(totalDeaths)-min(totalDeaths))/5 AS b5o6_totalDeaths,
					(sum(totalResurrections)-min(totalResurrections))/5 AS b5o6_totalResurrections,
					(sum(averageItemLevel)-min(averageItemLevel))/5 AS b5o6_averageItemLevel,
					(sum(totalDmgDone)-min(totalDmgDone))/5 AS b5o6_totalDmgDone,
					(sum(totalHealingDone)-min(totalHealingDone))/5 AS b5o6_totalHealingDone,
					(sum(totalDmgTaken)-min(totalDmgTaken))/5 AS b5o6_totalDmgTaken,
					(sum(totalHealingTaken)-min(totalHealingTaken))/5 AS b5o6_totalHealingTaken,
					(sum(totalOverhealingDone)-min(totalOverhealingDone))/5 AS b5o6_totalOverhealingDone,
					(sum(totalDmgAbsorbed)-min(totalDmgAbsorbed))/5 AS b5o6_totalDmgAbsorbed,
					(sum(totalAbsorbDone)-min(totalAbsorbDone))/5 AS b5o6_totalAbsorbDone,
					(sum(averageRaidDPS)-min(averageRaidDPS))/5 AS b5o6_averageRaidDPS,
					(sum(totalInterrupts)-min(totalInterrupts))/5 AS b5o6_totalInterrupts,
					(sum(totalDispels)-min(totalDispels))/5 AS b5o6_totalDispels,
					(sum(averageRaidHPS)-min(averageRaidHPS))/5 AS b5o6_averageRaidHPS
					FROM (
						SELECT totalDeaths, totalResurrections, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, averageRaidDPS, totalInterrupts, totalDispels, averageRaidHPS
						FROM raids_guild
						WHERE guildName=? AND instance=? AND numBosses=?
						ORDER BY raidID DESC
						LIMIT 6
					)
					''',
					b5o6_identifiers+(bossesInInstance[instance],))
	b5o6 = c.fetchall()
	newStats = b5o6[0]
	c.execute('''	SELECT numClears FROM b5o6_raids_guild_stats
					WHERE guildName=? AND instance=?
					LIMIT 1''',
					b5o6_identifiers)
	numClears = c.fetchall()
	if len(numClears) == 0: # First b5o6
		c.execute('''	INSERT INTO b5o6_raids_guild_stats
						VALUES (?, ?, ?, 1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
						''',
						b5o6_identifiers+(faction,)+newStats)
	else:
		c.execute('''	UPDATE b5o6_raids_guild_stats
						SET numClears=?, totalDeaths=?, totalResurrections=?, averageItemLevel=?, totalDmgDone=?, totalHealingDone=?, totalDmgTaken=?, totalHealingTaken=?, totalOverhealingDone=?, totalDmgAbsorbed=?, totalAbsorbDone=?, averageRaidDPS=?, totalInterrupts=?, totalDispels=?, averageRaidHPS=?
						WHERE guildName=? AND instance=?''',
						(numClears[0][0]+1,)+newStats+b5o6_identifiers)

def updateB5o6RaidsGuildCleartime(c, guildName, instance, faction):
	identifiers = (guildName, instance)
	c.execute('''	SELECT
					(sum(clearTime)-max(clearTime))/5 AS b5o6_clearTime
					FROM (
						SELECT (endTime - startTime) AS clearTime
						FROM raids_guild
						WHERE guildName=? AND instance =? AND finished=1
						ORDER BY raidID DESC
						LIMIT 6
					)
					''',
					identifiers)
	newTime = c.fetchall()[0]
	c.execute('''	SELECT numClears FROM b5o6_raids_guild_cleartime
					WHERE guildName=? and instance=?
					LIMIT 1''',
					identifiers)
	numClears = c.fetchall()
	if len(numClears) == 0: # First b5o6
		c.execute('''	INSERT INTO b5o6_raids_guild_cleartime
						VALUES (?, ?, ?, 1, ?)''',
						identifiers+(faction,)+newTime)
	else:
		c.execute('''	UPDATE b5o6_raids_guild_cleartime
						SET numClears=?, clearTime=?
						WHERE guildName=? AND instance=?''',
						((numClears[0][0]+1),)+newTime+identifiers)

def calcResetNo(instance, killedAtEpoch, twinstarID): # Reset numbers are only used internally. They represent the amount of resets that have happened since the release of Molten Core on K3.
	# Ony reset on 2018-06-21, so every 5d before then. The first such date before 2018-05-09 is 2018-05-07
	first7dresEpoch = datetime(2018,5,9,6,0,0).timestamp()
	first5dresEpoch = datetime(2018,5,7,6,0,0).timestamp()
	# tz = pytz.timezone("Europe/Stockholm") # Fucking DST kill me now
	# now = pytz.utc.localize(datetime.utcnow())
	# if (now.astimezone(tz).dst() != timedelta(0)): # True if summer time
		# killedAtEpoch -= 60*60
	if instance == "Molten Core" or instance == "Blackwing Lair" or instance == "Temple of Ahn'Qiraj" or instance == "Naxxramas": # 7 day reset schedule (Wednesdays 06:00)
		return int((killedAtEpoch - first7dresEpoch) / (7 * 24 * 60 * 60))
	elif instance == "Onyxia's Lair": # 5 day rolling reset schedule (06:00)
		return int((killedAtEpoch - first5dresEpoch) / (5 * 24 * 60 * 60))
	# elif instance == "Zul'Gurub" or instance == "Ruins of Ahn'Qiraj": # 3 day rolling reset schedule (06:00)
	# 	writeToLog(FAIL, "3 day reset raids do not have a timer yet.", twinstarID)
	# 	return
	else:
		writeToLog(FAIL, "Unknown instance " + instance + " passed to calcResetNo (despite legal boss)", twinstarID)
		exit(1)

def raidsPlayerExists(c, raidID, playerName):
	raidsPlayerIdentifier = ( raidID, playerName )
	c.execute('''	SELECT raidID FROM raids_player
					WHERE raidID=? AND playername=?''',
					raidsPlayerIdentifier)
	result = c.fetchone()
	return result != None

def getRaidID(c, instance, resetNo, playerName):
	raidsPlayerIdentifier = ( instance, resetNo, playerName )
	c.execute('''	SELECT raidID FROM raids_player JOIN raids_guild USING (raidID)
					WHERE instance=? AND resetNo=? AND playername=?''',
					raidsPlayerIdentifier)
	potential = c.fetchone()
	if potential != None:
		return str(potential[0])
	else:
		return None

def fetchRaidID( c, guildName, faction, instance, resetNo, deaths, resurrections, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, raidDPS, totalInterrupts, totalDispels, raidHPS ): # There's probably a better way to do this
	raidsGuildIdentifier = ( guildName, faction, instance, resetNo, deaths, resurrections, averageItemLevel, totalDmgDone, totalHealingDone, totalDmgTaken, totalHealingTaken, totalOverhealingDone, totalDmgAbsorbed, totalAbsorbDone, raidDPS, totalInterrupts, totalDispels, raidHPS )
	c.execute('''	SELECT raidID from raids_guild
					WHERE guildName=? AND faction=? AND instance=? AND resetNo=? AND totalDeaths=? AND totalResurrections=? AND averageItemLevel=? AND totalDmgDone=? AND totalHealingDone=? AND totalDmgTaken=? AND totalHealingTaken=? AND totalOverhealingDone=? AND totalDmgAbsorbed=? AND totalAbsorbDone=? AND averageRaidDPS=? AND totalInterrupts=? AND totalDispels=? AND averageRaidHPS=?''',
					raidsGuildIdentifier)
	return str(c.fetchone()[0])

def instanceFromBossname(bossname):
	if bossname == "Onyxia":
		return "Onyxia's Lair"
	elif bossname in ["Lucifron", "Magmadar", "Gehennas", "Garr", "Baron Geddon", "Shazzrah", "Sulfuron Harbinger", "Golemagg the Incinerator", "Majordomo Executus", "Ragnaros"]:
		return "Molten Core"
	elif bossname in ["Razorgore the Untamed", "Vaelastrasz the Corrupt", "Broodlord Lashlayer", "Firemaw", "Ebonroc", "Flamegor", "Chromaggus", "Nefarian"]:
		return "Blackwing Lair"
	elif bossname in ["The Prophet Skeram", "Bug Trio", "Battleguard Sartura", "Fankriss the Unyielding", "Viscidus", "Princess Huhuran", "Twin Emperors", "Ouro", "C'Thun"]:
		return "Temple of Ahn'Qiraj"
	elif bossname in ["Patchwerk", "Grobbulus", "Gluth", "Thaddius", "Anub'Rekhan", "Grand Widow Faerlina", "Maexxna", "Noth the Plaguebringer", "Heigan the Unclean", "Loatheb", "Instructor Razuvious", "Gothik the Harvester", "Four Horsemen", "Sapphiron", "Kel'Thuzad"]:
		return "Naxxramas"
	else:
		writeToLog(FAIL, "Unknown boss " + bossname + " passed to instanceFromBossname", -1)
		exit(1)

def updateRanks(
		bossesAffected=["Onyxia",
				"Lucifron", "Magmadar", "Gehennas", "Garr", "Baron Geddon", "Shazzrah", "Sulfuron Harbinger", "Golemagg the Incinerator", "Majordomo Executus", "Ragnaros",
				"Razorgore the Untamed", "Vaelastrasz the Corrupt", "Broodlord Lashlayer", "Firemaw", "Ebonroc", "Flamegor", "Chromaggus", "Nefarian",
				"The Prophet Skeram", "Bug Trio", "Battleguard Sartura", "Fankriss the Unyielding", "Viscidus", "Princess Huhuran", "Twin Emperors", "Ouro", "C'Thun",
				"Patchwerk", "Grobbulus", "Gluth", "Thaddius", "Anub'Rekhan", "Grand Widow Faerlina", "Maexxna", "Noth the Plaguebringer", "Heigan the Unclean", "Loatheb", "Instructor Razuvious", "Gothik the Harvester", "Four Horsemen", "Sapphiron", "Kel'Thuzad"],
		instancesAffected=["Onyxia's Lair", "Molten Core", "Blackwing Lair", "Temple of Ahn'Qiraj", "Naxxramas"],
		cleartimesAffected=["Onyxia's Lair", "Molten Core", "Blackwing Lair", "Temple of Ahn'Qiraj", "Naxxramas"]
		):
	print("bosses:", bossesAffected)
	print("instances:", instancesAffected)
	print("cleartimes:", cleartimesAffected)
	conn = sqlite3.connect(myPath + "logs.db")
	c = conn.cursor()

	n = 0
	tot = str(len(bossesAffected) + len(instancesAffected) + len(cleartimesAffected))
	bosses_players = {}
	bosses_guilds = {}
	b5o6_bosses_players = {}
	b5o6_bosses_guilds = {}
	for i in range(len(bossesAffected)):
		n += 1
		print("\rCalculating ranks: " + str(n) + "/" + tot, end="")
		bossname = bossesAffected[i]
		bosses_players[bossname] = {}
		b5o6_bosses_players[bossname] = {}
		legalstats_player = ["playerItemLevel", "usefullTime", "dmgDone", "healingDone", "dmgTaken", "healingTaken", "overhealingDone", "dmgAbsorbed", "absorbDone", "dps", "interrupts", "dispels", "hps", "deaths", "resurrections"]
		for j in range(len(legalstats_player)):  # Update for players
			stat = legalstats_player[j]
			c.execute('''	SELECT playerName, guildName, faction, playerClass, max(encounters_player.''' + stat + ''')
							FROM encounters_player JOIN encounters_guild using (encounterID)
							WHERE bossName=?
							GROUP BY playerName
							ORDER BY encounters_player.''' + stat + ''' DESC''',
							(bossname,))
			res = c.fetchall()
			gcount = {}
			ccount = [0] * 11
			fcount = [0] * 2
			for k in range(len(res)):
				if res[k][1] not in gcount:
					gcount[res[k][1]] = 0
				gcount[res[k][1]] += 1
				ccount[res[k][3]-1] += 1
				fcount[res[k][2]] += 1
				if res[k][0] not in bosses_players[bossname]:
					bosses_players[bossname][res[k][0]] = {}
				bosses_players[bossname][res[k][0]][stat] = (k+1, gcount[res[k][1]], ccount[res[k][3]-1], fcount[res[k][2]])

			c.execute('''	SELECT playerName, guildName, faction, playerClass, ''' + stat + '''
							FROM b5o6_encounters_player
							WHERE bossName=? AND numKills>=6
							ORDER BY ''' + stat + ''' DESC''',
							(bossname,))
			res = c.fetchall()
			gcount = {}
			ccount = [0] * 11
			fcount = [0] * 2
			for k in range(len(res)):
				if res[k][1] not in gcount:
					gcount[res[k][1]] = 0
				gcount[res[k][1]] += 1
				ccount[res[k][3]-1] += 1
				fcount[res[k][2]] += 1
				if res[k][0] not in b5o6_bosses_players[bossname]:
					b5o6_bosses_players[bossname][res[k][0]] = {}
				b5o6_bosses_players[bossname][res[k][0]][stat] = (k+1, gcount[res[k][1]], ccount[res[k][3]-1], fcount[res[k][2]])


		bosses_guilds[bossname] = {}
		b5o6_bosses_guilds[bossname] = {}
		legalstats_guild = ["fightLength", "deaths", "resurrections", "averageItemLevel", "totalDmgDone", "totalHealingDone", "totalDmgTaken", "totalHealingTaken", "totalOverHealingDone", "totalDmgAbsorbed", "totalAbsorbDone", "raidDps", "totalInterrupts", "totalDispels", "raidHps"]
		for j in range(len(legalstats_guild)): # Update for guilds
			stat = legalstats_guild[j]
			if stat == "fightLength":
				qualifier = "min("
				order = " ASC"
			else:
				qualifier = "max("
				orde = " DESC"
			c.execute('''	SELECT guildName, faction, ''' + qualifier + stat + ''')
							FROM encounters_guild
							WHERE bossName=?
							GROUP BY guildName
							ORDER BY ''' + stat + order,
							(bossname,))
			res = c.fetchall()
			fcount = [0] * 2
			for k in range(len(res)):
				fcount[res[k][1]] += 1
				if res[k][0] not in bosses_guilds[bossname]:
					bosses_guilds[bossname][res[k][0]] = {}
				bosses_guilds[bossname][res[k][0]][stat] = (k+1, fcount[res[k][1]])

			c.execute('''	SELECT guildName, faction, ''' + stat + '''
							FROM b5o6_encounters_guild
							WHERE bossName=? AND numKills>=6
							ORDER BY ''' + stat + ''' DESC''',
							(bossname,))
			res = c.fetchall()
			fcount = [0] * 2
			for k in range(len(res)):
				fcount[res[k][1]] += 1
				if res[k][0] not in b5o6_bosses_guilds[bossname]:
					b5o6_bosses_guilds[bossname][res[k][0]] = {}
				b5o6_bosses_guilds[bossname][res[k][0]][stat] = (k+1, fcount[res[k][1]])

	instances_players = {}
	instances_guilds = {}
	b5o6_instances_players = {}
	b5o6_instances_guilds = {}
	for i in range(len(instancesAffected)):
		n += 1
		print("\rCalculating ranks: " + str(n) + "/" + tot, end="")
		instancename = instancesAffected[i]
		instances_players[instancename] = {}
		b5o6_instances_players[instancename] = {}
		legalstats_player = ["playerItemLevel", "dmgDone", "healingDone", "dmgTaken", "healingTaken", "overhealingDone", "dmgAbsorbed", "absorbDone", "dps", "interrupts", "dispels", "hps", "deaths", "resurrections"]
		for j in range(len(legalstats_player)): # Update for players
			stat = legalstats_player[j]
			c.execute('''	SELECT playerName, guildName, faction, playerClass, max(raids_player.''' + stat + ''')
							FROM raids_player JOIN raids_guild using (raidID)
							WHERE instance=? AND raids_player.numBosses=?
							GROUP BY playerName
							ORDER BY raids_player.''' + stat + ''' DESC''',
							(instancename, bossesInInstance[instancename]))
			res = c.fetchall()
			gcount = {}
			ccount = [0] * 11
			fcount = [0] * 2
			for k in range(len(res)):
				if res[k][1] not in gcount:
					gcount[res[k][1]] = 0
				gcount[res[k][1]] += 1
				ccount[res[k][3]-1] += 1
				fcount[res[k][2]] += 1
				if res[k][0] not in instances_players[instancename]:
					instances_players[instancename][res[k][0]] = {}
				instances_players[instancename][res[k][0]][stat] = (k+1, gcount[res[k][1]], ccount[res[k][3]-1], fcount[res[k][2]])

			c.execute('''	SELECT playerName, guildName, faction, playerClass, ''' + stat + '''
							FROM b5o6_raids_player
							WHERE instance=? AND numClears>=6
							ORDER BY ''' + stat + ''' DESC''',
							(instancename,))
			res = c.fetchall()
			gcount = {}
			ccount = [0] * 11
			fcount = [0] * 2
			for k in range(len(res)):
				if res[k][1] not in gcount:
					gcount[res[k][1]] = 0
				gcount[res[k][1]] += 1
				ccount[res[k][3]-1] += 1
				fcount[res[k][2]] += 1
				if res[k][0] not in b5o6_instances_players[instancename]:
					b5o6_instances_players[instancename][res[k][0]] = {}
				b5o6_instances_players[instancename][res[k][0]][stat] = (k+1, gcount[res[k][1]], ccount[res[k][3]-1], fcount[res[k][2]])

		instances_guilds[instancename] = {}
		b5o6_instances_guilds[instancename] = {}
		legalstats_guild = ["totalDeaths", "totalResurrections", "averageItemLevel", "totalDmgDone", "totalHealingDone", "totalDmgTaken", "totalHealingTaken", "totalOverhealingDone", "totalDmgAbsorbed", "totalAbsorbDone", "averageRaidDPS", "totalInterrupts", "totalDispels", "averageRaidHPS"]
		for j in range(len(legalstats_guild)): # Update for guilds, stats
			stat = legalstats_guild[j]
			c.execute('''	SELECT guildName, faction, max(''' + stat + ''')
							FROM raids_guild
							WHERE instance=? AND numBosses=?
							GROUP BY guildName
							ORDER BY ''' + stat + ''' DESC''',
							(instancename, bossesInInstance[instancename]))
			res = c.fetchall()
			fcount = [0] * 2
			for k in range(len(res)):
				fcount[res[k][1]] += 1
				if res[k][0] not in instances_guilds[instancename]:
					instances_guilds[instancename][res[k][0]] = {}
				instances_guilds[instancename][res[k][0]][stat] = (k+1, fcount[res[k][1]])

			c.execute('''	SELECT guildName, faction, ''' + stat + '''
							FROM b5o6_raids_guild_stats
							WHERE instance=? AND numClears>=6
							ORDER BY ''' + stat + ''' DESC''',
							(instancename,))
			res = c.fetchall()
			fcount = [0] * 2
			for k in range(len(res)):
				fcount[res[k][1]] += 1
				if res[k][0] not in b5o6_instances_guilds[instancename]:
					b5o6_instances_guilds[instancename][res[k][0]] = {}
				b5o6_instances_guilds[instancename][res[k][0]][stat] = (k+1, fcount[res[k][1]])

	cleartimes = {}
	b5o6_cleartimes = {}
	for i in range(len(cleartimesAffected)):
		n += 1
		print("\rCalculating ranks: " + str(n) + "/" + tot, end="")
		instancename = cleartimesAffected[i]
		cleartimes[instancename] = {}
		c.execute('''	SELECT guildName, faction, min(endTime - startTime) AS clearTime
						FROM raids_guild
						WHERE instance=? AND finished=1
						GROUP BY guildName
						ORDER BY clearTime ASC''',
						(instancename,))
		res = c.fetchall()
		fcount = [0] * 2
		cleartimes[instancename] = {}
		for k in range(len(res)):
			fcount[res[k][1]] += 1
			if res[k][0] not in cleartimes[instancename]:
				cleartimes[instancename][res[k][0]] = {}
			cleartimes[instancename][res[k][0]] = (k+1, fcount[res[k][1]])

		c.execute('''	SELECT guildName, faction, clearTime
						FROM b5o6_raids_guild_cleartime
						WHERE instance=? AND numClears>=6
						ORDER BY clearTime ASC''',
						(instancename,))
		res = c.fetchall()
		fcount = [0] * 2
		b5o6_cleartimes[instancename] = {}
		for k in range(len(res)):
			fcount[res[k][1]] += 1
			if res[k][0] not in b5o6_cleartimes[instancename]:
				b5o6_cleartimes[instancename][res[k][0]] = {}
			b5o6_cleartimes[instancename][res[k][0]] = (k+1, fcount[res[k][1]])
	print()

	info_players = {}
	info_guilds = {}

	i = 0
	for boss in bossesAffected:
		i += 1
		print("\rInserting ranks: " + str(i) + "/" + tot, end="")
		for player in bosses_players[boss]:
			if player not in info_players:
				c.execute('''	SELECT playerClass, playerRace, guildName, faction
								FROM encounters_player JOIN encounters_guild USING (encounterID)
								WHERE playerName=?
								ORDER BY encounterID DESC
								LIMIT 1''',
								(player,))
				res = c.fetchall()[0]
				info_players[player] = (res[0], res[1], res[2], res[3])

			ranks = ()
			ranks += bosses_players[boss][player]["playerItemLevel"]
			ranks += bosses_players[boss][player]["usefullTime"]
			ranks += bosses_players[boss][player]["dmgDone"]
			ranks += bosses_players[boss][player]["healingDone"]
			ranks += bosses_players[boss][player]["dmgTaken"]
			ranks += bosses_players[boss][player]["healingTaken"]
			ranks += bosses_players[boss][player]["overhealingDone"]
			ranks += bosses_players[boss][player]["dmgAbsorbed"]
			ranks += bosses_players[boss][player]["absorbDone"]
			ranks += bosses_players[boss][player]["dps"]
			ranks += bosses_players[boss][player]["interrupts"]
			ranks += bosses_players[boss][player]["dispels"]
			ranks += bosses_players[boss][player]["hps"]
			ranks += bosses_players[boss][player]["deaths"]
			ranks += bosses_players[boss][player]["resurrections"] # Order must be preserved
			c.execute('''	SELECT *
							FROM ranks_encounters_player
							WHERE playerName=? AND bossName=?''',
							(player,boss))
			if len(c.fetchall()) == 0: # First rank for this boss
				c.execute('''	INSERT INTO ranks_encounters_player
				 				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
				 				(player, boss, instanceFromBossname(boss)) + info_players[player] + ranks)
			else:
				c.execute('''	UPDATE ranks_encounters_player
		 						SET total_playerItemLevel=?, guild_playerItemLevel=?, class_playerItemLevel=?, faction_playerItemLevel=?,
		 							total_usefullTime=?, guild_usefullTime=?, class_usefullTime=?, faction_usefullTime=?,
		 							total_dmgDone=?, guild_dmgDone=?, class_dmgDone=?, faction_dmgDone=?,
		 							total_healingDone=?, guild_healingDone=?, class_healingDone=?, faction_healingDone=?,
		 							total_dmgTaken=?, guild_dmgTaken=?, class_dmgTaken=?, faction_dmgTaken=?,
		 							total_healingTaken=?, guild_healingTaken=?, class_healingTaken=?, faction_healingTaken=?,
		 							total_overhealingDone=?, guild_overhealingDone=?, class_overhealingDone=?, faction_overhealingDone=?,
		 							total_dmgAbsorbed=?, guild_dmgAbsorbed=?, class_dmgAbsorbed=?, faction_dmgAbsorbed=?,
		 							total_absorbDone=?, guild_absorbDone=?, class_absorbDone=?, faction_absorbDone=?,
		 							total_dps=?, guild_dps=?, class_dps=?, faction_dps=?,
		 							total_interrupts=?, guild_interrupts=?, class_interrupts=?, faction_interrupts=?,
		 							total_dispels=?, guild_dispels=?, class_dispels=?, faction_dispels=?,
		 							total_hps=?, guild_hps=?, class_hps=?, faction_hps=?,
		 							total_deaths=?, guild_deaths=?, class_deaths=?, faction_deaths=?,
		 							total_resurrections=?, guild_resurrections=?, class_resurrections=?, faction_resurrections=?
				 				WHERE playerName=? AND bossName=?''',
				 				ranks+(player, boss))

			if player in b5o6_bosses_players[boss]: # There might not be a b5o6 entry
				ranks = ()
				ranks += b5o6_bosses_players[boss][player]["playerItemLevel"]
				ranks += b5o6_bosses_players[boss][player]["usefullTime"]
				ranks += b5o6_bosses_players[boss][player]["dmgDone"]
				ranks += b5o6_bosses_players[boss][player]["healingDone"]
				ranks += b5o6_bosses_players[boss][player]["dmgTaken"]
				ranks += b5o6_bosses_players[boss][player]["healingTaken"]
				ranks += b5o6_bosses_players[boss][player]["overhealingDone"]
				ranks += b5o6_bosses_players[boss][player]["dmgAbsorbed"]
				ranks += b5o6_bosses_players[boss][player]["absorbDone"]
				ranks += b5o6_bosses_players[boss][player]["dps"]
				ranks += b5o6_bosses_players[boss][player]["interrupts"]
				ranks += b5o6_bosses_players[boss][player]["dispels"]
				ranks += b5o6_bosses_players[boss][player]["hps"]
				ranks += b5o6_bosses_players[boss][player]["deaths"]
				ranks += b5o6_bosses_players[boss][player]["resurrections"] # Order must be preserved
				c.execute('''	SELECT *
								FROM ranks_b5o6_encounters_player
								WHERE playerName=? AND bossName=?''',
								(player,boss))
				if len(c.fetchall()) == 0: # First rank for this boss
					c.execute('''	INSERT INTO ranks_b5o6_encounters_player
					 				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
					 				(player, boss, instanceFromBossname(boss)) + info_players[player] + ranks)
				else:
					c.execute('''	UPDATE ranks_b5o6_encounters_player
			 						SET total_playerItemLevel=?, guild_playerItemLevel=?, class_playerItemLevel=?, faction_playerItemLevel=?,
			 							total_usefullTime=?, guild_usefullTime=?, class_usefullTime=?, faction_usefullTime=?,
			 							total_dmgDone=?, guild_dmgDone=?, class_dmgDone=?, faction_dmgDone=?,
			 							total_healingDone=?, guild_healingDone=?, class_healingDone=?, faction_healingDone=?,
			 							total_dmgTaken=?, guild_dmgTaken=?, class_dmgTaken=?, faction_dmgTaken=?,
			 							total_healingTaken=?, guild_healingTaken=?, class_healingTaken=?, faction_healingTaken=?,
			 							total_overhealingDone=?, guild_overhealingDone=?, class_overhealingDone=?, faction_overhealingDone=?,
			 							total_dmgAbsorbed=?, guild_dmgAbsorbed=?, class_dmgAbsorbed=?, faction_dmgAbsorbed=?,
			 							total_absorbDone=?, guild_absorbDone=?, class_absorbDone=?, faction_absorbDone=?,
			 							total_dps=?, guild_dps=?, class_dps=?, faction_dps=?,
			 							total_interrupts=?, guild_interrupts=?, class_interrupts=?, faction_interrupts=?,
			 							total_dispels=?, guild_dispels=?, class_dispels=?, faction_dispels=?,
			 							total_hps=?, guild_hps=?, class_hps=?, faction_hps=?,
			 							total_deaths=?, guild_deaths=?, class_deaths=?, faction_deaths=?,
			 							total_resurrections=?, guild_resurrections=?, class_resurrections=?, faction_resurrections=?
					 				WHERE playerName=? AND bossName=?''',
					 				ranks+(player, boss))

		for guild in bosses_guilds[boss]:
			if guild not in info_guilds:
				c.execute('''	SELECT faction
								FROM encounters_guild
								WHERE guildName=?
								ORDER BY encounterID DESC
								LIMIT 1''',
								(guild,))
				info_guilds[guild] = c.fetchall()[0][0]

			ranks = ()
			ranks += bosses_guilds[boss][guild]["fightLength"]
			ranks += bosses_guilds[boss][guild]["deaths"]
			ranks += bosses_guilds[boss][guild]["resurrections"]
			ranks += bosses_guilds[boss][guild]["averageItemLevel"]
			ranks += bosses_guilds[boss][guild]["totalDmgDone"]
			ranks += bosses_guilds[boss][guild]["totalHealingDone"]
			ranks += bosses_guilds[boss][guild]["totalDmgTaken"]
			ranks += bosses_guilds[boss][guild]["totalHealingTaken"]
			ranks += bosses_guilds[boss][guild]["totalOverHealingDone"]
			ranks += bosses_guilds[boss][guild]["totalDmgAbsorbed"]
			ranks += bosses_guilds[boss][guild]["totalAbsorbDone"]
			ranks += bosses_guilds[boss][guild]["raidDps"]
			ranks += bosses_guilds[boss][guild]["totalInterrupts"]
			ranks += bosses_guilds[boss][guild]["totalDispels"]
			ranks += bosses_guilds[boss][guild]["raidHps"] # Order must be preserved
			c.execute('''	SELECT *
							FROM ranks_encounters_guild
							WHERE guildName=? and bossName=?''',
							(guild, boss))
			if len(c.fetchall()) == 0: # First rank for this boss
				c.execute('''	INSERT INTO ranks_encounters_guild
								VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
								(guild, boss, info_guilds[guild], instanceFromBossname(boss)) + ranks)
			else:
				c.execute('''	UPDATE ranks_encounters_guild
								SET total_fightLength=?, faction_fightLength=?,
									total_deaths=?, faction_deaths=?,
									total_resurrections=?, faction_resurrections=?,
									total_averageItemLevel=?, faction_averageItemLevel=?,
									total_totalDmgDone=?, faction_totalDmgDone=?,
									total_totalHealingDone=?, faction_totalHealingDone=?,
									total_totalDmgTaken=?, faction_totalDmgTaken=?,
									total_totalHealingTaken=?, faction_totalHealingTaken=?,
									total_totalOverhealingDone=?, faction_totalOverhealingDone=?,
									total_totalDmgAbsorbed=?, faction_totalDmgAbsorbed=?,
									total_totalAbsorbDone=?, faction_totalAbsorbDone=?,
									total_raidDPS=?, faction_raidDPS=?,
									total_totalInterrupts=?, faction_totalInterrupts=?,
									total_totalDispels=?, faction_totalDispels=?,
									total_raidHPS=?, faction_raidHPS=?
				 				WHERE guildName=? AND bossName=?''',
				 				ranks+(guild, boss))

			if guild in b5o6_bosses_guilds[boss]: # There might not be a b5o6 entry
				ranks = ()
				ranks += b5o6_bosses_guilds[boss][guild]["fightLength"]
				ranks += b5o6_bosses_guilds[boss][guild]["deaths"]
				ranks += b5o6_bosses_guilds[boss][guild]["resurrections"]
				ranks += b5o6_bosses_guilds[boss][guild]["averageItemLevel"]
				ranks += b5o6_bosses_guilds[boss][guild]["totalDmgDone"]
				ranks += b5o6_bosses_guilds[boss][guild]["totalHealingDone"]
				ranks += b5o6_bosses_guilds[boss][guild]["totalDmgTaken"]
				ranks += b5o6_bosses_guilds[boss][guild]["totalHealingTaken"]
				ranks += b5o6_bosses_guilds[boss][guild]["totalOverHealingDone"]
				ranks += b5o6_bosses_guilds[boss][guild]["totalDmgAbsorbed"]
				ranks += b5o6_bosses_guilds[boss][guild]["totalAbsorbDone"]
				ranks += b5o6_bosses_guilds[boss][guild]["raidDps"]
				ranks += b5o6_bosses_guilds[boss][guild]["totalInterrupts"]
				ranks += b5o6_bosses_guilds[boss][guild]["totalDispels"]
				ranks += b5o6_bosses_guilds[boss][guild]["raidHps"] # Order must be preserved
				c.execute('''	SELECT *
								FROM ranks_b5o6_encounters_guild
								WHERE guildName=? and bossName=?''',
								(guild, boss))
				if len(c.fetchall()) == 0: # First rank for this boss
					c.execute('''	INSERT INTO ranks_b5o6_encounters_guild
									VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
									(guild, boss, info_guilds[guild], instanceFromBossname(boss)) + ranks)
				else:
					c.execute('''	UPDATE ranks_b5o6_encounters_guild
									SET total_fightLength=?, faction_fightLength=?,
										total_deaths=?, faction_deaths=?,
										total_resurrections=?, faction_resurrections=?,
										total_averageItemLevel=?, faction_averageItemLevel=?,
										total_totalDmgDone=?, faction_totalDmgDone=?,
										total_totalHealingDone=?, faction_totalHealingDone=?,
										total_totalDmgTaken=?, faction_totalDmgTaken=?,
										total_totalHealingTaken=?, faction_totalHealingTaken=?,
										total_totalOverhealingDone=?, faction_totalOverhealingDone=?,
										total_totalDmgAbsorbed=?, faction_totalDmgAbsorbed=?,
										total_totalAbsorbDone=?, faction_totalAbsorbDone=?,
										total_raidDPS=?, faction_raidDPS=?,
										total_totalInterrupts=?, faction_totalInterrupts=?,
										total_totalDispels=?, faction_totalDispels=?,
										total_raidHPS=?, faction_raidHPS=?
					 				WHERE guildName=? AND bossName=?''',
					 				ranks+(guild, boss))

	for instance in instancesAffected:
		i += 1
		print("\rInserting ranks: " + str(i) + "/" + tot, end="")
		for player in instances_players[instance]:
			if player not in info_players:
				c.execute('''	SELECT playerClass, playerRace, guildName, faction
								FROM encounters_player JOIN encounters_guild USING (encounterID)
								WHERE playerName=?
								ORDER BY encounterID DESC
								LIMIT 1''',
								(player,))
				res = c.fetchall()[0]
				info_players[player] = res

			ranks = ()
			ranks += instances_players[instance][player]["playerItemLevel"]
			ranks += instances_players[instance][player]["dmgDone"]
			ranks += instances_players[instance][player]["healingDone"]
			ranks += instances_players[instance][player]["dmgTaken"]
			ranks += instances_players[instance][player]["healingTaken"]
			ranks += instances_players[instance][player]["overhealingDone"]
			ranks += instances_players[instance][player]["dmgAbsorbed"]
			ranks += instances_players[instance][player]["absorbDone"]
			ranks += instances_players[instance][player]["dps"]
			ranks += instances_players[instance][player]["interrupts"]
			ranks += instances_players[instance][player]["dispels"]
			ranks += instances_players[instance][player]["hps"]
			ranks += instances_players[instance][player]["deaths"]
			ranks += instances_players[instance][player]["resurrections"] # Order must be preserved
			c.execute('''	SELECT *
							FROM ranks_raids_player
							WHERE playerName=? AND instance=?''',
							(player,instance))
			if len(c.fetchall()) == 0: # First rank for this raid
				c.execute('''	INSERT INTO ranks_raids_player
				 				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
				 				(player, instance) + info_players[player] + ranks)
			else:
				c.execute('''	UPDATE ranks_raids_player
		 						SET total_playerItemLevel=?, guild_playerItemLevel=?, class_playerItemLevel=?, faction_playerItemLevel=?,
		 							--total_usefullTime=?, guild_usefullTime=?, class_usefullTime=?, faction_usefullTime=?,
		 							total_dmgDone=?, guild_dmgDone=?, class_dmgDone=?, faction_dmgDone=?,
		 							total_healingDone=?, guild_healingDone=?, class_healingDone=?, faction_healingDone=?,
		 							total_dmgTaken=?, guild_dmgTaken=?, class_dmgTaken=?, faction_dmgTaken=?,
		 							total_healingTaken=?, guild_healingTaken=?, class_healingTaken=?, faction_healingTaken=?,
		 							total_overhealingDone=?, guild_overhealingDone=?, class_overhealingDone=?, faction_overhealingDone=?,
		 							total_dmgAbsorbed=?, guild_dmgAbsorbed=?, class_dmgAbsorbed=?, faction_dmgAbsorbed=?,
		 							total_absorbDone=?, guild_absorbDone=?, class_absorbDone=?, faction_absorbDone=?,
		 							total_dps=?, guild_dps=?, class_dps=?, faction_dps=?,
		 							total_interrupts=?, guild_interrupts=?, class_interrupts=?, faction_interrupts=?,
		 							total_dispels=?, guild_dispels=?, class_dispels=?, faction_dispels=?,
		 							total_hps=?, guild_hps=?, class_hps=?, faction_hps=?,
		 							total_deaths=?, guild_deaths=?, class_deaths=?, faction_deaths=?,
		 							total_resurrections=?, guild_resurrections=?, class_resurrections=?, faction_resurrections=?
				 				WHERE playerName=? AND instance=?''',
				 				ranks+(player, instance))

			if player in b5o6_instances_players[instance]: # There might not be a b5o6 entry
				ranks = ()
				ranks += b5o6_instances_players[instance][player]["playerItemLevel"]
				ranks += b5o6_instances_players[instance][player]["dmgDone"]
				ranks += b5o6_instances_players[instance][player]["healingDone"]
				ranks += b5o6_instances_players[instance][player]["dmgTaken"]
				ranks += b5o6_instances_players[instance][player]["healingTaken"]
				ranks += b5o6_instances_players[instance][player]["overhealingDone"]
				ranks += b5o6_instances_players[instance][player]["dmgAbsorbed"]
				ranks += b5o6_instances_players[instance][player]["absorbDone"]
				ranks += b5o6_instances_players[instance][player]["dps"]
				ranks += b5o6_instances_players[instance][player]["interrupts"]
				ranks += b5o6_instances_players[instance][player]["dispels"]
				ranks += b5o6_instances_players[instance][player]["hps"]
				ranks += b5o6_instances_players[instance][player]["deaths"]
				ranks += b5o6_instances_players[instance][player]["resurrections"] # Order must be preserved
				c.execute('''	SELECT *
								FROM ranks_b5o6_raids_player
								WHERE playerName=? AND instance=?''',
								(player,instance))
				if len(c.fetchall()) == 0: # First rank for this raid
					c.execute('''	INSERT INTO ranks_b5o6_raids_player
					 				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
					 				(player, instance) + info_players[player] + ranks)
				else:
					c.execute('''	UPDATE ranks_b5o6_raids_player
			 						SET total_playerItemLevel=?, guild_playerItemLevel=?, class_playerItemLevel=?, faction_playerItemLevel=?,
			 							--total_usefullTime=?, guild_usefullTime=?, class_usefullTime=?, faction_usefullTime=?,
			 							total_dmgDone=?, guild_dmgDone=?, class_dmgDone=?, faction_dmgDone=?,
			 							total_healingDone=?, guild_healingDone=?, class_healingDone=?, faction_healingDone=?,
			 							total_dmgTaken=?, guild_dmgTaken=?, class_dmgTaken=?, faction_dmgTaken=?,
			 							total_healingTaken=?, guild_healingTaken=?, class_healingTaken=?, faction_healingTaken=?,
			 							total_overhealingDone=?, guild_overhealingDone=?, class_overhealingDone=?, faction_overhealingDone=?,
			 							total_dmgAbsorbed=?, guild_dmgAbsorbed=?, class_dmgAbsorbed=?, faction_dmgAbsorbed=?,
			 							total_absorbDone=?, guild_absorbDone=?, class_absorbDone=?, faction_absorbDone=?,
			 							total_dps=?, guild_dps=?, class_dps=?, faction_dps=?,
			 							total_interrupts=?, guild_interrupts=?, class_interrupts=?, faction_interrupts=?,
			 							total_dispels=?, guild_dispels=?, class_dispels=?, faction_dispels=?,
			 							total_hps=?, guild_hps=?, class_hps=?, faction_hps=?,
			 							total_deaths=?, guild_deaths=?, class_deaths=?, faction_deaths=?,
			 							total_resurrections=?, guild_resurrections=?, class_resurrections=?, faction_resurrections=?
					 				WHERE playerName=? AND instance=?''',
					 				ranks+(player, instance))

		for guild in instances_guilds[instance]:
			if guild not in info_guilds:
				c.execute('''	SELECT faction
								FROM encounters_guild
								WHERE guildName=?
								ORDER BY encounterID DESC
								LIMIT 1''',
								(guild,))
				info_guilds[guild] = c.fetchall()[0][0]

			ranks = ()
			ranks += instances_guilds[instance][guild]["totalDeaths"]
			ranks += instances_guilds[instance][guild]["totalResurrections"]
			ranks += instances_guilds[instance][guild]["averageItemLevel"]
			ranks += instances_guilds[instance][guild]["totalDmgDone"]
			ranks += instances_guilds[instance][guild]["totalHealingDone"]
			ranks += instances_guilds[instance][guild]["totalDmgTaken"]
			ranks += instances_guilds[instance][guild]["totalHealingTaken"]
			ranks += instances_guilds[instance][guild]["totalOverhealingDone"]
			ranks += instances_guilds[instance][guild]["totalDmgAbsorbed"]
			ranks += instances_guilds[instance][guild]["totalAbsorbDone"]
			ranks += instances_guilds[instance][guild]["averageRaidDPS"]
			ranks += instances_guilds[instance][guild]["totalInterrupts"]
			ranks += instances_guilds[instance][guild]["totalDispels"]
			ranks += instances_guilds[instance][guild]["averageRaidHPS"] # Order must be preserved
			c.execute('''	SELECT *
							FROM ranks_raids_guild_stats
							WHERE guildName=? and instance=?''',
							(guild, instance))
			if len(c.fetchall()) == 0: # First rank for this raid
				c.execute('''	INSERT INTO ranks_raids_guild_stats
								VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
								(guild, instance, info_guilds[guild]) + ranks)
			else:
				c.execute('''	UPDATE ranks_raids_guild_stats
								SET total_totalDeaths=?, faction_totalDeaths=?,
									total_totalResurrections=?, faction_totalResurrections=?,
									total_averageItemLevel=?, faction_averageItemLevel=?,
									total_totalDmgDone=?, faction_totalDmgDone=?,
									total_totalHealingDone=?, faction_totalHealingDone=?,
									total_totalDmgTaken=?, faction_totalDmgTaken=?,
									total_totalHealingTaken=?, faction_totalHealingTaken=?,
									total_totalOverhealingDone=?, faction_totalOverhealingDone=?,
									total_totalDmgAbsorbed=?, faction_totalDmgAbsorbed=?,
									total_totalAbsorbDone=?, faction_totalAbsorbDone=?,
									total_averageRaidDPS=?, faction_averageRaidDPS=?,
									total_totalInterrupts=?, faction_totalInterrupts=?,
									total_totalDispels=?, faction_totalDispels=?,
									total_averageRaidHPS=?, faction_averageRaidHPS=?
				 				WHERE guildName=? AND instance=?''',
				 				ranks+(guild, instance))

			if guild in b5o6_instances_guilds[instance]: # There might not be a b5o6 entry
				ranks = ()
				ranks += b5o6_instances_guilds[instance][guild]["totalDeaths"]
				ranks += b5o6_instances_guilds[instance][guild]["totalResurrections"]
				ranks += b5o6_instances_guilds[instance][guild]["averageItemLevel"]
				ranks += b5o6_instances_guilds[instance][guild]["totalDmgDone"]
				ranks += b5o6_instances_guilds[instance][guild]["totalHealingDone"]
				ranks += b5o6_instances_guilds[instance][guild]["totalDmgTaken"]
				ranks += b5o6_instances_guilds[instance][guild]["totalHealingTaken"]
				ranks += b5o6_instances_guilds[instance][guild]["totalOverhealingDone"]
				ranks += b5o6_instances_guilds[instance][guild]["totalDmgAbsorbed"]
				ranks += b5o6_instances_guilds[instance][guild]["totalAbsorbDone"]
				ranks += b5o6_instances_guilds[instance][guild]["averageRaidDPS"]
				ranks += b5o6_instances_guilds[instance][guild]["totalInterrupts"]
				ranks += b5o6_instances_guilds[instance][guild]["totalDispels"]
				ranks += b5o6_instances_guilds[instance][guild]["averageRaidHPS"] # Order must be preserved
				c.execute('''	SELECT *
								FROM ranks_b5o6_raids_guild_stats
								WHERE guildName=? and instance=?''',
								(guild, instance))
				if len(c.fetchall()) == 0: # First rank for this raid
					c.execute('''	INSERT INTO ranks_b5o6_raids_guild_stats
									VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
									(guild, instance, info_guilds[guild]) + ranks)
				else:
					c.execute('''	UPDATE ranks_b5o6_raids_guild_stats
									SET total_totalDeaths=?, faction_totalDeaths=?,
										total_totalResurrections=?, faction_totalResurrections=?,
										total_averageItemLevel=?, faction_averageItemLevel=?,
										total_totalDmgDone=?, faction_totalDmgDone=?,
										total_totalHealingDone=?, faction_totalHealingDone=?,
										total_totalDmgTaken=?, faction_totalDmgTaken=?,
										total_totalHealingTaken=?, faction_totalHealingTaken=?,
										total_totalOverhealingDone=?, faction_totalOverhealingDone=?,
										total_totalDmgAbsorbed=?, faction_totalDmgAbsorbed=?,
										total_totalAbsorbDone=?, faction_totalAbsorbDone=?,
										total_averageRaidDPS=?, faction_averageRaidDPS=?,
										total_totalInterrupts=?, faction_totalInterrupts=?,
										total_totalDispels=?, faction_totalDispels=?,
										total_averageRaidHPS=?, faction_averageRaidHPS=?
					 				WHERE guildName=? AND instance=?''',
					 				ranks+(guild, instance))

	for instance in cleartimesAffected:
		i += 1
		print("\rInserting ranks: " + str(i) + "/" + tot, end="")
		for guild in cleartimes[instance]:
			if guild not in info_guilds:
				c.execute('''	SELECT faction
								FROM encounters_guild
								WHERE guildName=?
								ORDER BY encounterID DESC
								LIMIT 1''',
								(guild,))
				info_guilds[guild] = c.fetchall()[0][0]

			c.execute('''	SELECT *
							FROM ranks_raids_guild_cleartime
							WHERE guildName=? and instance=?''',
							(guild, instance))
			if len(c.fetchall()) == 0: # First rank for this raid
				c.execute('''	INSERT INTO ranks_raids_guild_cleartime
								VALUES (?, ?, ?, ?, ?)''',
								(guild, instance, info_guilds[guild]) + cleartimes[instance][guild])
			else:
				c.execute('''	UPDATE ranks_raids_guild_cleartime
								SET total_clearTime=?, faction_clearTime=?
								WHERE guildName=? AND instance=?''',
								cleartimes[instance][guild] + (guild, instance))

			if guild in b5o6_cleartimes[instance]: # There might not be a b5o6 entry
				c.execute('''	SELECT *
								FROM ranks_b5o6_raids_guild_cleartime
								WHERE guildName=? and instance=?''',
								(guild, instance))
				if len(c.fetchall()) == 0: # First rank for this raid
					c.execute('''	INSERT INTO ranks_b5o6_raids_guild_cleartime
									VALUES (?, ?, ?, ?, ?)''',
									(guild, instance, info_guilds[guild]) + b5o6_cleartimes[instance][guild])
				else:
					c.execute('''	UPDATE ranks_b5o6_raids_guild_cleartime
									SET total_clearTime=?, faction_clearTime=?
									WHERE guildName=? AND instance=?''',
									b5o6_cleartimes[instance][guild] + (guild, instance))
	print()
	conn.commit()
	conn.close()

def getFromUntil(start, end, noranks=False):
	print("Fetching info for all fights between IDs " + str(start) + " and " + str(end) + " (inclusive)")
	bosses = []
	instances_stat = []
	instances_clear = []
	for i in range(start,end+1):
		res = getInfo(i)
		if res == "abort":
			break
		if res != None:
			(bossName, instance_stat, instance_clear) = res
			if bossName not in bosses:
				bosses.append(bossName)
			if instance_stat != None and instance_stat not in instances_stat:
				instances_stat.append(instance_stat)
			if instance_clear != None and instance_clear not in instances_clear:
				instances_clear.append(instance_clear)
	global doneFetching
	doneFetching = True
	if not noranks:
		updateRanks(bosses, instances_stat, instances_clear)

def autoScrape(until=None, limit=None, noranks=False):
	if os.path.isfile(myPath + "latest.txt"):
		with open(myPath + "latest.txt", "r") as file:
			latestEntered = file.readlines()[0]
	else:
		conn = sqlite3.connect(myPath + "logs.db")
		c = conn.cursor()
		c.execute("SELECT encounterID from encounters_guild ORDER BY encounterID DESC LIMIT 1")
		res = c.fetchone()
		if res is None:
			latestEntered = 649264-1 # The first K3 raid kill, minus one to include it
		else:
			latestEntered = res[0]
			writeToLog(WARN, "Couldn't locate latest.txt. Falling back to latest database entry.", latestEntered)

	url = "https://vanilla-twinhead.twinstar.cz/?latest=bosskills"
	page = requests.get(url)
	if page.status_code != 200 or page.url != url:
		writeToLog(FAIL, "Failed to fetch latest bosskills page: " + url, latestEntered)
		return
	content = page.text
	marker = content.find("[{")
	while marker != -1: # For some reasons raid ID's don't seem to be in order unless the realm is correct
		marker = content.find("{id:", marker+1)
		marker2 = content.find(",", marker+1)
		killID = content[marker+4:marker2]
		marker = content.find(",realm", marker2)
		marker2 = content.find(",", marker+1)
		realm = content[marker+8:marker2-1]
		if realm == "Kronos III":
			break
	if marker != -1:
		scrapeFrom = int(latestEntered)+1
		scrapeUntil = int(killID)
		if until is not None and limit is not None:
			writeToLog(FAIL, "Only one of until and limit may be suppled to autoScrape", latestEntered)
		elif until is not None:
			scrapeUntil = min(scrapeUntil, until)
		elif limit is not None:
			scrapeUntil = min(scrapeUntil, scrapeFrom+limit-1)
		getFromUntil(scrapeFrom, scrapeUntil, noranks)
	else:
		writeToLog(FAIL, "Unable to find a Kronos III raid on latest bosskill page", latestEntered)
		return

def handleSignal(signal, frame):
	if doneFetching:
		print("Cancelling rank calculation due to SIGINT. Ranks will be inaccurate.")
		exit(0)
	else:
		print("Aborting fetch after current")
		global stopFetching
		stopFetching = True

def setPath(path):
	if path[-1] != "/":
		path += "/"
	if os.access(path, os.W_OK):
		global myPath
		myPath = path
	else:
		raise OSError("No write permission to specified path.")

def printUsage(msg):
	print("usage: " + USAGE)
	print(msg)

USAGE = sys.argv[0] + " [-h] [-p PATH] [--ranksonly] [--noranks] [ID [ID]] [--until ID | --limit LIMIT]"

def main():
	parser = argparse.ArgumentParser(
		description="Scraper for K3Stats",
		usage=USAGE
	)
	parser.add_argument("ID", nargs="*", help="If no ID is supplied the IDs to be scraped are automatically determined. If one ID is supplied this ID is scraped. If two IDs are supplied all IDs from the first ID until and including the second ID are scraped.")
	parser.add_argument("-p", "--path", default="./", help="The path to the folder containing the database file. If not present current directory is assumed.")
	parser.add_argument("--until", nargs=1, metavar="ID", help="Scrapes until ID, instead of until the latest available. May not be used alongside manually specified IDs.")
	parser.add_argument("--limit", nargs=1, help="Scrapes a maximum of LIMIT IDs.")
	parser.add_argument("--ranksonly", action="store_true", help="Calculates all ranks and exits, without scraping any new IDs.")
	parser.add_argument("--noranks", action="store_true", help="Do not update ranks after scraping.")
	args = parser.parse_args()

	setPath(args.path)

	global doneFetching

	if args.ranksonly:
		doneFetching = True
		updateRanks()
		return

	if args.limit is not None and args.until is not None:
		printUsage("error: Only one of --until and --limit may be used")
		exit(1)

	if args.limit is not None:
		if len(args.ID) > 1:
			printUsage("error: You may not supply a range of IDs alongside --limit")
			exit(1)
		elif len(args.ID) == 1:
			try:
				start = int(args.ID[0])
			except ValueError:
				printUsage("error: ID must be an int")
				exit(1)
			try:
				limit = int(args.limit[0])
			except ValueError:
				printUsage("error: LIMIT must be an int")
				exit(1)
			getFromUntil(start, start+limit-1, noranks=args.noranks)
			return
		else:
			try:
				limit = int(args.limit[0])
			except ValueError:
				printUsage("error: LIMIT must be an int")
				exit(1)
			autoScrape(limit=limit, noranks=args.noranks)
			return

	if args.until is not None:
		if len(args.ID) > 0:
			printUsage("error: You may not specify a start ID alongside --until. Use a normal ID range instead")
			exit(1)
		else:
			try:
				until = int(args.until[0])
			except ValueError:
				printUsage("error: ID must be an int")
				exit(1)
			autoScrape(until=until, noranks=args.noranks)
			return

	if len(args.ID) > 2:
		printUsage("error: Only up to two IDs may be supplied")
		exit(1)
	elif len(args.ID) == 2:
		try:
			start = int(args.ID[0])
			end = int(args.ID[1])
		except ValueError:
			printUsage("error: ID must be an int")
			exit(1)
		getFromUntil(start, end, noranks=args.noranks)
	elif len(args.ID) == 1:
		try:
			toScrape = int(args.ID[0])
		except ValueError:
			printUsage("error: ID must be an int")
			exit(1)
		res = getInfo(toScrape)
		doneFetching = True
		if res != None and not args.noranks:
			(bosses, instances_stat, instances_clear) = res
			updateRanks(
				[bosses] if bosses != None else [],
				[instances_stat] if instances_stat != None else [],
				[instances_clear] if instances_clear != None else []
				)
	else:
		autoScrape(noranks=args.noranks)

signal.signal(signal.SIGINT, handleSignal)
main()
