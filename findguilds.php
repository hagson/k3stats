<!-- Copyright 2018,2019 Hagson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

<?php
	require("util.php");
	echoInitial("Guild search", false, false);

	$db = getDB();
	if (isset($_GET["name"]) && $_GET["name"] != "") {
		$statement = $db->prepare("SELECT guildName, faction FROM raids_guild WHERE guildName LIKE :guildname GROUP BY guildName ORDER BY guildName COLLATE NOCASE LIMIT 100");
		$statement->bindValue(":guildname", "%" . /*sqlite_escape_string*/(deniceify(htmlspecialchars($_GET["name"])) . "%"));
		$result = $statement->execute();

		$i = 0;
		$table = "";
		while ($row = $result->fetchArray()) {
			$table .= "<tr><td class=\"faction" . $row["faction"] . "\"><a href=\"guilds.php?name=" . deniceify($row["guildName"]) . "\">" . niceify($row["guildName"]) . "</a></td></tr>\n";
			$temp = $row["guildName"];
			$i++;
		}

		if ($table == "") {
			echo ("\n\t<h1>Find Guilds</h1>\n\t<form method=\"GET\">
			Guild name:
			<input type=\"text\" name=\"name\" value=\"\" class=\"raidfilter\">
			<input type=\"submit\" value=\"Search\">
			</form>
			");
			echo ("<h1>Nothing found</h1>\n");
		} elseif ($i == 1) {
			header("Location: guilds.php?name=" . $temp);
			die();
		} else {
			echo ("<h1>Find Guilds</h1>\n<form method=\"GET\">
			Guild name:
			<input type=\"text\" name=\"name\" value=\"\" class=\"raidfilter\">
			<input type=\"submit\" value=\"Search\">
			</form>
			");
			echo ("<h1>Results:</h1>\n<table class=\"sortable\"><tr><th>Guild</th></tr>" . $table . "</table>\n");
		}
	} else {
		function echoGuild($res, $faction, $factionStr, $tableExtra) {
			$table = "";
			while ($row = $res->fetchArray()) {
				$table .= "\t\t<tr><td class=\"faction" . $faction . "\"><a href=\"guilds.php?name=" . deniceify($row["guildName"]) . "\">" . niceify($row["guildName"]) . "</a></td></tr>\n";
			}
			if ($table != "") {
				echo ("\t<table class=\"sortable\" " . $tableExtra . "><tr><th>" . $factionStr . "</th></tr>" . $table . "\t</table>\n");
			}	
		}
		echo ("<h1>Find Guilds</h1>\n<form method=\"GET\">
			Guild name:
			<input type=\"text\" name=\"name\" value=\"\" class=\"raidfilter\">
			<input type=\"submit\" value=\"Search\">
		</form>");
		$activeally = $db->prepare("SELECT DISTINCT guildName FROM raids_guild WHERE endTime > :14dAgoEpoch AND faction = 0 ORDER BY guildName COLLATE NOCASE");
		$activehorde = $db->prepare("SELECT DISTINCT guildName FROM raids_guild WHERE endTime > :14dAgoEpoch AND faction = 1 ORDER BY guildName COLLATE NOCASE");
		$activeally->bindValue(":14dAgoEpoch", strtotime("-2 week"));
		$activehorde->bindValue(":14dAgoEpoch", strtotime("-2 week"));
		$allyresult = $activeally->execute();
		$horderesult = $activehorde->execute();
		echo ("\t<h3>Recently active guilds</h3>\n\t<div style=\"width: auto; margin: auto; display: inline-block\">\n");
		echoGuild($allyresult, 0, "Alliance", "style=\"float: left; margin-right: 40px\"");
		echoGuild($horderesult, 1, "Horde", "style=\"float: right\"");
		echo ("\t</div>");
	}
	echo ("\n</body>\n</html>");
?>
