<!-- Copyright 2018,2019 Hagson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

<?php
	if ($_GET["id"] == "") {
		header("Location: findraids.php");
		die();
	}
	require("util.php");
	$db = getDB();
	$statement = $db->prepare("SELECT * FROM raids_guild WHERE raidID = :raidid");
	$statement->bindValue(":raidid", /*sqlite_escape_string*/(htmlspecialchars($_GET["id"])));
	$result = $statement->execute();
	$row = $result->fetchArray();

	echoInitial(niceify($row["guildName"]) . " " . $row["instance"] . " " . substr(formatTimeAbs($row["startTime"]), 0, 10), true, false);
	echo ("\n\t<h1>" . niceify($row["guildName"]) . " " . $row["instance"] . " " . substr(formatTimeAbs($row["startTime"]), 0, 10) . "</h1>\n\t<h4>Duration: " . formatTimeRel($row["endTime"] - $row["startTime"], true));
	if ($row["finished"] == 0) {
		echo " (NOT FINISHED!)";
	}
	echo ("</h4>\n\n\t<h2>Encounters:</h2>\n");

	$encstmt = $db->prepare("SELECT * FROM encounters_guild WHERE raidID = :raidid ORDER BY encounterID ASC");
	$encstmt->bindValue(":raidid", /*sqlite_escape_string*/(htmlspecialchars($_GET["id"])));
	$encresult = $encstmt->execute();
	encounterDisplay($encresult);

	if ($row["instance"] == "Blackwing Lair") {
		echo ("\n\t<h2 title=\"Excluding Razorgore the Untamed\">Character stats:*</h2>\n");
	} else {
		echo ("\n\t<h2>Character stats:</h2>\n");
	}
	$charstmt = $db->prepare("SELECT * FROM raids_player WHERE raidID = :raidid ORDER BY dmgDone DESC");
	$charstmt->bindValue(":raidid", /*sqlite_escape_string*/(htmlspecialchars($_GET["id"])));
	$charresult = $charstmt->execute();
	statDisplay($charresult);
?>

</body>
</html>
