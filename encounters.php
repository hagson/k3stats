<!-- Copyright 2018,2019 Hagson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

<?php
	if ($_GET["id"] == "") {
		header("Location: findraids.php");
		die();
	}
	require("util.php");

	$db = getDB();
	$statement = $db->prepare("SELECT * FROM encounters_guild WHERE encounterId = :encid");
	$statement->bindValue(":encid", /*sqlite_escape_string*/(htmlspecialchars($_GET["id"])));
	$result = $statement->execute();
	$row = $result->fetchArray();
	if ($row == false) {
		echoInitial("Error", false, false);
		die("<h1>No such encounter</h1>");
	}
	echoInitial(niceify($row["guildName"]) . " " . $row["bossName"] . " " . formatTimeAbs($row["killedAt"]), true, false);
	echo ("<h1>" . niceify($row["guildName"]) . " " . $row["bossName"] . " " . formatTimeAbs($row["killedAt"]) . "</h1>\n\t<h4><a href=\"raids.php?id=" . $row["raidID"] . "\">Back to raid</a></h4>\n\n\t<h2>Total:</h2>");

	$table = "\n\t<table>\n\t\t<tr><th>Guild</th><th>Fight Length</th><th>Dmg Done</th><th>DPS</th><th>Dmg Taken</th><th>Healing Done</th><th>HPS</th><th>Healing Taken</th><th>Absorption Done</th><th>Dmg Absorbed</th><th>Overhealing Done</th><th>Dispels</th><th>Interrupts</th><th>Deaths</th></tr>\n";
	$table .= "\t\t<tr><td class=\"faction" . $row["faction"] . "\">" . niceify($row["guildName"]) . "</td><td>" . formatTimeRel($row["fightLength"], false) . "</td><td>" . $row["totalDmgDone"] . "</td><td>" . floor($row["raidDPS"]) . "</td><td>" . $row["totalDmgTaken"] . "</td><td>" . $row["totalHealingDone"] . "</td><td>" . floor($row["raidHPS"]) . "</td><td>" . $row["totalHealingTaken"] . "</td><td>" . $row["totalAbsorbDone"] . "</td><td>" . $row["totalDmgAbsorbed"] . "</td><td>" . $row["totalOverhealingDone"] . "</td><td>" . $row["totalDispels"] . "</td><td>" . $row["totalInterrupts"] . "</td><td>" . $row["deaths"] . "</td></tr>\n\t</table>\n";
	echo $table;

	$statement = $db->prepare("SELECT * FROM encounters_player WHERE encounterId = :encid ORDER BY dps DESC");
	$statement->bindValue(":encid", /*sqlite_escape_string*/(htmlspecialchars($_GET["id"])));
	$result = $statement->execute();

	echo ("\n\t<h2>Individual:</h2>\n");
	statDisplay($result);
?>

</body>
</html>
