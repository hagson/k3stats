<!-- Copyright 2018,2019 Hagson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

<?php
	if ($_GET["name"] == "") {
		header("Location: findguilds.php");
		die();
	}
	require("util.php");

	$db = getDB();

	$stmt = $db->prepare("SELECT faction FROM raids_guild WHERE guildName = :guild LIMIT 1");
	$stmt->bindValue(":guild", /*sqlite_escape_string*/deniceify(htmlspecialchars($_GET["name"])));
	$res = $stmt->execute()->fetchArray();
	if ($res == false) {
		echoInitial("Unknown guild", false, false);
		echo ("<h1>Unknown guild</h1>");
		die();
	} elseif ($res["faction"] == 1) {
		$faction = "Horde";
	} else {
		$faction = "Alliance";
	}

	$guildName = deniceify(htmlspecialchars($_GET["name"]));
	echoInitial(niceify($guildName), false, false);
	echo ("<h1>" . niceify($guildName) . "</h1><h4>" . $faction . "</h4>\n<div style=\"width: auto; margin: auto; display:inline-block\"><div style=\"float: right; margin-left: 40px\"><h2>Cleartime records</h2>\n");

	$guildInstances = [];
	foreach($instances as $instanceName) { // Cleartimes and rank for all instances
		$cleartimestmt = $db->prepare("	SELECT raidID, MIN(endTime-startTime) AS clearTime FROM raids_guild
										WHERE guildName = :name AND instance = :instance AND finished = 1" );
		$cleartimestmt->bindValue(":name", $guildName);
		$cleartimestmt->bindValue(":instance", $instanceName);
		$cleartimerow = $cleartimestmt->execute()->fetchArray();
		if ($cleartimerow["raidID"] != "") {
			$guildInstances[$instanceName]["raidID"] = $cleartimerow["raidID"];
			$guildInstances[$instanceName]["clearTime"] = $cleartimerow["clearTime"];
			$rankres = $db->prepare("	SELECT 1+COUNT(DISTINCT guildName) as rank FROM raids_guild
										WHERE (endTime - startTime) < :clearTime AND finished = 1 AND instance = :instance");
			$rankres->bindValue(":clearTime", $guildInstances[$instanceName]["clearTime"]);
			$rankres->bindValue(":instance", $instanceName);
			$rankres = $rankres->execute();
			$guildInstances[$instanceName]["rank"] = $rankres->fetchArray()["rank"];
		}
	}
	$table = "<table class=\"sortable\"><tr><th>ID</th><th>Instance</th><th>Cleartime</th><th>Rank</th></tr>";
	foreach($guildInstances as $instanceName => $val) {
		$table .= "<tr><td><a href=\"raids.php?id=" . $val["raidID"] . "\">" . $val["raidID"] . "</a></td><td><a href=\"records.php?instance=" . $instanceshort[$instanceName] . "\">" . $instanceName . "</a></td><td>" . formatTimeRel($val["clearTime"], true) . "</td><td>" . $val["rank"] . "</td></tr>";
	}
	echo $table . "</table>";

	// Killtime records and rank
	echo "<h2>Killtime records</h2>\n";
	$killtimestmt = $db->prepare("	SELECT encounterID, killedAt, bossName, instance, MIN(fightLength) AS len, 1+(
										SELECT count(DISTINCT guildName)
										FROM encounters_guild a
										WHERE a.fightLength < b.fightLength AND a.bossName = b.bossName
									) AS rank FROM encounters_guild b
									WHERE b.guildName = :name
									GROUP BY bossName
									ORDER BY " . $sqlbossorder);
	$killtimestmt->bindValue(":name", $guildName);
	$killtimeresult = $killtimestmt->execute();
	$tablestart = "<table class=\"sortable\"><tr><th>Killed At</th><th>Boss</th><th>Killtime</th><th>Rank</th></tr>";
	$tableend = "</table>";
	$instance = "";
	while ($val = $killtimeresult->fetchArray()) {
		if ($val["instance"] != $instance) {
			if ($instance != "") {
				echo ($table . $tableend);
			}
			$instance = $val["instance"];
			echo ("<h3>" . $instance . "</h3>");
			$table = $tablestart;
		}
		$table .= "<tr><td><a href=\"encounters.php?id=" . $val["encounterID"] . "\">" . formatTimeAbs($val["killedAt"]) . "</a></td><td><a href=\"records.php?boss=" . $val["bossName"] . "\">" . $val["bossName"] . "</a></td><td>" . formatTimeRel($val["len"], false) . "</td><td>" . $val["rank"] . "</td></tr>";
	}
	echo $table . $tableend;

	// Latest 30 bosskills
	echo "</div><div style=\"float: left\"><h2>Latest bosskills</h2>\n";
	$statement = $db->prepare(	"SELECT raidID, instance, bossName, encounterID, killedAt FROM encounters_guild
								WHERE guildName = :name
								GROUP BY encounterID
								ORDER BY encounterID DESC
								LIMIT 30");
	$statement->bindValue(":name", $guildName);
	$result = $statement->execute();
	$table = "";
	while ($row = $result->fetchArray()) {
		$table .= "<tr><td><a href=\"raids.php?id=" . $row["raidID"] . "\">" . $row["raidID"] . "</a></td><td>" . $row["instance"] . "</td><td>" . $row["bossName"] . "</td><td><a href=\"encounters.php?id=" . $row["encounterID"] . "\">" . formatTimeAbs($row["killedAt"]) . "</a></td></tr>";
	}
	if ($table != "") {
		echo "<table><tr><th>Raid</th><th>Instance</th><th>Boss</th><th>Killed At</th></tr>" . $table . "</table></div></div>";
	}
?>
</body>
</html>
