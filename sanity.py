#!/usr/bin/python3

# Copyright 2018,2019 Hagson

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sqlite3

def testTooManyBossesInARaid(c):
	passed = True
	query = "SELECT * FROM raids_guild WHERE instance=? AND numBosses>?"

	vals = {}
	vals[0] = ("Onyxia's Lair", 1)
	vals[1] = ("Molten Core", 10)
	vals[2] = ("Blackwing Lair", 8)
	vals[3] = ("Temple of Ahn'Qiraj", 9)
	vals[4] = ("Naxxramas", 15)

	for i in range(4):
		c.execute(query, vals[i])
		ans = c.fetchall()
		if len(ans) != 0:
			print("Test testTooManyBossesInARaid failed with query " + query + " and vals (" + vals[i][0] + ", " + str(vals[i][1]) + ")")
			passed = False
	return passed

def testTooManyPeopleInAnEncounter(c):
	passed = True
	query = "SELECT COUNT(*) FROM encounters_player WHERE encounterID=?"

	c.execute("SELECT encounterID FROM encounters_guild")
	encs = c.fetchall()

	for i in range(len(encs)):
		c.execute(query, encs[i])
		ans = c.fetchall()
		if ans[0][0] > 40:
			print("Test testTooManyPeopleInAnEncounter failed with query " + query + " and vals " + encs[i])
			passed = False
	return passed

def testB5o6BetterThanBest(c):
	print("TODO")

def testBossThatShouldntBeInIsIn(c):
	passed = True
	query = "SELECT * FROM encounters_guild WHERE bossName=?"
	vals = ["Princess Yauj", "Vem", "Lord Kri", "Emperor Vek'lor", "Emperor Vek'nilash", "Eye of C'Thun", "Thane Korth'azz", "Highlord Mograine", "Lady Blaumeux", "Sir Zeliek"]
	for v in vals:
		c.execute(query, (v,))
		if len(c.fetchall()) != 0:
			print("Test testBossThatShouldntBeInIsIn failed with query " + query + " and vals " + v)
			passed = False
	return passed

def testMultipleSameBoss(c):
	passed = True
	query = "SELECT raidID, cnt FROM (SELECT raidID, COUNT(*) AS cnt FROM encounters_guild WHERE bossName=? GROUP BY raidID) WHERE cnt!=1"
	legalBosses = [	"Onyxia",
				"Lucifron", "Magmadar", "Gehennas", "Garr", "Baron Geddon", "Shazzrah", "Sulfuron Harbinger", "Golemagg the Incinerator", "Majordomo Executus", "Ragnaros",
				"Razorgore the Untamed", "Vaelastrasz the Corrupt", "Broodlord Lashlayer", "Firemaw", "Ebonroc", "Flamegor", "Chromaggus", "Nefarian",
				"The Prophet Skeram", "Bug Trio", "Battleguard Sartura", "Fankriss the Unyielding", "Viscidus", "Princess Huhuran", "Twin Emperors", "Ouro", "C'Thun",
				"Patchwerk", "Grobbulus", "Gluth", "Thaddius", "Anub'Rekhan", "Grand Widow Faerlina", "Maexxna", "Noth the Plaguebringer", "Heigan the Unclean", "Loatheb", "Instructor Razuvious", "Gothik the Harvester", "Four Horsemen", "Sapphiron", "Kel'Thuzad" ]
	for boss in legalBosses:
		c.execute(query, (boss,))
		res = c.fetchall()
		if len(res) != 0:
			raidID, cnt = res[0][0], res[0][1]
			print("Test testMultipleSameBoss failed with query " + query + " and vals " + boss + ". Raid " + str(raidID) + " has " + str(cnt) + " " + boss)


def main():
	conn = sqlite3.connect("logs.db")
	c = conn.cursor()

	print("Running testTooManyBossesInARaid")
	if testTooManyBossesInARaid(c):
		print("Passed!")

	print("Running testTooManyPeopleInAnEncounter")
	if testTooManyPeopleInAnEncounter(c):
		print("Passed!")

	print("Running testBossThatShouldntBeInIsIn")
	if testBossThatShouldntBeInIsIn(c):
		print("Passed!")

	print("Running testMultipleSameBoss")
	if testMultipleSameBoss(c):
		print("Passed!")

main()
