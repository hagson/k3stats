<!-- Copyright 2018,2019 Hagson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

<?php
	require("util.php");
	if (isset($_GET["guild"]) && $_GET["guild"] != "") {
		$guild = deniceify(htmlspecialchars($_GET["guild"]));
	} else {
		$guild = "";
	}
	if (isset($_GET["class"]) && $_GET["class"] != "") {
		$playerclass = htmlspecialchars($_GET["class"]);
		$classtext = ucfirst($playerclass);
	} else {
		$playerclass = "";
		$classtext = "Any Class";
	}
	$legalstats = ["playerItemLevel", "dmgDone", "healingDone", "dmgTaken", "healingTaken", "overhealingDone", "dmgAbsorbed", "absorbDone", "dps", "interrupts", "dispels", "hps", "deaths", "resurrections"];
	if (isset($_GET["stat"]) && $_GET["stat"] != "" && !in_array(htmlspecialchars($_GET["stat"]), $legalstats)) {
		echoInitial("Error", false, false);
		echo("<h1>Can't rank by " . htmlspecialchars($_GET["stat"]) . "</h1>");
		die();
	} elseif (isset($_GET["stat"]) && $_GET["stat"] != "") {
		$stat = htmlspecialchars($_GET["stat"]);
		$stattext = statName($stat);
	} else {
		$stat = "";
		$stattext = "Overview";
	}

	$filtersNoStat = "<form method=\"GET\">
		Guild:
		<input type=\"text\" name=\"guild\" value=\"" . niceify($guild) . "\" class=\"raidfilter\">
		<select name=\"class\" class=\"raidfilter\">
			<option value=\"" . $playerclass . "\" selected hidden>" . $classtext . "</option>
			<option value=\"\">Any class</option>
			<option value=\"druid\">Druid</option>
			<option value=\"hunter\">Hunter</option>
			<option value=\"mage\">Mage</option>
			<option value=\"paladin\">Paladin</option>
			<option value=\"priest\">Priest</option>
			<option value=\"rogue\">Rogue</option>
			<option value=\"shaman\">Shaman</option>
			<option value=\"warlock\">Warlock</option>
			<option value=\"warrior\">Warrior</option>
		</select>";
	$filtersStat = $filtersNoStat .
		"<select name=\"stat\" class=\"raidfilter\">
			<option value=\"" . $stat . "\" selected hidden>" . $stattext . "</option>
			<option value=\"\">Overview</option>
			<option value=\"dps\">DPS</option>
			<option value=\"hps\">HPS</option>
			<option value=\"dmgDone\">Damage Done</option>
			<option value=\"healingDone\">Healing Done</option>
			<option value=\"dmgTaken\">Damage Taken</option>
			<option value=\"healingTaken\">Healing Taken</option>
			<option value=\"overhealingDone\">Overhealing Done</option>
			<option value=\"absorbDone\">Absorption Done</option>
			<option value=\"dmgAbsorbed\">Damage Absorbed</option>
			<option value=\"interrupts\">Interrupts</option>
			<option value=\"dispels\">Dispels</option>
			<option value=\"deaths\">Deaths</option>
			<!--<option value=\"resurrections\">Times Resurrected</option>
			<option value=\"playerItemLevel\">Item Level</option>-->
		</select>
		<br>";
	$filtersNoStat .= "<br><input type=\"hidden\" name=\"stat\" value=\"" . $stat . "\">";
	if (isset($_GET["multiple"]) && htmlspecialchars($_GET["multiple"]) == "yes") {
		$filtersNoStat .= "<label>Allow multiple entries</label><input type=\"checkbox\" name=\"multiple\" value=\"yes\" checked=\"checked\" class=\"raidfilter\">";
		$filtersStat .= "<label>Allow multiple entries</label><input type=\"checkbox\" name=\"multiple\" value=\"yes\" checked=\"checked\" class=\"raidfilter\">";
		$multiple = true;
	} else {
		$filtersNoStat .= "<label>Allow multiple entries</label><input type=\"checkbox\" name=\"multiple\" value=\"yes\" class=\"raidfilter\">";
		$filtersStat .= "<label>Allow multiple entries</label><input type=\"checkbox\" name=\"multiple\" value=\"yes\" class=\"raidfilter\">";
		$multiple= false;
	}
	$filtersNoStat .= "<input type=\"submit\" value=\"Apply filters\">";
	$filtersStat .= "<input type=\"submit\" value=\"Apply filters\">";

	if ($guild == "") {
		$guild = "%";
	}
	if ($playerclass == "") {
		$playerclass = "%";
	}

	$db = getDB();

	if (isset($_GET["stat"]) && $_GET["stat"] != "" && isset($_GET["boss"]) && $_GET["boss"] != "") { // Records for a specific stat and boss
		if ($multiple) {
			$statement = $db->prepare("SELECT playerName, playerClass, guildName, faction, killedAt, encounterID, encounters_player." . $stat . "
			FROM encounters_player JOIN encounters_guild USING (encounterID)
			WHERE bossName = :boss AND guildName LIKE \"" . $guild . "\" AND playerClass LIKE \"" . getClassID(htmlspecialchars($playerclass)) . "\"
			ORDER BY encounters_player." . $stat . " DESC LIMIT 50");
		} else {
			$statement = $db->prepare("SELECT playerName, playerClass, guildName, faction, killedAt, encounterID, max(encounters_player." . $stat . ") AS " . $stat . "
			FROM encounters_player JOIN encounters_guild USING (encounterID)
			WHERE bossName = :boss AND guildName LIKE \"" . $guild . "\" AND playerClass LIKE \"" . getClassID(htmlspecialchars($playerclass)) . "\"
			GROUP BY playerName
			ORDER BY encounters_player." . $stat . " DESC LIMIT 50");
		}
		$statement->bindValue(":boss", htmlspecialchars($_GET["boss"]));
		$b5o6statement = $db->prepare("SELECT playerName, playerClass, guildName, faction, " . $stat . "
		FROM b5o6_encounters_player
		WHERE bossName = :boss AND guildName LIKE :guild AND playerClass LIKE :class AND numKills >= 6
		ORDER BY " . $stat . " DESC LIMIT 50");
		$b5o6statement->bindValue(":boss", htmlspecialchars($_GET["boss"]));
		$b5o6statement->bindValue(":guild", $guild);
		$b5o6statement->bindValue(":class", getClassID(htmlspecialchars($playerclass)));
		$res = $statement->execute();
		$b5o6res = $b5o6statement->execute();

		if ($playerclass != "%") {
			echoInitial($_GET["boss"] . " " . ucfirst($playerclass) . " " . $stattext . " records", false, true);
			echo "<h1>Boss Records - " . $_GET["boss"] . " - " . ucfirst($playerclass) . " " . $stattext . "</h1>";
		} else {
			echoInitial($_GET["boss"] . " " . $stattext . " records", false, true);
			echo "<h1>Boss Records - " . $_GET["boss"] . " - " . $stattext . "</h1>";
		}
		echo $filtersStat . "<input type=\"hidden\" name=\"boss\" value=\"" . $_GET["boss"] . "\"></form>";

		echo "<div style=\"width: auto; margin: auto; display: inline-block\"><div style=\"width: auto; margin: auto; float: left; margin-right: 100px\"><h2>All time records</h2>";
		echo playerBossRecordsDisplay($res, "", $stat, $stattext);
		echo "</div><div style=\"width: auto; margin: auto; float: right\"><h2 title=\"Average of the best 5 of the last 6 entries\" style=\"cursor: help\">Average Performance</h2>";
		echo b5o6_playerBossRecordsDisplay($b5o6res, "", $stat, $stattext);
		echo "</div></div>";
	} elseif (isset($_GET["stat"]) && $_GET["stat"] != "" && isset($_GET["instance"]) && $_GET["instance"] != "") { // Records for a specific stat and instance
		$instance = $instancelong[htmlspecialchars($_GET["instance"])];
		if ($multiple) {
			$statement = $db->prepare("SELECT playerName, playerClass, raidID, guildName, faction, " . $stat . "
			FROM raids_player JOIN raids_guild USING (raidID)
			WHERE instance = :instance AND raids_player.numBosses=" . sizeof($bosses[$instance]) . " AND guildName LIKE \"" . $guild . "\" AND playerClass LIKE \"" . getClassID(htmlspecialchars($playerclass)) . "\"
			ORDER BY " . $stat . " DESC LIMIT 50");
		} else {
			$statement = $db->prepare("SELECT playerName, playerClass, raidID, guildName, faction, max(" . $stat . ") AS " . $stat . "
			FROM raids_player JOIN raids_guild USING (raidID)
			WHERE instance = :instance AND raids_player.numBosses=" . sizeof($bosses[$instance]) . " AND guildName LIKE \"" . $guild . "\" AND playerClass LIKE \"" . getClassID(htmlspecialchars($playerclass)) . "\"
			GROUP BY playerName
			ORDER BY " . $stat . " DESC LIMIT 50");
		}
		$statement->bindValue(":instance", $instance);
		$b5o6statement = $db->prepare("SELECT playerName, playerClass, guildName, faction, " . $stat . "
		FROM b5o6_raids_player
		WHERE instance = :instance AND guildName LIKE :guild AND playerClass LIKE :class AND numClears >= 6
		ORDER BY " . $stat . " DESC LIMIT 50");
		$b5o6statement->bindValue(":instance", $instance);
		$b5o6statement->bindValue(":guild", $guild);
		$b5o6statement->bindValue(":class", getClassID(htmlspecialchars($playerclass)));
		$res = $statement->execute();
		$b5o6res = $b5o6statement->execute();

		if ($playerclass != "%") {
			echoInitial($instance . " " . ucfirst($playerclass) . " " . $stattext . " records", false, true);
			echo "<h1>Instance Records - " . $instance . " - " . ucfirst($playerclass) . " " . $stattext . "</h1>";
		} else {
			echoInitial($instance . " " . $stattext . " records", false, true);
			echo "<h1>Instance Records - " . $instance . " - " . $stattext . "</h1>";
		}
		echo $filtersStat . "<input type=\"hidden\" name=\"instance\" value=\"" . $_GET["instance"] . "\"></form>";
		echo "<div style=\"width: auto; margin: auto; display: inline-block\"><div style=\"width: auto; margin: auto; float: left; margin-right: 100px\"><h2>All time records</h2>";
		echo playerRaidRecordsDisplay($res, "", $stat, $stattext);
		echo "</div><div style=\"width: auto; margin: auto; float: right\"><h2 title=\"Average of the best 5 of the last 6 entries\" style=\"cursor: help\">Average Performance</h2>";
		echo b5o6_playerRaidRecordsDisplay($b5o6res, "", $stat, $stattext);
		echo "</div></div>";
	} elseif (isset($_GET["boss"]) && $_GET["boss"] != "") { // Generic records for a specific boss
		if ($playerclass != "%") {
			echoInitial($_GET["boss"] . " " . ucfirst($playerclass) . " records", false, true);
			echo "<h1>Boss Records - " . $_GET["boss"] . " - " . ucfirst($playerclass) . "\n</h1>\n";
		} else {
			echoInitial($_GET["boss"] . " records", false, true);
			echo "<h1>Boss Records - " . $_GET["boss"] . "\n</h1>\n";
		}
		echo $filtersStat . "<input type=\"hidden\" name=\"boss\" value=\"" . $_GET["boss"] . "\"></form>";

		if ($multiple) {
			$killtime = $db->prepare("SELECT encounterID, guildName, faction, fightLength, killedAt FROM encounters_guild WHERE bossName= :boss AND guildName LIKE \"" . $guild . "\" ORDER BY fightLength ASC LIMIT 15");
			$dps = $db->prepare("SELECT dps, playerName, playerClass, guildName, faction, killedAt, encounterID FROM encounters_player JOIN encounters_guild USING (encounterID) WHERE bossName = :name AND guildName LIKE \"" . $guild . "\" AND playerClass LIKE \"" . getClassID(htmlspecialchars($playerclass)) . "\" ORDER BY dps DESC LIMIT 25");
			$hps = $db->prepare("SELECT hps, playerName, playerClass, guildName, faction, killedAt, encounterID FROM encounters_player JOIN encounters_guild USING (encounterID) WHERE bossName = :name AND guildName LIKE \"" . $guild . "\" AND playerClass LIKE \"" . getClassID(htmlspecialchars($playerclass)) . "\" ORDER BY hps DESC LIMIT 25");
		} else {
			$killtime = $db->prepare("SELECT encounterID, guildName, faction, min(fightLength) AS fightLength, killedAt FROM encounters_guild WHERE bossName= :boss AND guildName LIKE \"" . $guild . "\" GROUP BY guildName ORDER BY fightLength ASC LIMIT 15");
			$dps = $db->prepare("SELECT max(dps) AS dps, playerName, playerClass, guildName, faction, killedAt, encounterID FROM encounters_player JOIN encounters_guild USING (encounterID) WHERE bossName = :name AND guildName LIKE \"" . $guild . "\" AND playerClass LIKE \"" . getClassID(htmlspecialchars($playerclass)) . "\" GROUP BY playerName ORDER BY dps DESC LIMIT 25");
			$hps = $db->prepare("SELECT max(hps) AS hps, playerName, playerClass, guildName, faction, killedAt, encounterID FROM encounters_player JOIN encounters_guild USING (encounterID) WHERE bossName = :name AND guildName LIKE \"" . $guild . "\" AND playerClass LIKE \"" . getClassID(htmlspecialchars($playerclass)) . "\" GROUP BY playerName ORDER BY hps DESC LIMIT 25");
		}
		$killtime->bindValue(":boss", htmlspecialchars($_GET["boss"]));
		$killtime = $killtime->execute();
		echo "<div style=\"width: auto; display: inline-block\"><div style=\"width: auto; float: left; margin-right: 100px\">";
		echo killtimeRecordsDisplay($killtime, "All time records");

		$dps->bindValue(":name", htmlspecialchars($_GET["boss"]));
		$dps = $dps->execute();

		$hps->bindValue(":name", htmlspecialchars($_GET["boss"]));
		$hps = $hps->execute();

		echo "<div style=\"width: auto; margin: auto; margin-top: 20px; display: inline-block\">";
		echo playerBossRecordsDisplay($dps, "style=\"float: left; margin-right: 40px\"", "dps", "DPS");
		echo playerBossRecordsDisplay($hps, "style=\"float: right\"", "hps", "HPS");
		echo "</div>";

		$killb5o6 = $db->prepare("SELECT guildName, faction, fightLength FROM b5o6_encounters_guild WHERE bossName = :boss AND numKills >= 6 AND guildName LIKE :guild ORDER BY fightLength ASC LIMIT 15");
		$killb5o6->bindValue(":boss", htmlspecialchars($_GET["boss"]));
		$killb5o6->bindValue(":guild", $guild);
		$DPSb5o6 = $db->prepare("SELECT playerName, playerClass, guildName, faction, dps FROM b5o6_encounters_player WHERE bossName = :boss AND numKills >= 6 AND guildName LIKE :guild AND playerClass LIKE :class ORDER BY dps DESC LIMIT 25");
		$DPSb5o6->bindValue(":boss", htmlspecialchars($_GET["boss"]));
		$DPSb5o6->bindValue(":guild", $guild);
		$DPSb5o6->bindValue(":class", getClassID(htmlspecialchars($playerclass)));
		$HPSb5o6 = $db->prepare("SELECT playerName, playerClass, guildName, faction, hps FROM b5o6_encounters_player WHERE bossName = :boss AND numKills >= 6 AND guildName LIKE :guild AND playerClass LIKE :class ORDER BY hps DESC LIMIT 25");
		$HPSb5o6->bindValue(":boss", htmlspecialchars($_GET["boss"]));
		$HPSb5o6->bindValue(":guild", $guild);
		$HPSb5o6->bindValue(":class", getClassID(htmlspecialchars($playerclass)));

		$killb5o6 = $killb5o6->execute();
		$DPSb5o6 = $DPSb5o6->execute();
		$HPSb5o6 = $HPSb5o6->execute();

		echo "</div><div style=\"width: auto; float: right\">\n";

		echo b5o6_killtimeRecordsDisplay($killb5o6, "All time records");

		echo b5o6_playerBossRecordsDisplay($DPSb5o6, "style=\"float: left; margin-top: 20px; margin-right: 40px\"", "dps", "DPS");
		echo b5o6_playerBossRecordsDisplay($HPSb5o6, "style=\"float: right; margin-top: 20px\"", "hps", "HPS");

		echo "</div></div>";

	} elseif (isset($_GET["instance"]) && $_GET["instance"] != "") { // Generic records for a specific instance
		if (!(in_array($_GET["instance"], ["naxx", "aq", "bwl", "mc", "ony"]))) {
				echoInitial("Error", false, false);
				echo "<h1>Unknown instance</h1>";
				die();
		} else {
			$instance = $instancelong[$_GET["instance"]];
			if ($playerclass != "%") {
				echoInitial($instance . " " . ucfirst($playerclass) . " records", false, true);
				echo "<h1>Instance Records - " . $instance . " - " . ucfirst($playerclass) . "\n</h1>\n";
			} else {
				echoInitial($instance . " records", false, true);
				echo "<h1>Instance Records - " . $instance . "\n</h1>\n";
			}
			echo $filtersStat . "<input type=\"hidden\" name=\"instance\" value=\"" . $_GET["instance"] . "\"></form>";

			if ($multiple) {
				$instanceClear = $db->prepare("SELECT raidID, guildName, faction, startTime, endTime, (endTime - startTime) AS clearTime FROM raids_guild WHERE instance = :instance AND finished = 1 AND guildName LIKE :guildName ORDER BY clearTime ASC LIMIT 15");
				$instanceDPS = $db->prepare("SELECT dps, playerName, playerClass, guildName, faction, raidID FROM raids_player JOIN raids_guild USING (raidID) WHERE instance = :instance AND raids_player.numBosses = :numBosses AND guildName LIKE :guildName AND playerClass LIKE :playerClass ORDER BY dps DESC LIMIT 25");
				$instanceHPS = $db->prepare("SELECT hps, playerName, playerClass, guildName, faction, raidID FROM raids_player JOIN raids_guild USING (raidID) WHERE instance = :instance AND raids_player.numBosses = :numBosses AND guildName LIKE :guildName AND playerClass LIKE :playerClass ORDER BY hps DESC LIMIT 25");
			} else {
				$instanceClear = $db->prepare("SELECT raidID, guildName, faction, startTime, endTime, min(endTime - startTime) AS clearTime FROM raids_guild WHERE instance = :instance AND finished = 1 AND guildName LIKE :guildName GROUP BY guildName ORDER BY clearTime ASC limit 15");
				$instanceDPS = $db->prepare("SELECT max(dps) AS dps, playerName, playerClass, guildName, faction, raidID FROM raids_player JOIN raids_guild USING (raidID) WHERE instance = :instance AND raids_player.numBosses = :numBosses AND guildName LIKE :guildName AND playerClass LIKE :playerClass GROUP BY playerName ORDER BY dps DESC LIMIT 25");
				$instanceHPS = $db->prepare("SELECT max(hps) AS hps, playerName, playerClass, guildName, faction, raidID FROM raids_player JOIN raids_guild USING (raidID) WHERE instance = :instance AND raids_player.numBosses = :numBosses AND guildName LIKE :guildName AND playerClass LIKE :playerClass GROUP BY playerName ORDER BY hps DESC LIMIT 25");
			}
			$instanceClear->bindValue(":instance", $instance);
			$instanceClear->bindValue(":guildName", $guild);

			$instanceDPS->bindValue(":instance", $instance);
			$instanceDPS->bindValue(":numBosses", sizeof($bosses[$instance]));
			$instanceDPS->bindValue(":guildName", $guild);
			$instanceDPS->bindValue(":playerClass", getClassID(htmlspecialchars($playerclass)));

			$instanceHPS->bindValue(":instance", $instance);
			$instanceHPS->bindValue(":numBosses", sizeof($bosses[$instance]));
			$instanceHPS->bindValue(":guildName", $guild);
			$instanceHPS->bindValue(":playerClass", getClassID(htmlspecialchars($playerclass)));

			$instanceClear = $instanceClear->execute();
			$instanceDPS = $instanceDPS->execute();
			$instanceHPS = $instanceHPS->execute();

			echo "<div style=\"width: auto; margin: auto; display: inline-block\"><div style=\"width: auto; float: left; margin: auto; margin-top: 20px; margin-right: 40px\">\n";
			$table = "<table><tr><th>Bossname</th></tr>\n";
			for ($i = 0; $i < sizeof($bosses[$instance]); $i++) {
				$table .= "<tr><td><a href=\"records.php?boss=" . $bosses[$instance][$i] . "\">" . $bosses[$instance][$i] . "</a></td></tr>\n";
			}
			echo $table . "</table></div>\n<div style=\"float: right\"><div style=\"float: left; margin-right: 100px\">";
			list($table, $rows) = instanceRecordsDisplay($instanceClear, "All time records");
			echo $table;
			// echo instanceRecordsDisplay($instanceClear, "All time records");
			echo "<div style=\"margin: auto; width: auto\">";
			echo playerRaidRecordsDisplay($instanceDPS, "style=\"float: left; margin-top: 20px; display:inline-block; margin-right: 40px\"", "dps", "DPS");
			echo playerRaidRecordsDisplay($instanceHPS, "style=\"float: right; margin-top: 20px\"", "hps", "HPS");
			echo ("</div></div><div style=\"float: right\">");

			$clearb5o6 = $db->prepare("SELECT guildName, faction, clearTime FROM b5o6_raids_guild_cleartime WHERE numClears >= 6 AND guildName LIKE :guild AND instance = :instance ORDER BY clearTime ASC LIMIT 15");
			$clearb5o6->bindValue(":guild", $guild);
			$clearb5o6->bindValue(":instance", $instance);
			$DPSb5o6 = $db->prepare("SELECT dps, playerName, playerClass, guildName, faction FROM b5o6_raids_player WHERE numClears >= 6 AND guildName LIKE :guild AND playerClass LIKE :class AND instance = :instance ORDER BY " . $sqlinstanceorder . ", dps DESC LIMIT 25");
			$DPSb5o6->bindValue(":guild", $guild);
			$DPSb5o6->bindValue(":instance", $instance);
			$DPSb5o6->bindValue(":class", getClassID(htmlspecialchars($playerclass)));
			$HPSb5o6 = $db->prepare("SELECT hps, playerName, playerClass, guildName, faction FROM b5o6_raids_player WHERE numClears >= 6 AND guildName LIKE :guild AND playerClass LIKE :class AND instance = :instance ORDER BY " . $sqlinstanceorder . ", hps DESC LIMIT 25");
			$HPSb5o6->bindValue(":guild", $guild);
			$HPSb5o6->bindValue(":instance", $instance);
			$HPSb5o6->bindValue(":class", getClassID(htmlspecialchars($playerclass)));

			$clearb5o6 = $clearb5o6->execute();
			$DPSb5o6 = $DPSb5o6->execute();
			$HPSb5o6 = $HPSb5o6->execute();

			echo b5o6_instanceRecordsDisplay($clearb5o6, $rows);

			echo "<div style=\"margin: auto; width: auto; display: inline-block\">";
			echo b5o6_playerBossRecordsDisplay($DPSb5o6, "style=\"float: left; margin-top: 20px; margin-right: 40px\"", "dps", "DPS");
			echo b5o6_playerBossRecordsDisplay($HPSb5o6, "style=\"float: right; margin-top: 20px\"", "hps", "HPS");

			echo "</div></div></div>\n";
		}
	} else { // Generic records for instances
		echoInitial("Records overview", false, true);
		echo "<h1>Instance Records</h1>";
		echo $filtersNoStat . "</form>";

		function buildSQLClearStmt($multiple, $limit) { // User input is never concated
			$str = "SELECT raidID, guildName, faction, startTime, endTime, ";
			if (!$multiple) {
				$str .= "min";
			}
			$str .= "(endTime - startTime) AS clearTime FROM raids_guild WHERE instance = :instance AND finished=1 AND guildName LIKE :guildName ";
			if (!$multiple) {
				$str .= "GROUP BY guildName ";
			}
			$str .= "ORDER BY clearTime ASC LIMIT " . $limit;
			return $str;
		}

		function buildSQLStatStmt($multiple, $stat, $limit) { // User input is never concated
			$str = "SELECT ";
			if ($multiple) {
				$str .= $stat;
			} else {
				$str .= "max(" . $stat . ") AS " . $stat;
			}
			$str .= ", playerName, playerClass, guildName, faction, raidID FROM raids_player JOIN raids_guild USING (raidID) WHERE instance = :instance AND raids_player.numBosses = :numBosses AND playerClass LIKE :playerClass AND guildName LIKE :guildName ";
			if (!$multiple) {
				$str .= "GROUP BY playerName ";
			}
			$str .= "ORDER BY " . $stat . " DESC LIMIT " . $limit;
			return $str;
		}

		$clearstr = buildSQLClearStmt($multiple, 10);
		$dpsstr = buildSQLStatStmt($multiple, "dps", 15);
		$hpsstr = buildSQLStatStmt($multiple, "hps", 15);

		$naxxClear = $db->prepare($clearstr);
		$aqClear = $db->prepare($clearstr);
		$bwlClear = $db->prepare($clearstr);
		$mcClear = $db->prepare($clearstr);
		$onyClear = $db->prepare($clearstr);
		$naxxClear->bindValue(":instance", "Naxxramas");
		$naxxClear->bindValue(":guildName", $guild);
		$aqClear->bindValue(":instance", "Temple of Ahn'Qiraj");
		$aqClear->bindValue(":guildName", $guild);
		$bwlClear->bindValue(":instance", "Blackwing Lair");
		$bwlClear->bindValue(":guildName", $guild);
		$mcClear->bindValue(":instance", "Molten Core");
		$mcClear->bindValue(":guildName", $guild);
		$onyClear->bindValue(":instance", "Onyxia's Lair");
		$onyClear->bindValue(":guildName", $guild);

		$naxxDPS = $db->prepare($dpsstr);
		$aqDPS = $db->prepare($dpsstr);
		$bwlDPS = $db->prepare($dpsstr);
		$mcDPS = $db->prepare($dpsstr);
		$onyDPS = $db->prepare($dpsstr);
		$naxxDPS->bindValue(":instance", "Naxxramas");
		$naxxDPS->bindValue(":numBosses", sizeof($bosses["Naxxramas"]));
		$naxxDPS->bindValue(":playerClass", getClassID(htmlspecialchars($playerclass)));
		$naxxDPS->bindValue(":guildName", $guild);
		$aqDPS->bindValue(":instance", "Temple of Ahn'Qiraj");
		$aqDPS->bindValue(":numBosses", sizeof($bosses["Temple of Ahn'Qiraj"]));
		$aqDPS->bindValue(":playerClass", getClassID(htmlspecialchars($playerclass)));
		$aqDPS->bindValue(":guildName", $guild);
		$bwlDPS->bindValue(":instance", "Blackwing Lair");
		$bwlDPS->bindValue(":numBosses", sizeof($bosses["Blackwing Lair"]));
		$bwlDPS->bindValue(":playerClass", getClassID(htmlspecialchars($playerclass)));
		$bwlDPS->bindValue(":guildName", $guild);
		$mcDPS->bindValue(":instance", "Molten Core");
		$mcDPS->bindValue(":numBosses", sizeof($bosses["Molten Core"]));
		$mcDPS->bindValue(":playerClass", getClassID(htmlspecialchars($playerclass)));
		$mcDPS->bindValue(":guildName", $guild);
		$onyDPS->bindValue(":instance", "Onyxia's Lair");
		$onyDPS->bindValue(":numBosses", sizeof($bosses["Onyxia's Lair"]));
		$onyDPS->bindValue(":playerClass", getClassID(htmlspecialchars($playerclass)));
		$onyDPS->bindValue(":guildName", $guild);

		$naxxHPS = $db->prepare($hpsstr);
		$aqHPS = $db->prepare($hpsstr);
		$bwlHPS = $db->prepare($hpsstr);
		$mcHPS = $db->prepare($hpsstr);
		$onyHPS = $db->prepare($hpsstr);
		$naxxHPS->bindValue(":instance", "Naxxramas");
		$naxxHPS->bindValue(":numBosses", sizeof($bosses["Naxxramas"]));
		$naxxHPS->bindValue(":playerClass", getClassID(htmlspecialchars($playerclass)));
		$naxxHPS->bindValue(":guildName", $guild);
		$aqHPS->bindValue(":instance", "Temple of Ahn'Qiraj");
		$aqHPS->bindValue(":numBosses", sizeof($bosses["Temple of Ahn'Qiraj"]));
		$aqHPS->bindValue(":playerClass", getClassID(htmlspecialchars($playerclass)));
		$aqHPS->bindValue(":guildName", $guild);
		$bwlHPS->bindValue(":instance", "Blackwing Lair");
		$bwlHPS->bindValue(":numBosses", sizeof($bosses["Blackwing Lair"]));
		$bwlHPS->bindValue(":playerClass", getClassID(htmlspecialchars($playerclass)));
		$bwlHPS->bindValue(":guildName", $guild);
		$mcHPS->bindValue(":instance", "Molten Core");
		$mcHPS->bindValue(":numBosses", sizeof($bosses["Molten Core"]));
		$mcHPS->bindValue(":playerClass", getClassID(htmlspecialchars($playerclass)));
		$mcHPS->bindValue(":guildName", $guild);
		$onyHPS->bindValue(":instance", "Onyxia's Lair");
		$onyHPS->bindValue(":numBosses", sizeof($bosses["Onyxia's Lair"]));
		$onyHPS->bindValue(":playerClass", getClassID(htmlspecialchars($playerclass)));
		$onyHPS->bindValue(":guildName", $guild);

		$naxxClear = $naxxClear->execute();
		$aqClear = $aqClear->execute();
		$bwlClear = $bwlClear->execute();
		$mcClear = $mcClear->execute();
		$onyClear = $onyClear->execute();

		$naxxDPS = $naxxDPS->execute();
		$aqDPS = $aqDPS->execute();
		$bwlDPS = $bwlDPS->execute();
		$mcDPS = $mcDPS->execute();
		$onyDPS = $onyDPS->execute();

		$naxxHPS = $naxxHPS->execute();
		$aqHPS = $aqHPS->execute();
		$bwlHPS = $bwlHPS->execute();
		$mcHPS = $mcHPS->execute();
		$onyHPS = $onyHPS->execute();

		$b5o6_naxxClear = $db->prepare("SELECT guildName, faction, clearTime FROM b5o6_raids_guild_cleartime WHERE numClears >= 6 AND guildName LIKE :guild AND instance = \"Naxxramas\" ORDER BY clearTime ASC LIMIT 10");
		$b5o6_aqClear = $db->prepare("SELECT guildName, faction, clearTime FROM b5o6_raids_guild_cleartime WHERE numClears >= 6 AND guildName LIKE :guild AND instance = \"Temple of Ahn'Qiraj\" ORDER BY clearTime ASC LIMIT 10");
		$b5o6_bwlClear = $db->prepare("SELECT guildName, faction, clearTime FROM b5o6_raids_guild_cleartime WHERE numClears >= 6 AND guildName LIKE :guild AND instance = \"Blackwing Lair\" ORDER BY clearTime ASC LIMIT 10");
		$b5o6_mcClear = $db->prepare("SELECT guildName, faction, clearTime FROM b5o6_raids_guild_cleartime WHERE numClears >= 6 AND guildName LIKE :guild AND instance = \"Molten Core\" ORDER BY clearTime ASC LIMIT 10");
		$b5o6_onyClear = $db->prepare("SELECT guildName, faction, clearTime FROM b5o6_raids_guild_cleartime WHERE numClears >= 6 AND guildName LIKE :guild AND instance = \"Onyxia's Lair\" ORDER BY clearTime ASC LIMIT 10");
		$b5o6_naxxClear->bindValue(":guild", $guild);
		$b5o6_aqClear->bindValue(":guild", $guild);
		$b5o6_bwlClear->bindValue(":guild", $guild);
		$b5o6_mcClear->bindValue(":guild", $guild);
		$b5o6_onyClear->bindValue(":guild", $guild);

		$b5o6_naxxDPS = $db->prepare("SELECT dps, playerName, playerClass, guildName, faction FROM b5o6_raids_player WHERE numClears >= 6 AND guildName LIKE :guild AND playerClass LIKE :class AND instance = \"Naxxramas\" ORDER BY dps DESC LIMIT 15");
		$b5o6_aqDPS = $db->prepare("SELECT dps, playerName, playerClass, guildName, faction FROM b5o6_raids_player WHERE numClears >= 6 AND guildName LIKE :guild AND playerClass LIKE :class AND instance = \"Temple of Ahn'Qiraj\" ORDER BY dps DESC LIMIT 15");
		$b5o6_bwlDPS = $db->prepare("SELECT dps, playerName, playerClass, guildName, faction FROM b5o6_raids_player WHERE numClears >= 6 AND guildName LIKE :guild AND playerClass LIKE :class AND instance = \"Blackwing Lair\" ORDER BY dps DESC LIMIT 15");
		$b5o6_mcDPS = $db->prepare("SELECT dps, playerName, playerClass, guildName, faction FROM b5o6_raids_player WHERE numClears >= 6 AND guildName LIKE :guild AND playerClass LIKE :class AND instance = \"Molten Core\" ORDER BY dps DESC LIMIT 15");
		$b5o6_onyDPS = $db->prepare("SELECT dps, playerName, playerClass, guildName, faction FROM b5o6_raids_player WHERE numClears >= 6 AND guildName LIKE :guild AND playerClass LIKE :class AND instance = \"Onyxia's Lair\" ORDER BY dps DESC LIMIT 15");
		$b5o6_naxxDPS->bindValue(":guild", $guild);
		$b5o6_naxxDPS->bindValue(":class", getClassID(htmlspecialchars($playerclass)));
		$b5o6_aqDPS->bindValue(":guild", $guild);
		$b5o6_aqDPS->bindValue(":class", getClassID(htmlspecialchars($playerclass)));
		$b5o6_bwlDPS->bindValue(":guild", $guild);
		$b5o6_bwlDPS->bindValue(":class", getClassID(htmlspecialchars($playerclass)));
		$b5o6_mcDPS->bindValue(":guild", $guild);
		$b5o6_mcDPS->bindValue(":class", getClassID(htmlspecialchars($playerclass)));
		$b5o6_onyDPS->bindValue(":guild", $guild);
		$b5o6_onyDPS->bindValue(":class", getClassID(htmlspecialchars($playerclass)));

		$b5o6_naxxHPS = $db->prepare("SELECT hps, playerName, playerClass, guildName, faction FROM b5o6_raids_player WHERE numClears >= 6 AND guildName LIKE :guild AND playerClass LIKE :class AND instance = \"Naxxramas\" ORDER BY hps DESC LIMIT 15");
		$b5o6_aqHPS = $db->prepare("SELECT hps, playerName, playerClass, guildName, faction FROM b5o6_raids_player WHERE numClears >= 6 AND guildName LIKE :guild AND playerClass LIKE :class AND instance = \"Temple of Ahn'Qiraj\" ORDER BY hps DESC LIMIT 15");
		$b5o6_bwlHPS = $db->prepare("SELECT hps, playerName, playerClass, guildName, faction FROM b5o6_raids_player WHERE numClears >= 6 AND guildName LIKE :guild AND playerClass LIKE :class AND instance = \"Blackwing Lair\" ORDER BY hps DESC LIMIT 15");
		$b5o6_mcHPS = $db->prepare("SELECT hps, playerName, playerClass, guildName, faction FROM b5o6_raids_player WHERE numClears >= 6 AND guildName LIKE :guild AND playerClass LIKE :class AND instance = \"Molten Core\" ORDER BY hps DESC LIMIT 15");
		$b5o6_onyHPS = $db->prepare("SELECT hps, playerName, playerClass, guildName, faction FROM b5o6_raids_player WHERE numClears >= 6 AND guildName LIKE :guild AND playerClass LIKE :class AND instance = \"Onyxia's Lair\" ORDER BY hps DESC LIMIT 15");
		$b5o6_naxxHPS->bindValue(":guild", $guild);
		$b5o6_naxxHPS->bindValue(":class", getClassID(htmlspecialchars($playerclass)));
		$b5o6_aqHPS->bindValue(":guild", $guild);
		$b5o6_aqHPS->bindValue(":class", getClassID(htmlspecialchars($playerclass)));
		$b5o6_bwlHPS->bindValue(":guild", $guild);
		$b5o6_bwlHPS->bindValue(":class", getClassID(htmlspecialchars($playerclass)));
		$b5o6_mcHPS->bindValue(":guild", $guild);
		$b5o6_mcHPS->bindValue(":class", getClassID(htmlspecialchars($playerclass)));
		$b5o6_onyHPS->bindValue(":guild", $guild);
		$b5o6_onyHPS->bindValue(":class", getClassID(htmlspecialchars($playerclass)));

		$b5o6_naxxClear = $b5o6_naxxClear->execute();
		$b5o6_aqClear = $b5o6_aqClear->execute();
		$b5o6_bwlClear = $b5o6_bwlClear->execute();
		$b5o6_mcClear = $b5o6_mcClear->execute();
		$b5o6_onyClear = $b5o6_onyClear->execute();

		$b5o6_naxxDPS = $b5o6_naxxDPS->execute();
		$b5o6_aqDPS = $b5o6_aqDPS->execute();
		$b5o6_bwlDPS = $b5o6_bwlDPS->execute();
		$b5o6_mcDPS = $b5o6_mcDPS->execute();
		$b5o6_onyDPS = $b5o6_onyDPS->execute();

		$b5o6_naxxHPS = $b5o6_naxxHPS->execute();
		$b5o6_aqHPS = $b5o6_aqHPS->execute();
		$b5o6_bwlHPS = $b5o6_bwlHPS->execute();
		$b5o6_mcHPS = $b5o6_mcHPS->execute();
		$b5o6_onyHPS = $b5o6_onyHPS->execute();

		list($naxxTable, $rows) = instanceRecordsDisplay($naxxClear, "All time records");
		if ($naxxTable != "") {
			echo ("<div style=\"width: auto; margin: auto; display: inline-block\">\n");
			echo ("<h1 style=\"clear: both; padding-top: 60px; margin-top: 0px\"><a href=\"records.php?instance=naxx\">Naxxramas</a></h1>\n");
			echo ("<div style=\"width: auto; float: left; margin-right: 140px\">\n");
			echo $naxxTable;
			echo playerRaidRecordsDisplay($naxxDPS, "style=\"float: left; margin-top: 20px; margin-right: 40px\"", "dps", "DPS");
			echo playerRaidRecordsDisplay($naxxHPS, "style=\"float: right; margin-top: 20px\"", "hps", "HPS");
			echo ("</div>\n<div style=\"width: auto; float: right\">\n");
			echo b5o6_instanceRecordsDisplay($b5o6_naxxClear, $rows);
			echo b5o6_playerRaidRecordsDisplay($b5o6_naxxDPS, "style=\"float: left; margin-top: 20px; margin-right: 40px\"", "dps", "DPS");
			echo b5o6_playerRaidRecordsDisplay($b5o6_naxxHPS, "style=\"float: right; margin-top: 20px\"", "hps", "HPS");
			echo ("</div>\n</div>\n");
		}

		list($aqTable, $rows) = instanceRecordsDisplay($aqClear, "All time records");
		if ($aqTable != "") {
			echo ("<div style=\"width: auto; margin: auto; display: inline-block\">\n");
			echo ("<h1 style=\"clear: both; padding-top: 60px; margin-top: 0px\"><a href=\"records.php?instance=aq\">Temple of Ahn'Qiraj</a></h1>\n");
			echo ("<div style=\"width: auto; float: left; margin-right: 140px\">\n");
			echo $aqTable;
			echo playerRaidRecordsDisplay($aqDPS, "style=\"float: left; margin-top: 20px; margin-right: 40px\"", "dps", "DPS");
			echo playerRaidRecordsDisplay($aqHPS, "style=\"float: right; margin-top: 20px\"", "hps", "HPS");
			echo ("</div>\n<div style=\"width: auto; float: right\">\n");
			echo b5o6_instanceRecordsDisplay($b5o6_aqClear, $rows);
			echo b5o6_playerRaidRecordsDisplay($b5o6_aqDPS, "style=\"float: left; margin-top: 20px; margin-right: 40px\"", "dps", "DPS");
			echo b5o6_playerRaidRecordsDisplay($b5o6_aqHPS, "style=\"float: right; margin-top: 20px\"", "hps", "HPS");
			echo ("</div>\n</div>\n");
		}

		list($bwlTable, $rows) = instanceRecordsDisplay($bwlClear, "All time records");
		if ($bwlTable != "") {
			echo ("<div style=\"width: auto; margin: auto; display: inline-block\">\n");
			echo ("<h1 style=\"clear: both; padding-top: 60px; margin-top: 0px\"><a href=\"records.php?instance=bwl\">Blackwing Lair</a></h1>\n");
			echo ("<div style=\"width: auto; float: left; margin-right: 140px\">\n");
			echo $bwlTable;
			echo playerRaidRecordsDisplay($bwlDPS, "style=\"float: left; margin-top: 20px; margin-right: 40px\"", "dps", "DPS");
			echo playerRaidRecordsDisplay($bwlHPS, "style=\"float: right; margin-top: 20px\"", "hps", "HPS");
			echo ("</div>\n<div style=\"width: auto; float: right\">\n");
			echo b5o6_instanceRecordsDisplay($b5o6_bwlClear, $rows);
			echo b5o6_playerRaidRecordsDisplay($b5o6_bwlDPS, "style=\"float: left; margin-top: 20px; margin-right: 40px\"", "dps", "DPS");
			echo b5o6_playerRaidRecordsDisplay($b5o6_bwlHPS, "style=\"float: right; margin-top: 20px\"", "hps", "HPS");
			echo ("</div>\n</div>\n");
		}

		list($mcTable, $rows) = instanceRecordsDisplay($mcClear, "All time records");
		if ($mcTable != "") {
			echo ("<div style=\"width: auto; margin: auto; display: inline-block\">\n");
			echo ("<h1 style=\"clear: both; padding-top: 60px; margin-top: 0px\"><a href=\"records.php?instance=mc\">Molten Core</a></h1>\n");
			echo ("<div style=\"width: auto; float: left; margin-right: 140px\">\n");
			echo $mcTable;
			echo playerRaidRecordsDisplay($mcDPS, "style=\"float: left; margin-top: 20px; margin-right: 40px\"", "dps", "DPS");
			echo playerRaidRecordsDisplay($mcHPS, "style=\"float: right; margin-top: 20px\"", "hps", "HPS");
			echo ("</div>\n<div style=\"width: auto; float: right\">\n");
			echo b5o6_instanceRecordsDisplay($b5o6_mcClear, $rows);
			echo b5o6_playerRaidRecordsDisplay($b5o6_mcDPS, "style=\"float: left; margin-top: 20px; margin-right: 40px\"", "dps", "DPS");
			echo b5o6_playerRaidRecordsDisplay($b5o6_mcHPS, "style=\"float: right; margin-top: 20px\"", "hps", "HPS");
			echo ("</div>\n</div>\n");
		}

		list($onyTable, $rows) = instanceRecordsDisplay($onyClear, "All time records");
		if ($onyTable != "") {
			echo ("<div style=\"width: auto; margin: auto; display: inline-block\">\n");
			echo ("<h1 style=\"clear: both; padding-top: 60px; margin-top: 0px\"><a href=\"records.php?instance=ony\">Onyxia's Lair</a></h1>\n");
			echo ("<div style=\"width: auto; float: left; margin-right: 140px\">\n");
			echo $onyTable;
			echo playerRaidRecordsDisplay($onyDPS, "style=\"float: left; margin-top: 20px; margin-right: 40px\"", "dps", "DPS");
			echo playerRaidRecordsDisplay($onyHPS, "style=\"float: right; margin-top: 20px\"", "hps", "HPS");
			echo ("</div>\n<div style=\"width: auto; float: right\">\n");
			echo b5o6_instanceRecordsDisplay($b5o6_onyClear, $rows);
			echo b5o6_playerRaidRecordsDisplay($b5o6_onyDPS, "style=\"float: left; margin-top: 20px; margin-right: 40px\"", "dps", "DPS");
			echo b5o6_playerRaidRecordsDisplay($b5o6_onyHPS, "style=\"float: right; margin-top: 20px\"", "hps", "HPS");
			echo ("</div>\n</div>\n");
		}
	}
?>

</body>

</html>
