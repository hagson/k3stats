<!-- Copyright 2018,2019 Hagson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

<?php
	require("util.php");
	echoInitial("Raid search", false, true);
?>

	<h1>Find Raids</h1>
	<p class="note">
		Empty field means anything will match. <br>
		The maximum number of results is 50. <br>
		If the time fields are empty update your browser or enter dates on the form "yyyy-mm-dd"<br>
		Results are sorted by latest kill
	</p>
	<form method="GET">
		Guild:
		<input type="text" name="guild" value="" class="raidfilter">
		Instance:
		<select name="instance" class="raidfilter">
			<option value="">Any</option>
			<option value="Naxxramas">Naxxramas</option>
			<option value="Temple of Ahn'Qiraj">Temple of Ahn'Qiraj</option>
			<option value="Blackwing Lair">Blackwing Lair</option>
			<option value="Molten Core">Molten Core</option>
			<option value="Onyxia's Lair">Onyxia's Lair</option>
		</select><br>
		Started after:
		<input type="date" name="starttime" value="" class="raidfilter">
		Ended before:
		<input type="date" name="endtime" value="" class="raidfilter"><br><br>
		<input type="submit" value="Filter">
	</form>

	<h2>Results:</h2>
	<?php
		if (isset($_GET["guild"])) {
			$guild = /*sqlite_escape_string*/(htmlspecialchars($_GET["guild"]));
		} else {
			$guild = "";
		}
		if (isset($_GET["instance"])) {
			$instance = /*sqlite_escape_string*/(htmlspecialchars($_GET["instance"]));
		} else {
			$instance = "";
		}
		if (isset($_GET["starttime"])) {
			$starttime = strtotime(/*sqlite_escape_string*/(htmlspecialchars($_GET["starttime"])));
		} else {
			$starttime = "";
		}
		if (isset($_GET["endtime"])) {
			$endtime = strtotime(/*sqlite_escape_string*/(htmlspecialchars($_GET["endtime"])));	
		} else {
			$endtime = "";
		}

		$db = getDB();

		// This really is not a very good way to create a query
		$preparestring = "SELECT raidID, guildName, faction, instance, startTime, endTime FROM raids_guild";
		if ($guild != "" or $instance != "" or $starttime != "" or $endtime != "") {
			$preparestring .= " WHERE ";
		}
		if ($guild != "") {
			$preparestring .= "guildname LIKE :guild";
		}
		if ($instance != "") {
			if ($preparestring != "SELECT raidID, guildName, faction, instance, startTime, endTime FROM raids_guild WHERE ") {
				$preparestring .= " AND ";
			}
			$preparestring .= "instance=:instance";
		}
		if ($starttime != "") {
			if ($preparestring != "SELECT raidID, guildName, faction, instance, startTime, endTime FROM raids_guild WHERE ") {
				$preparestring .= " AND ";
			}
			$preparestring .= "starttime>=:starttime";
		}
		if ($endtime != "") {
			if ($preparestring != "SELECT raidID, guildName, faction, instance, startTime, endTime FROM raids_guild WHERE ") {
				$preparestring .= " AND ";
			}
			$preparestring .= "endtime<=:endtime";
		}
		$preparestring .= " ORDER BY endTime DESC LIMIT 50;";
		$statement = $db->prepare($preparestring);
		$statement->bindValue(":guild", "%" . $guild . "%");
		$statement->bindValue(":instance", $instance);
		$statement->bindValue(":starttime", $starttime);
		$statement->bindValue(":endtime", $endtime);

		$result = $statement->execute();

		raidDisplay($result);
	?>

</body>
</html>
