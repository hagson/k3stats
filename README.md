## Preparation ##
1. Open the util.php file and edit the $myPath variable to contain the complete path to the logs.db file.
2. Create the logs.db file using sqlite. `sqlite3 logs.db < createdb.sql`
3. Run the scrape.py file to fetch bosskill data. This will take significant time the first time it's ran. See next section for details.
4. Set up your web server, and move the .php, .css, .js and .html files to an appropriate folder.

## Running the scraper ##
    usage: ./scrape.py [-h] [-p PATH] [--ranksonly] [--noranks] [ID [ID]] [--until ID | --limit LIMIT]

PATH is the path to the folder containing the database and auxiliary files. If not supplied the current directory will be assumed.

If no ID is supplied the scraper automatically scrapes the ID's needed to keep the database up to date. If one ID is supplied this ID will be scraped. If two ID's are supplied these and all ID's between them are scraped.

To keep the database up to date you need to rerun the scraper regularly, eg. every 15 minutes or so, for example using a cronjob.

For more information about ways to modify the scrapers behaviour run `./scrape.py -h`

## Licensing ##
All code is written by me (Hagson) and licensed under the GNU Affero GPL Version 3 or later (see the COPYING file) EXCEPT sorttable.js which is written by Stuart Langridge and licensed under the MIT license (see https://kryogenix.org/code/browser/licence.html)
