<!-- Copyright 2018,2019 Hagson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

<?php
	require("util.php");
	echoInitial("", false, false);

	echo "\t<h1>Latest raids:</h1>\n";
	$db = getDB();
	$result = $db->query("SELECT raidID, guildName, faction, instance, startTime, endTime FROM raids_guild ORDER BY endTime DESC LIMIT 30");
	raidDisplay($result);
	?>
</body>
</html>
