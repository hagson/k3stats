<!-- Copyright 2018,2019 Hagson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

<?php
	require("util.php");
	echoInitial("Player search", false, false);

	if (isset($_GET["name"]) && $_GET["name"] != "") {
		$db = getDB();
		$statement = $db->prepare("SELECT DISTINCT playerName FROM encounters_player WHERE playerName LIKE :playername LIMIT 100");
		$statement->bindValue(":playername", "%" . /*sqlite_escape_string*/(htmlspecialchars($_GET["name"])) . "%");
		$result = $statement->execute();

		$i = 0;
		$table = "<table class=\"sortable\"><tr><th>Player</th></tr>";
		while ($row = $result->fetchArray()) {
			$table .= "<tr><td><a href=\"players.php?name=" . $row["playerName"] . "\">" . $row["playerName"] . "</a></td></tr>\n";
			$temp = $row["playerName"];
			$i++;
		}

		if ($i == 0) {
			echo "<h1>Find Players</h1>";
			echo ("
			<form method=\"GET\">
					Player name:
					<input type=\"text\" name=\"name\" value=\"\" class=\"raidfilter\">
					<input type=\"submit\" value=\"Search\">
			</form>");
			echo ("<h1>Nothing found</h1>\n");
		} elseif ($i == 1) {
			header("Location: players.php?name=" . $temp);
			die();
		} else {
			echo ("<h1>Find Players</h1>");
			echo ("
			<form method=\"GET\">
					Player name:
					<input type=\"text\" name=\"name\" value=\"\" class=\"raidfilter\">
					<input type=\"submit\" value=\"Search\">
			</form>");
			echo ("<h1>Results:</h1>\n" . $table . "</table>\n");
		}
	} else {
		echo ("<h1>Find Players</h1>");
		echo ("
		<form method=\"GET\">
				Player name:
				<input type=\"text\" name=\"name\" value=\"\" class=\"raidfilter\">
				<input type=\"submit\" value=\"Search\">
		</form>");
	}
?>

</body>
</html>
