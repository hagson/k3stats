function cleanForms() {
    var forms = document.getElementsByTagName("form");
	for (var i = 0; i < forms.length; i++) {
		var form = forms[i];
		form.addEventListener("submit", function(event) {
			event.preventDefault();
			disableEmptyFields(form);
			form.submit();
			enableFields(form); // re-enable fields after submitting so they can be edited again
		});
	}
}

function enableFields(form) {
	var selects = form.getElementsByTagName("select");
	for (var i = 0; i < selects.length; i++) {
		selects[i].disabled = false;
	}
	var inputs = form.getElementsByTagName("input");
	for (var i = 0; i < inputs.length; i++) {
		inputs[i].disabled = false;
	}
}

function disableEmptyFields(form) {
	var selects = form.getElementsByTagName("select");
	for (var i = 0; i < selects.length; i++) {
		var select = selects[i];
		if (select.value == "") {
			select.disabled = true;
		}
	}
	var inputs = form.getElementsByTagName("input");
	for (var i = 0; i < inputs.length; i++) {
		var input = inputs[i];
		if (input.value == "") {
			input.disabled = true;
		}
	}
}

window.onload = function() {
	cleanForms();
}
